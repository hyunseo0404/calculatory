//
//  QuickModeController.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "QuickModeController.h"
#import "RoundRectWindow.h"
#import "AppDelegate.h"
#import "QuickField.h"
#import "Constants.h"
#import "DCMathParser.h"

// macro for printing Calculator objects
#define PRINT(O, SF) [(Calculator *)(O) print:(SF)]

@implementation QuickModeController

@synthesize preY, currentError, isMiniMode;

-(NSString*) ans_str
{
    return ans_str;
}

-(void) setAns_str:(NSString*) str;
{
    ans_str = str;
}

- (id)init
{
    isMiniMode = [[NSUserDefaults standardUserDefaults] boolForKey:MINI_MODE_KEY];
    self = [super initWithWindowNibName:isMiniMode?@"QuickMode-Mini":@"QuickMode"];
    if (self) {
        AppDelegate *appDelegate = (AppDelegate*)[[NSApplication sharedApplication] delegate];
        [appDelegate setQuickModeIsOpen:YES];
    }
    return self;
}

- (NSString *)binaryStringFromInt:(int)n
{
    if (n == 0) return @"0";
    BOOL isNegative = NO;
    if (n < 0) {
        isNegative = YES;
        n = ~n;
    }
    // support 2's complement binary numbers
    NSMutableString *str = [NSMutableString new];
    for (NSInteger numberCopy = n; numberCopy > 0; numberCopy >>= 1) {
        // Prepend "0" or "1", depending on the bit
        [str insertString:((numberCopy & 1) ? @"1" : @"0") atIndex:0];
    }
    return isNegative ? [@"-" stringByAppendingString:str] : str;
}

-(IBAction)calculate:(id)sender
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setRoundingMode:NSNumberFormatterRoundHalfUp];
    
    id ans;
    int sigfig = (int) [userDefaults integerForKey:MAX_FRAC_DIGIT_KEY];
    int maxInt = (int) [userDefaults integerForKey:MAX_INT_DIGIT_KEY];
    int numsys = (int) [userDefaults integerForKey:NUMSYS_KEY];
    [formatter setMaximumFractionDigits:[userDefaults integerForKey:MAX_FRAC_DIGIT_KEY]];
    [formatter setUsesGroupingSeparator:NO];
    
    NSString *sf = [NSString stringWithFormat:@"%%.%dLf", sigfig]; // TEST
    temp = [[NSString alloc] initWithString:aField.string];
    
    DCMathParser *parser = [[DCMathParser alloc] initWith:temp];
    
    currentError = nil;
    [warningLabel setStringValue:@""];
    
    @try {
        ans = [parser parse];
        if ([ans isMemberOfClass:[Numbers class]]) {
            if (numsys == DECIMAL) {
                long double ansVal = [ans longDoubleValue];
                long double digit = fabsl(floorl(log10l(fabsl(fmodl(ansVal,1)))));
                if ([ans n] != 0 && (floorl(log10l(fabsl(ansVal))) + 1 > maxInt
                                     ||  (ansVal < 1 && ansVal > -1 && digit != INFINITY && digit > sigfig))) {
//                    [formatter setNumberStyle:NSNumberFormatterScientificStyle];
//                    ans = [formatter stringFromNumber:[NSNumber numberWithDouble:[ans doubleValue]]];
                    ans = [NSString stringWithFormat:@"%.2Le",[ans longDoubleValue]];
                } else {
                    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                    if ([userDefaults boolForKey:COMMA_SEP_KEY]) {
                        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:[NSLocale preferredLanguages][0]]];
                        [formatter setUsesGroupingSeparator:YES];
                    }
                    ans = [formatter stringFromNumber:[NSNumber numberWithDouble:[ans doubleValue]]];
                }
            } else {
                if (log2([ans n]) > 32) {
                    [warningLabel setStringValue:@"Numbers larger than 32 bits can only be shown in decimal"];
                    ans = [NSString stringWithFormat:@"%La", [ans n]];
                } else if (numsys == BINARY) {
                    if (ceill([ans n]) == [ans n] && floorl([ans n]) == [ans n]) {
                        ans = [self binaryStringFromInt:[ans n]];
                    } else {
                        [warningLabel setStringValue:@"Floating-point binary representation is not supported yet"];
                        ans = [NSString stringWithFormat:@"%La", [ans n]];
                    }
                } else if (numsys == HEX) {
                    if (ceill([ans n]) == [ans n] && floorl([ans n]) == [ans n]) {
                        ans = [NSString stringWithFormat:@"0x%x", (int)[ans n]];
                    } else {
                        [warningLabel setStringValue:@"Floating-point hexadecimal representation is not supported yet"];
                        ans = [NSString stringWithFormat:@"%La", [ans n]];
                    }
                } else if (numsys == OCTAL) {
                    if (ceill([ans n]) == [ans n] && floorl([ans n]) == [ans n]) {
                        ans = [NSString stringWithFormat:@"0%o", (int)[ans n]];
                    } else {
                        [warningLabel setStringValue:@"Floating-point octal representation is not supported yet"];
                        ans = [NSString stringWithFormat:@"%La", [ans n]];
                    }
                }
            }
            ans = [ans stringByReplacingOccurrencesOfString:@"-inf" withString:[NSString stringWithFormat:@"-%C", (unsigned short) 0x221E]];
            ans = [ans stringByReplacingOccurrencesOfString:@"inf" withString:[NSString stringWithFormat:@"+%C", (unsigned short) 0x221E]];
            ans = [ans stringByReplacingOccurrencesOfString:@"nan" withString:@"NaN"];
            ans = [[Message alloc] initWith:ans];
        }
        else if ([ans isMemberOfClass:[Vector class]]) {
            if (numsys != DECIMAL) {
                [warningLabel setStringValue:@"Vector result is currently only shown in decimal"];
            }
            ans = PRINT(ans, sf);
            ans = [ans stringByReplacingOccurrencesOfString:@"," withString:@", "];
            ans = [ans stringByReplacingOccurrencesOfString:@"-inf" withString:[NSString stringWithFormat:@"-%C", (unsigned short) 0x221E]];
            ans = [ans stringByReplacingOccurrencesOfString:@"inf" withString:[NSString stringWithFormat:@"+%C", (unsigned short) 0x221E]];
            ans = [ans stringByReplacingOccurrencesOfString:@"nan" withString:@"NaN"];
            ans = [[Message alloc] initWith:ans];
        }
        else if ([ans isKindOfClass:[Booleans class]]) {
            ans = [[Message alloc] initWith:[(Booleans*)ans stringValue]];
        }
        else if ([ans isKindOfClass:[NSError class]] || !ans) {
            currentError = ans;
            ans = [[Message alloc] initWith:@"?"];
            if (parser.errPos != -1) {
                [[aField textStorage] addAttribute:NSForegroundColorAttributeName value:[NSColor purpleColor]
                                             range:NSMakeRange(MIN(parser.errPos, parser.exp.length-1), parser.exp.length-parser.errPos)];
            }
        }
    } @catch (NSException *e) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:e.reason forKey:NSLocalizedDescriptionKey];
        currentError = [NSError errorWithDomain:e.name code:-1 userInfo:details];
        ans = [[Message alloc] initWith:@"?"];
    }
    
    ans_str = PRINT(ans, sf);
    ans = [NSString stringWithFormat:@"= %@", ans_str];
    [self changeFontToFitRect:ans];
    [ansField setStringValue:ans];
}

-(IBAction)ansClicked:(id)sender
{
    if ([[ansField stringValue] isNotEqualTo:@""] && [[ansField stringValue] isNotEqualTo:@"= ?"]) {
        NSPasteboard *pasteBoard = [NSPasteboard generalPasteboard];
        [pasteBoard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:nil];
        [pasteBoard setString:ans_str forType:NSStringPboardType];
    }
}

- (IBAction)calculateAndCopy:(id)sender
{
    [self calculate:sender];
    [self ansClicked:sender];
}

- (IBAction)skipToNextParenthesis:(id)sender
{
    NSUInteger cursorPosition = aField.selectedRange.location;
    NSRange nextRange = [aField.string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"()[]{}"]
                                                       options:NSLiteralSearch
                                                         range:NSMakeRange(cursorPosition, aField.string.length-cursorPosition)];
    if (nextRange.location == NSNotFound) {
        [aField setSelectedRange:NSMakeRange(aField.string.length, 0)];
    } else {
        [aField setSelectedRange:NSMakeRange(nextRange.location+1, 0)];
    }
}

- (IBAction)skipToPrevParenthesis:(id)sender
{
    NSUInteger cursorPosition = aField.selectedRange.location;
    NSRange prevRange = [aField.string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"()[]{}"]
                                                       options:NSBackwardsSearch
                                                         range:NSMakeRange(0, cursorPosition)];
    if (prevRange.location == NSNotFound) {
        [aField setSelectedRange:NSMakeRange(0, 0)];
    } else {
        [aField setSelectedRange:NSMakeRange(prevRange.location, 0)];
    }
}

- (IBAction)changeNumSys:(id)sender
{
    switch ([sender tag]) {
        case DECIMAL:
            [sender setTag:BINARY];
            break;
        case BINARY:
            [sender setTag:HEX];
            break;
        case HEX:
            [sender setTag:OCTAL];
            break;
        case OCTAL:
            [sender setTag:DECIMAL];
            break;
        default:
            break;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:[sender tag] forKey:NUMSYS_KEY];
    [defaults synchronize];
    [sender setTitle:[self numSysToStr:(Numeral)[numSysButton tag]]];
    if ([[aField string] isNotEqualTo:@""]) {
        [self calculate:nil];
    }
}

- (void)textDidChange:(NSNotification *)obj
{
    if ([obj object] == aField) {
//        [[aField textStorage] removeAttribute:NSForegroundColorAttributeName range:NSMakeRange(0, aField.string.length)];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [aField setTextColor:[NSColor colorWithCalibratedRed:[defaults floatForKey:FONT_RED_KEY]
                                                       green:[defaults floatForKey:FONT_GREEN_KEY]
                                                        blue:[defaults floatForKey:FONT_BLUE_KEY] alpha:1]];
        if ([[aField string] isNotEqualTo:@""]) {
            [self calculate:aField];
            [self setLineBreakMode:NSLineBreakByCharWrapping forView:aField];
        } else {
            [ansField setFont:[NSFont fontWithName:[[ansField font] fontName] size:(isMiniMode?34:50)]];
            [ansField setStringValue:@""];
            [warningLabel setStringValue:@""];
            currentError = nil;
        }
        [self updateViews];
    }
}

-(void)setLineBreakMode:(NSLineBreakMode)mode forView:(NSTextView*)view
{
    NSTextStorage *storage = [view textStorage];
    NSMutableParagraphStyle *style = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    [style setLineBreakMode:mode];
    [storage addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, [storage length])];
}

- (int)numberOfLines
{
    NSLayoutManager *layoutManager = [aField layoutManager];
    unsigned numberOfLines, index, numberOfGlyphs = (unsigned) [layoutManager numberOfGlyphs];
    NSRange lineRange;
    for (numberOfLines = 0, index = 0; index < numberOfGlyphs; numberOfLines++){
        (void) [layoutManager lineFragmentRectForGlyphAtIndex:index
                                               effectiveRange:&lineRange];
        index = (unsigned) NSMaxRange(lineRange);
    }
    return numberOfLines;
}

-(void)updateViews
{
    int line = [self numberOfLines];
    float def = [[aField layoutManager] defaultLineHeightForFont:aField.font];
    float def_2 = def * (isMiniMode?1:2);
    float y = def * (line + (aField.string.length > 0 && [aField.string characterAtIndex:aField.string.length-1] == '\n'));
//    NSLog(@"y: %f, preY: %f", y, preY);
    
    preY = MAX(preY, def_2);
    
    if (y != preY && y > def_2) {
        [[self window] setFrame:NSMakeRect(self.window.frame.origin.x,
                                           self.window.frame.origin.y-(y-preY),
                                           self.window.frame.size.width,
                                           self.window.frame.size.height+(y-preY))
                        display:YES animate:YES];
    } else if (y != preY && preY > def_2) {
        [[self window] setFrame:NSMakeRect(self.window.frame.origin.x,
                                           self.window.frame.origin.y-(def_2-preY),
                                           self.window.frame.size.width,
                                           self.window.frame.size.height+(def_2-preY))
                        display:YES animate:YES];
        y = def_2;
    }
    
    preY = MAX(y, def_2);
    [mainView updateTrackingAreas];
}

-(void)changeFontToFitRect:(NSString*)str
{
    NSRect r = [ansField frame];
    float targetWidth = r.size.width;
    float targetHeight = r.size.height;
    int i;
    int minFontSize, maxFontSize;
    if (isMiniMode) {
        minFontSize = 10;
        maxFontSize = 34;
    } else {
        minFontSize = 20;
        maxFontSize = 50;
    }
    NSFont *tmp_fnt;
    for (i = minFontSize; i < maxFontSize; i++) {
        NSDictionary* attrs = [[NSDictionary alloc] initWithObjectsAndKeys:[NSFont fontWithName:[[ansField font] fontName] size:i], NSFontAttributeName, nil];
        NSSize strSize = [str sizeWithAttributes:attrs];
        if (strSize.width > targetWidth || strSize.height > targetHeight) break;
    }
    tmp_fnt = [NSFont fontWithName:[[ansField font] fontName] size:i];
    [ansField setFont:tmp_fnt];
}

-(IBAction)stick:(id)sender
{
    if ([self.window level] == NSFloatingWindowLevel) {
        [self.window setLevel:NSNormalWindowLevel];
        [self.window setHidesOnDeactivate:YES];
        [stickyButton setImage:[NSImage imageNamed:@"pin_button_normal"]];
    } else {
        [self.window setLevel:NSFloatingWindowLevel];
        [self.window setHidesOnDeactivate:NO];
        [stickyButton setImage:[NSImage imageNamed:@"pin_button_pressed"]];
    }
}

- (IBAction)showInfo:(id)sender
{
    if ([infoPopover isShown]) {
        [infoPopover close];
    } else {
        if (currentError) {
            [infoNoError setHidden:YES];
            infoErrorTitle.stringValue = [NSLocalizedString(@"Error: ",nil) stringByAppendingString:currentError.domain];
            infoErrorReason.stringValue = [NSLocalizedString(@"Reason: ",nil) stringByAppendingString:[currentError localizedDescription]];
            infoErrorType.stringValue = [NSLocalizedString(@"Error Code: ",nil) stringByAppendingString:[NSString stringWithFormat:@"%ld", currentError.code]];
        } else {
            [infoNoError setHidden:NO];
            infoErrorTitle.stringValue = @"";
            infoErrorReason.stringValue = @"";
            infoErrorType.stringValue = @"";
        }
        [infoPopover showRelativeToRect:[sender bounds] ofView:sender preferredEdge:NSMaxYEdge];
    }
}

-(BOOL)textView:(NSTextView *)fieldEditor doCommandBySelector:(SEL)commandSelector {
    if (commandSelector == @selector(insertNewline:)) {
        [self ansClicked:fieldEditor];
        return YES;
    } else if (commandSelector == @selector(insertTab:)) {
        [self skipToNextParenthesis:fieldEditor];
        return YES;
    } else if (commandSelector == @selector(insertBacktab:)) {
        [self skipToPrevParenthesis:fieldEditor];
        return YES;
    }
    return NO;
}

-(void)cancelOperation:(id)sender {
    [self.window close];
    AppDelegate *appDelegate = (AppDelegate*)[[NSApplication sharedApplication] delegate];
    [appDelegate setQuickModeIsOpen:NO];
}

- (void)windowWillClose:(NSNotification *)notification {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:REMEMBER_LAST_KEY]) {
        [defaults setObject:aField.string forKey:REMEMBER_LAST_STR_KEY];
        [defaults synchronize];
    }
}

-(void)windowDidResignKey:(NSNotification *)notification {
    if ([[notification object] level] == NSFloatingWindowLevel)
        [[notification object] setAlphaValue:0.5];
    else {
        [self.window close];
        AppDelegate *appDelegate = (AppDelegate*)[[NSApplication sharedApplication] delegate];
        [appDelegate setQuickModeIsOpen:NO];
    }
}

-(void)windowDidBecomeKey:(NSNotification *)notification {
    [[notification object] setAlphaValue:1];
}

- (void)awakeFromNib
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [ansField setTextColor:[NSColor colorWithCalibratedRed:[defaults floatForKey:FONT_RED_KEY]
                                                     green:[defaults floatForKey:FONT_GREEN_KEY]
                                                      blue:[defaults floatForKey:FONT_BLUE_KEY] alpha:1]];
    [numSysButton setTag:[defaults integerForKey:NUMSYS_KEY]];
    [numSysButton setTitle:[self numSysToStr:(Numeral)[numSysButton tag]]];
    [errDetailButton setHidden:![defaults integerForKey:ERR_DETAIL_KEY]];
    [stickyButton setHidden:![defaults boolForKey:SHOW_STICKY_KEY]];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:ALWAYS_STICKY_KEY]) {
        [self.window setLevel:NSFloatingWindowLevel];
        [self.window setHidesOnDeactivate:NO];
        [stickyButton setImage:[NSImage imageNamed:@"pin_button_pressed"]];
    }
}

- (NSString*)windowFrameAutosaveName
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:REMEMBER_POS_KEY];
}

- (void)changeNumSysShortcut:(unsigned short)keyCode
{
    switch (keyCode) {
        case 18: // 1
            // DEC
            [numSysButton setTag:DECIMAL];
            break;
        case 19: // 2
            // BIN
            [numSysButton setTag:BINARY];
            break;
        case 20: // 3
            // HEX
            [numSysButton setTag:HEX];
            break;
        case 21: // 4
            // OCT
            [numSysButton setTag:OCTAL];
            break;
        default:
            break;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:[numSysButton tag] forKey:NUMSYS_KEY];
    [defaults synchronize];
    [numSysButton setTitle:[self numSysToStr:(Numeral)[numSysButton tag]]];
    if ([[aField string] isNotEqualTo:@""]) {
        [self calculate:nil];
    }
}

- (NSString *)numSysToStr:(Numeral)numeral
{
    switch (numeral) {
        case DECIMAL:
            return @"DEC";
        case BINARY:
            return @"BIN";
        case HEX:
            return @"HEX";
        case OCTAL:
            return @"OCT";
        default:
            return @"?";
    }
}

@end
