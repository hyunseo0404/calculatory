//
//  Booleans.h
//  Calculatory
//
//  Created by Dennis Chung on 12. 12. 4..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"

@interface Booleans : Calculator

@property BOOL value;

- (id)initWithValue:(BOOL)boolean;
- (NSString *) stringValue;

@end
