//
//  RoundRectWindow.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RoundRectWindow : NSView

@property (nonatomic,retain) NSTrackingArea *trackingArea;

@end
