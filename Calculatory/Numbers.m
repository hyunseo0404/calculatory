//
//  Numbers.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Numbers.h"
#import "Vector.h"

@implementation Numbers

@synthesize n;

-(id) init
{
    return [self initWith:0];
}

-(id) initWith:(long double)num
{
    self = [super init];
    
    if (self) {
        [self setTo:num];
    }
    
    return self;
}

-(id) initWithString:(NSString *)str
{
    self = [super init];
    
    if (self) {
        str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self setTo:[str doubleValue]];
    }
    
    return self;
}

-(void) setTo:(long double)num
{
    n = num;
}

-(double) doubleValue
{
    return (double) n;
}

- (long double) longDoubleValue
{
    return n;
}

-(NSString *) print:(NSString *)fmt
{
    return [NSString stringWithFormat:fmt, n];
}

-(Numbers *) add:(Numbers *)num
{
    if ([self isMemberOfClass:[Numbers class]] && [num isMemberOfClass:[Numbers class]]) {
        return [[Numbers alloc] initWith:self.n+num.n];
    } else {
        [NSException raise:@"Invalid operation" format:@"Cannot add a scalar with a vector"];
        return nil;
    }
}

-(Numbers *) subtract:(Numbers *)num
{
    if ([self isMemberOfClass:[Numbers class]] && [num isMemberOfClass:[Numbers class]]) {
        return [[Numbers alloc] initWith:self.n-num.n];
    } else {
        [NSException raise:@"Invalid operation" format:@"Cannot subtract a scalar with a vector"];
        return nil;
    }
}

-(id) multiply:(id)num
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [num isMemberOfClass:[Numbers class]] ) {
            return [[Numbers alloc] initWith:self.n*[num n]];

        }
        else if ( [num isMemberOfClass:[Vector class]] ) {
            return [(Vector*)num multiply:self];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [num isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self multiply:num];
        }
        else if ( [num isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self multiply:num];
        }
    }
    
    else {
        [NSException raise:@"Invalid multiplication" format:@"Unknown multiplication error"];
    }
    return nil;
}

-(id) divide:(id)num
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [num isMemberOfClass:[Numbers class]] ) {
            return [[Numbers alloc] initWith:self.n/[num n]];
        }
        else if ( [num isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector division" format:@"Cannot divide a scalar with a vector"];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [num isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self divide:num];
        }
        else if ( [num isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self divide:num];
        }
    }
    
    else {
        [NSException raise:@"Invalid division" format:@"Unknown division error"];
    }
    return nil;
}

-(Numbers *) exponentiate:(Numbers *)num
{
    return [[Numbers alloc] initWith:(powl(self.n, num.n))];
}

-(Numbers *) factorial
{
    if (self.n == 1 || self.n == 0) {
        return [[Numbers alloc] initWith:1];
    }
    return [[Numbers alloc] initWith:tgammal(self.n+1)];
}

-(Numbers *) modulo:(Numbers *)num
{
    return [[Numbers alloc] initWith:fmodl(self.n, num.n)];
}

-(Numbers *) chooseR:(Numbers *)num
{
    if (self.n == 0 || num.n == 0 || self.n == num.n) return [[Numbers alloc] initWith:1];
    return [[Numbers alloc] initWith:roundl(expl((lgammal(self.n+1)) - (lgammal(num.n+1) + lgammal(self.n-num.n+1))))];
}

-(Numbers *) pickR:(Numbers *)num
{
    if (self.n == 0 || num.n == 0) return [[Numbers alloc] initWith:1];
    return [[Numbers alloc] initWith:roundl(expl(lgammal(self.n+1) - lgammal(self.n-num.n+1)))];
}

// Iterative Binary Euclid Algorithm
- (Numbers *) gcd:(Numbers *)num
{
    int t, k;
    
    long double u_tmp = self.n;
    long double v_tmp = num.n;
    
    int rounded = 1;
    
    // move the decimal up until the number has reached the maximum value that int can hold (+/- 2e31),
    // or until both numbers have become a whole number
    while ((floorl(u_tmp) != u_tmp || floorl(v_tmp) != v_tmp)
           && u_tmp < 10e8 && v_tmp < 10e8 && u_tmp > -10e8 && v_tmp > -10e8) {
        u_tmp *= 10.0;
        v_tmp *= 10.0;
        rounded *= 10;
    }
    
    // truncates the remaining decimal digits, if any
    int u = truncl(u_tmp);
    int v = truncl(v_tmp);
    
    u = u < 0 ? -u : u; /* abs(u) */
    v = v < 0 ? -v : v;
    
    if (u < v) {
        t = u;
        u = v;
        v = t;
    }
    if (v == 0)
        return [[Numbers alloc] initWith:u/rounded];
    
    k = 1;
    while ((u & 1) == 0 && (v & 1) == 0) { /* u, v - even */
        u >>= 1; v >>= 1;
        k <<= 1;
    }
    
    t = (u & 1) ? -v : u;
    BOOL check = NO;
    while (t) {
        while ((t & 1) == 0)
            t >>= 1;
        
        if (t > 0)
            u = t;
        else
            v = -t;
        
        t = u - v;
        
        if (check && t == 2)
            [NSException raise:@"Unexpected error occured while calculating GCD/LCM" format:@"Unknown (maybe the value is too big?)"];
        else check = t == 2;
    }
    
    return [[Numbers alloc] initWith:(u * k)/rounded];
}

- (Numbers *) lcm:(Numbers *)num
{
    long double a = self.n;
    long double b = num.n;
    long double gcd = [[self gcd:num] longDoubleValue];
    long double lcm = (fabsl(a >= b ? a : b)/gcd) * fabsl(a >= b ? b : a);
    
    return [[Numbers alloc] initWith:lcm];
}

- (Numbers *) shiftLeft:(Numbers *)num
{
    if (floorl(self.n) != self.n || floorl(num.n) != num.n) {
        [NSException raise:@"Invalid operands to binary expression: <<" format:@"Both operands must be an integer"];
    }
    
    return [[Numbers alloc] initWith:((int)self.n << (int)num.n)];
}

- (Numbers *) shiftRight:(Numbers *)num
{
    if (floorl(self.n) != self.n || floorl(num.n) != num.n) {
        [NSException raise:@"Invalid operands to binary expression: >>" format:@"Both operands must be an integer"];
    }
    
    return [[Numbers alloc] initWith:((int)self.n >> (int)num.n)];
}

- (Numbers *) shiftRightUnsigned:(Numbers *)num
{
    if (floorl(self.n) != self.n || floorl(num.n) != num.n) {
        [NSException raise:@"Invalid operands to binary expression: >>>" format:@"Both operands must be an integer"];
    }
    
    return [[Numbers alloc] initWith:((unsigned)self.n >> (int)num.n)];
}

- (Numbers *) bitwiseAnd:(Numbers *)num
{
    if (floorl(self.n) != self.n || floorl(num.n) != num.n) {
        [NSException raise:@"Invalid operands to binary expression: &" format:@"Both operands must be an integer"];
    }
    
    return [[Numbers alloc] initWith:((int)self.n & (int)num.n)];
}

- (Numbers *) bitwiseXor:(Numbers *)num
{
    if (floorl(self.n) != self.n || floorl(num.n) != num.n) {
        [NSException raise:@"Invalid operands to binary expression: ^" format:@"Both operands must be an integer"];
    }
    
    return [[Numbers alloc] initWith:((int)self.n ^ (int)num.n)];
}

- (Numbers *) bitwiseOr:(Numbers *)num
{
    if (floorl(self.n) != self.n || floorl(num.n) != num.n) {
        [NSException raise:@"Invalid operands to binary expression: |" format:@"Both operands must be an integer"];
    }
    
    return [[Numbers alloc] initWith:((int)self.n | (int)num.n)];
}

- (Numbers *) negate
{
    if (floorl(self.n) != self.n) {
        [NSException raise:@"Invalid operands to binary expression: ~" format:@"Operand must be an integer"];
    }
    
    return [[Numbers alloc] initWith:~(int)self.n];
}

- (Numbers *) not
{
    return [[Numbers alloc] initWith:!self.n];
}

- (Numbers *) negative
{
    return [[Numbers alloc] initWith:-self.n];
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"%Lf", self.n];
}

- (BOOL)isEqual:(id)object
{
    return [self isEqualTo:object];
}

- (BOOL)isEqualTo:(id)object
{
    if (![object isMemberOfClass:[Numbers class]]) return NO;
    else return self.n == [object n];
}

@end
