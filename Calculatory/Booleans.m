//
//  Booleans.m
//  Calculatory
//
//  Created by Dennis Chung on 12. 12. 4..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Booleans.h"

@implementation Booleans

@synthesize value;

- (id)initWithValue:(BOOL)boolean
{
    self = [super init];
    if (self) {
        value = boolean;
    }
    return self;
}

- (NSString *) stringValue
{
    return [NSString stringWithFormat:@"%@", value ? @"True" : @"False"];
}

- (NSString *) description
{
    return [self stringValue];
}

@end
