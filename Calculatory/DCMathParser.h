//
//  DCMathParser.h
//  Calculatory
//
//  Created by Dennis Chung on 13. 1. 10..
//  Copyright (c) 2013년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"

@interface DCMathParser : NSObject

typedef enum {
    START_MARK,
    SIN, COS, TAN, CSC, SEC, COT, ASIN, ACOS, ATAN, ACSC, ASEC, ACOT,
    SINH, COSH, TANH, CSCH, SECH, COTH, ASINH, ACOSH, ATANH, ACSCH, ASECH, ACOTH,
    LOG, SQRT, CEIL, FLOOR, ROUND, TRUNC, ABS, FTOC, CTOF, FTOK, KTOF, CTOK, KTOC,
    M2S, S2M, M2H, H2M, H2S, S2H, VECTOR, COMB, COMB_C, PERM, PERM_C, MAX, MIN, GCD, GCF, LCM,
    MAG, NORM, LN, LOG2, LOG10, DOT, RANDOM, RANDOMR,
    CUSTOM,
    ERROR,
    END_MARK
} Functions;

typedef enum {
    PLUS = '+', MINUS = '-', MULT = '*', DIVD = '/', FACT = '!', EXPO = '`', NEGATE = '~',
    LEFT_SHIFT = '<', RIGHT_SHIFT = '>', UNSIGNED_RIGHT_SHIFT,
    AND = '&', XOR = '^', OR = '|',
    OPEN_PAREN = '(', CLOSE_PAREN = ')',
    MODULO = '%', NOT = '$',
    NEGATIVE = '_'
} Operators;

typedef enum {
    PI, EULER
} Symbols;

typedef enum {
    DECIMAL = 10, BINARY = 2, HEX = 16, OCTAL = 8
} Numeral;

@property (nonatomic, strong) NSString *exp;
@property (nonatomic, strong) Calculator *ans;
@property (nonatomic) Numeral numsys;
@property (nonatomic) unsigned long errPos;
@property (nonatomic, strong) NSMutableArray *raw;

- (id) init;
- (id) initWith:(NSString *)input;
- (void) initialize;
- (id) parse;
- (id) evaluate:(NSMutableArray *)output;

- (void)addVariableWithSymbol:(NSString *)symb forValue:(double)val;
- (void)updateVariables;
- (void)updateFunctions;

@end
