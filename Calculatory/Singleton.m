//
//  Singleton.m
//  Calculatory
//
//  Created by Dennis Chung on 12. 11. 20..
//  Copyright (c) 2012년 Dennis Chung. All rights reserved.
//

#import "Singleton.h"

static Singleton *sharedMyManager = nil;

@implementation Singleton

@synthesize sharedInstance;

#pragma mark - Singleton Methods

+ (id)sharedManager {
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        sharedInstance = nil;
    }
    return self;
}

@end
