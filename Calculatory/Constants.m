//
//  Constants.m
//  Calculatory
//
//  Created by Dennis Chung on 13. 2. 12..
//  Copyright (c) 2013년 hyunseo0404@naver.com. All rights reserved.
//

#import "Constants.h"

NSString * const REG_HOTKEY_NOTE = @"registerHotKeys";
NSString * const CUST_FUNC_KEY = @"customFunctions";
NSString * const CUST_FUNC_FUNC = @"func";
NSString * const CUST_FUNC_ARGS = @"numArgs";
NSString * const CUST_FUNC_ID = @"id";
NSString * const CUST_FUNC_NAME = @"name";
NSString * const SYMB_NAME_COLUMN_IDENT = @"symbolNameColumn";
NSString * const SYMB_VAL_COLUMN_IDENT = @"symbolValueColumn";
NSString * const FUNC_NAME_COLUMN_IDENT = @"functionNameColumn";
NSString * const FUNC_FORM_COLUMN_IDENT = @"functionFormulaColumn";
NSString * const VAR_KEY = @"var";
NSString * const KEYCODE_KEY = @"keyCode";
NSString * const MODFLAG_KEY = @"modFlags";
NSString * const HOTKEY_KEY = @"hotkey";
NSString * const PREFLANG_KEY = @"lang";
NSString * const MANUALLOC_KEY = @"manLoc";
NSString * const NUMSYS_KEY = @"numSys";
NSString * const MAX_FRAC_DIGIT_KEY = @"maxfrac";
NSString * const MAX_INT_DIGIT_KEY = @"maxint";
NSString * const COMMA_SEP_KEY = @"commaSep";
NSString * const BITWISE_ON_KEY = @"bitwiseOn";
NSString * const CARET_EXPO_KEY = @"caretExpo";
NSString * const AUTO_PAREN_KEY = @"autoParen";
NSString * const LOGIN_KEY = @"login";
NSString * const ERR_DETAIL_KEY = @"errdet";
NSString * const REMEMBER_POS_KEY = @"rememberPos";
NSString * const FRAME_RED_KEY = @"frameRedColor";
NSString * const FRAME_GREEN_KEY = @"frameGreenColor";
NSString * const FRAME_BLUE_KEY = @"frameBlueColor";
NSString * const FIELD_RED_KEY = @"fieldRedColor";
NSString * const FIELD_GREEN_KEY = @"fieldGreenColor";
NSString * const FIELD_BLUE_KEY = @"fieldBlueColor";
NSString * const FONT_RED_KEY = @"fontRedColor";
NSString * const FONT_GREEN_KEY = @"fontGreenColor";
NSString * const FONT_BLUE_KEY = @"fontBlueColor";
NSString * const THEME_KEY = @"theme";
NSString * const SHOW_STICKY_KEY = @"showSticky";
NSString * const ALWAYS_STICKY_KEY = @"alwaysSticky";
NSString * const MINI_MODE_KEY = @"miniMode";
NSString * const REMEMBER_LAST_KEY = @"rememberLast";
NSString * const REMEMBER_LAST_STR_KEY = @"rememberLastStr";

NSString * const THEME_NAMES[] = { @"Sky", @"Cherry Blossom", @"Cloud", @"Tree", @"Dark" };
const int NUM_THEMES = 5;

// default themes
CGFloat const SKY_THEME[]             = {   [FRAME_R] = 0.6882,   [FRAME_G] = 0.8098,   [FRAME_B] = 1.0,
                                            [FIELD_R] = 0.804309, [FIELD_G] = 0.912957, [FIELD_B] = 0.990653,
                                            [FONT_R]  = 0.107414, [FONT_G]  = 0.122144, [FONT_B]  = 0.313578    };
CGFloat const CHERRY_BLOSSOM_THEME[]  = {   [FRAME_R] = 1.0,      [FRAME_G] = 0.706066, [FRAME_B] = 0.886228,
                                            [FIELD_R] = 1.0,      [FIELD_G] = 0.900153, [FIELD_B] = 0.975364,
                                            [FONT_R]  = 0.568315, [FONT_G]  = 0.258504, [FONT_B]  = 0.429349    };
CGFloat const CLOUD_THEME[]           = {   [FRAME_R] = 0.952941, [FRAME_G] = 0.952941, [FRAME_B] = 0.952941,
                                            [FIELD_R] = 0.992157, [FIELD_G] = 0.992157, [FIELD_B] = 0.992157,
                                            [FONT_R]  = 0.250980, [FONT_G]  = 0.250980, [FONT_B]  = 0.250980    };
CGFloat const TREE_THEME[]            = {   [FRAME_R] = 0.751208, [FRAME_G] = 0.936828, [FRAME_B] = 0.666846,
                                            [FIELD_R] = 0.863683, [FIELD_G] = 1.0,      [FIELD_B] = 0.803031,
                                            [FONT_R]  = 0.303756, [FONT_G]  = 0.459541, [FONT_B]  = 0.311835    };
CGFloat const DARK_THEME[]            = {   [FRAME_R] = 0.214543, [FRAME_G] = 0.214543, [FRAME_B] = 0.214543,
                                            [FIELD_R] = 0.409839, [FIELD_G] = 0.417287, [FIELD_B] = 0.427372,
                                            [FONT_R]  = 0.818015, [FONT_G]  = 0.818015, [FONT_B]  = 0.818015    };

const CGFloat * const THEMES[] = { SKY_THEME, CHERRY_BLOSSOM_THEME, CLOUD_THEME, TREE_THEME, DARK_THEME };
