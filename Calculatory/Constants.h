//
//  Constants.h
//  Calculatory
//
//  Created by Dennis Chung on 13. 2. 12..
//  Copyright (c) 2013년 hyunseo0404@naver.com. All rights reserved.
//

#import "Numbers.h"
#import "Vector.h"
#import "Booleans.h"
#import "Message.h"

/// "FOUNDATION_EXPORT" == "extern" ///

extern NSString * const REG_HOTKEY_NOTE;            // NSNotification name for the hotkey registration method
extern NSString * const CUST_FUNC_KEY;              // key string for the userDefaults custom functions
extern NSString * const CUST_FUNC_FUNC;             // key string for the custom functions for function string representation
extern NSString * const CUST_FUNC_ARGS;             // key string for the custom functions for arguments needed
extern NSString * const CUST_FUNC_ID;               // key string for the custom functions for ID
extern NSString * const CUST_FUNC_NAME;             // key string for the custom functions for function name
extern NSString * const SYMB_NAME_COLUMN_IDENT;     // identifier value for the symbol table's name column
extern NSString * const SYMB_VAL_COLUMN_IDENT;      // identifier value for the symbol table's value column
extern NSString * const FUNC_NAME_COLUMN_IDENT;     // identifier value for the function table's name column
extern NSString * const FUNC_FORM_COLUMN_IDENT;     // identifier value for the function table's formula column
extern NSString * const VAR_KEY;                    // key string for the userDefaults custom variables
extern NSString * const KEYCODE_KEY;                // key string for the userDefaults hotkey main key code
extern NSString * const MODFLAG_KEY;                // key string for the userDefaults hotkey modifier flags
extern NSString * const HOTKEY_KEY;                 // key string for the userDefaults hotkey string representation
extern NSString * const PREFLANG_KEY;               // key string for the userDefaults preferred language (localization)
extern NSString * const MANUALLOC_KEY;              // key string for the userDefaults custom localization enabled boolean value
extern NSString * const NUMSYS_KEY;                 // key string for the userDefaults numeral system                               /*** KVO ***/
extern NSString * const MAX_FRAC_DIGIT_KEY;         // key string for the userDefaults maximum fraction digits                      /*** KVO ***/
extern NSString * const MAX_INT_DIGIT_KEY;          // key string for the userDefaults maximum integer digits                       /*** KVO ***/
extern NSString * const COMMA_SEP_KEY;              // key string for the userDefaults comma separated                              /*** KVO ***/
extern NSString * const BITWISE_ON_KEY;             // key string for the userDefaults bitwise on                                   /*** KVO ***/
extern NSString * const CARET_EXPO_KEY;             // key string for the userDefaults caret as exponentiation                      /*** KVO ***/
extern NSString * const AUTO_PAREN_KEY;             // key string for the userDefaults auto-close open parenthesis                  /*** KVO ***/
extern NSString * const LOGIN_KEY;                  // key string for the userDefaults start at login option                        /*** KVO ***/
extern NSString * const ERR_DETAIL_KEY;             // key string for the userDefaults to show error details
extern NSString * const REMEMBER_POS_KEY;           // key string for the userDefaults to remember the previous window position
extern NSString * const FRAME_RED_KEY;              // key string for the userDefaults frame color's red component
extern NSString * const FRAME_GREEN_KEY;            // key string for the userDefaults frame color's green component
extern NSString * const FRAME_BLUE_KEY;             // key string for the userDefaults frame color's blue component
extern NSString * const FIELD_RED_KEY;              // key string for the userDefaults field color's red component
extern NSString * const FIELD_GREEN_KEY;            // key string for the userDefaults field color's green component
extern NSString * const FIELD_BLUE_KEY;             // key string for the userDefaults field color's blue component
extern NSString * const FONT_RED_KEY;               // key string for the userDefaults font color's red component
extern NSString * const FONT_GREEN_KEY;             // key string for the userDefaults font color's green component
extern NSString * const FONT_BLUE_KEY;              // key string for the userDefaults font color's blue component
extern NSString * const THEME_KEY;                  // key string for the userDefaults theme
extern NSString * const SHOW_STICKY_KEY;            // key string for the userDefaults to show sticky button                        /*** KVO ***/
extern NSString * const ALWAYS_STICKY_KEY;          // key string for the userDefaults to always start sticky                       /*** KVO ***/
extern NSString * const MINI_MODE_KEY;              // key string for the userDefaults for the mini mode                            /*** KVO ***/
extern NSString * const REMEMBER_LAST_KEY;          // key string for the userDefaults to remember the last calculation             /*** KVO ***/
extern NSString * const REMEMBER_LAST_STR_KEY;      // key string for the userDefaults for the last calculation                     /*** KVO ***/

/** PRE-DEFINED THEMES **/
enum { FRAME_R, FRAME_G, FRAME_B, FIELD_R, FIELD_G, FIELD_B, FONT_R, FONT_G, FONT_B }; // For Theme Indices
extern const int NUM_THEMES;                        // number of pre-defined themes
extern NSString * const THEME_NAMES[];              // array of pre-defined names of the themes
extern CGFloat const SKY_THEME[];                   // array for the default theme's color scheme
extern CGFloat const CHERRY_BLOSSOM_THEME[];        // array for the 'CHERRY BLOSSOM' theme's color scheme
extern CGFloat const CLOUD_THEME[];                 // array for the 'CLOUD' theme's color scheme
extern CGFloat const TREE_THEME[];                  // array for the 'TREE' theme's color scheme
extern CGFloat const DARK_THEME[];                  // array for the 'DARK' theme's color scheme
extern const CGFloat * const THEMES[];
