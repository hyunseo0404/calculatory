//
//  AppDelegate.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Carbon/Carbon.h>

@class QuickModeController, ConfigWindowController;

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    
    QuickModeController *quickModeController;
    ConfigWindowController *configWindowController;
    
    IBOutlet NSMenu *statusMenu;
    NSStatusItem *statusItem;
    
}

-(IBAction)showQuickMode:(id)sender;
-(IBAction)showConfigWindow:(id)sender;

-(void)registerHotKeys;

@property (assign) IBOutlet NSWindow *window;
@property (assign) NSWindow *quickMode, *configWindow;
@property (assign) NSTextView *aField;
@property BOOL quickModeIsOpen;
@property EventHotKeyRef prevHotKeyRef;

@end
