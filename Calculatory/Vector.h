//
//  Vector.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"
#import "Numbers.h"

@interface Vector : Calculator

@property long double x, y, z;

-(id) init;
-(id) initWith:(long double)a :(long double)b :(long double)c;
-(id) initWithStr:(NSString *) str;
-(void) setTo:(long double)a :(long double)b :(long double)c;
-(NSString *) print:(NSString *) fmt;
-(Vector *) add: (Vector *) v;
-(Vector *) subtract: (Vector *) v;
-(Vector *) multiply: (id) v;
-(Vector *) divide: (id) v;
-(Vector *) exponentiate: (Numbers *) n;
-(Numbers *) dot: (id) v;
-(Numbers *) magnitude;
-(Vector *) normalize; // unit vector
-(Vector *) cross: (Vector *) v; // cross product
-(Vector *) negative;

@end
