//
//  Calculator.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Calculator.h"
#import "Numbers.h"
#import "Vector.h"
#import "Message.h"
#import "Booleans.h"

@implementation Calculator

-(NSString *) print:(NSString *)fmt
{
    return [self print:fmt];
}

- (double)doubleValue
{
    if (![self isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid type" format:@"Trying to use a %@ as a number", NSStringFromClass([self class])];
    } else {
        [NSException raise:@"Unknown error" format:@"Unknown error occurred while converting a number object"];
    }
    return 0;
}

- (long double)longDoubleValue
{
    if (![self isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid type" format:@"Trying to use a %@ as a number", NSStringFromClass([self class])];
    } else {
        [NSException raise:@"Unknown error" format:@"Unknown error occurred while converting a number object"];
    }
    return 0;
}

-(id) add:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self add:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector addition" format:@"Cannot add a scalar with a vector"];
        }
    } else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self add:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            [NSException raise:@"Invalid vector addition" format:@"Cannot add a vector with a scalar"];
        }
    }
    [NSException raise:@"Invalid addition" format:@"Unknown addition error"];
    return nil;
}

-(id) subtract:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self subtract:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector subtraction" format:@"Cannot subtract a scalar with a vector"];
        }
    } else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self subtract:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            [NSException raise:@"Invalid vector subtraction" format:@"Cannot subtract a vector with a scalar"];
        }
    }
    [NSException raise:@"Invalid subtraction" format:@"Unknown subtraction error"];
    return nil;
}

-(id) multiply:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self multiply:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)n multiply:self];
        }
    } else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self multiply:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self multiply:n];
        }
    }
    [NSException raise:@"Invalid multiplication" format:@"Unknown multiplication error"];
    return nil;
}

-(id) divide:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self divide:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector division" format:@"Cannot divide a scalar with a vector"];
        }
    } else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self divide:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self divide:n];
        }
    }
    [NSException raise:@"Invalid division" format:@"Unknown division error"];
    return nil;
}

-(id) dot:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            [NSException raise:@"Invalid dot operation" format:@"Cannot use a dot operation with scalars"];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)n dot:self];
        }
    } else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self dot:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self dot:n];
        }
    }
    [NSException raise:@"Invalid dot operation" format:@"Unknown dot operation error"];
    return nil;
}

-(id) exponentiate:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self exponentiate:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid exponentiation" format:@"Cannot exponentiate to a vector"];
        }
    } else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid exponentiation" format:@"Cannot exponentiate to a vector"];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self exponentiate:n];
        }
    }
    [NSException raise:@"Invalid exponentiation" format:@"Unknown exponentiation error"];
    return nil;
}

-(id) cross:(id)n
{
    if ( [self isMemberOfClass:[Vector class]] && [n isMemberOfClass:[Vector class]] ) {
        return [(Vector*)self cross:n];
    } else {
        [NSException raise:@"Invalid cross operation" format:@"Both arguments must be a vector"];
    }
    return nil;
}

-(id) factorial
{
    if (![self isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for factorial operation (!)" format:@"Argument must be a number"];
    }
    return [(Numbers *)self factorial];
}

-(id) modulo:(id)n
{
    if (![self isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for modulo operation (%)" format:@"Argument must be a number"];
    }
    return [(Numbers *)self modulo:n];
}

-(id) chooseR:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for combination operation (nCr)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self chooseR:n];
}

-(id) pickR:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for permutation operation (nPr)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self pickR:n];
}

-(id) shiftLeft:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for left shift operation (<<)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self shiftLeft:n];
}

-(id) shiftRight:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for right shift operation (>>)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self shiftRight:n];
}

-(id) shiftRightUnsigned:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for unsigned right shift operation (>>>)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self shiftRightUnsigned:n];
}

-(id) bitwiseAnd:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for AND operation (&)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self bitwiseAnd:n];
}

-(id) bitwiseXor:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for XOR operation (^)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self bitwiseXor:n];
}

-(id) bitwiseOr:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for OR operation (|)" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self bitwiseOr:n];
}

-(id) negate
{
    if (![self isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for negate operation (~)" format:@"Argument must be a number"];
    }
    return [(Numbers *)self negate];
}

-(id) not
{
    if (![self isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for not operation (!)" format:@"Argument must be a number"];
    }
    return [(Numbers *)self not];
}

-(id) magnitude
{
    if (![self isMemberOfClass:[Vector class]]) {
        [NSException raise:@"Invalid argument for magnitude operation (mag)" format:@"Argument must be a vector"];
    }
    return [(Vector *)self magnitude];
}

- (id)normalize
{
    if (![self isMemberOfClass:[Vector class]]) {
        [NSException raise:@"Invalid argument for normalize operation (norm)" format:@"Argument must be a vector"];
    }
    return [(Vector *)self normalize];
}

- (id)gcd:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for gcd operation" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self gcd:n];
}

- (id)lcm:(id)n
{
    if (![self isMemberOfClass:[Numbers class]] && [n isMemberOfClass:[Numbers class]]) {
        [NSException raise:@"Invalid argument for lcm operation" format:@"Both arguments must be a number"];
    }
    return [(Numbers *)self lcm:n];
}

- (id)negative
{
    [NSException raise:@"Invalid argument for negative (-) operation" format:@"Argument must be either a number or a vector"];
    return nil;
}

@end
