//
//  ConfigWindowController.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ConfigWindowController : NSWindowController <NSToolbarDelegate, NSTableViewDataSource, NSTableViewDelegate>
{
    IBOutlet NSToolbar *toolbar;
    IBOutlet NSView *general, *features, *appearance, *advanced, *usage, *about;
    
    IBOutlet NSButton *hotkeyButton;
    IBOutlet NSPopUpButton *localization;
    IBOutlet NSButton *resetButton;
    IBOutlet NSButton *loginButton;
    
    IBOutlet NSPopUpButton *sigfig, *numsys;
    
    IBOutlet NSSearchField *symbolSearch;
    IBOutlet NSTableView *symbolTable;
    IBOutlet NSButton *symbolAddButton, *symbolRemoveButton;
    IBOutlet NSSearchField *functionSearch;
    IBOutlet NSTableView *functionTable;
    IBOutlet NSButton *functionAddButton, *functionRemoveButton;
    
    IBOutlet NSButton *showErrorDetailButton;
    IBOutlet NSButton *rememberPositionButton;
    
    IBOutlet NSTextView *modelFieldView;
    IBOutlet NSTextField *modelAnsField;
    IBOutlet NSColorWell *frameWell, *fieldWell, *fontWell;
    IBOutlet NSPopUpButton *themes;
    
    IBOutlet NSTextField *versionLabel;
}

- (IBAction)toolbarAction:(id)sender;
- (IBAction)hotKeyChangeTriggered:(id)sender;
- (IBAction)reset:(id)sender;
- (IBAction)changeLanguage:(id)sender;
- (IBAction)addSymbol:(id)sender;
- (IBAction)removeSymbol:(id)sender;
- (IBAction)addFunction:(id)sender;
- (IBAction)removeFunction:(id)sender;
- (IBAction)loginChecked:(id)sender;
- (IBAction)errorDetailChecked:(id)sender;
- (IBAction)rememberButtonChecked:(id)sender;
- (IBAction)rememberLastChecked:(id)sender;
- (IBAction)escape:(id)sender;
- (IBAction)colorChanged:(id)sender;
- (IBAction)themeChanged:(id)sender;
- (IBAction)contact:(id)sender;
- (IBAction)openWebsite:(id)sender;
- (IBAction)help:(id)sender;
- (IBAction)faq:(id)sender;
- (IBAction)openHelpPage:(id)sender;

@property BOOL listening;
@property long prevTag;
@property NSUInteger currentModifierFlags, currentKeyCode;

@end
