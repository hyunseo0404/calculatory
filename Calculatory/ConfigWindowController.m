//
//  ConfigWindowController.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <ServiceManagement/ServiceManagement.h>

#import "ConfigWindowController.h"
#import "Constants.h"
#import "DCMathParser.h"

// predefined values for keys
#define kSpaceKeyCode 49
#define kEnterKeyCode 36

// identifier for the tabs
#define kGeneralTabIdentifier @"General"
#define kFeaturesTabIdentifier @"Features"
#define kAppearanceTabIdentifier @"Appearance"
#define kAdvancedTabIdentifier @"Advanced"
#define kUsageTabIdentifier @"Usage"
#define kAboutTabIdentifier @"About"

// tag for the tabs
#define kGeneralTabTag 0
#define kFeaturesTabTag 1
#define kAppearanceTabTag 2
#define kAdvancedTabTag 3
#define kUsageTabTag 4
#define kAboutTabTag 5

// macro for concatenating two strings
#define CONCAT(A,B) A = [A stringByAppendingString:B]

#define kHotKeyPlaceHolder NSLocalizedString(@"Press Any Combination of Keys", nil)

@implementation ConfigWindowController {
    const NSArray *RESERVED_VARS;
    const NSArray *RESERVED_FUNCS;
    const NSCharacterSet *alphanum;
}

@synthesize listening, prevTag;
@synthesize currentModifierFlags, currentKeyCode;

- (id) init {
    self = [super initWithWindowNibName:@"ConfigWindow"];
    RESERVED_VARS = @[ @"pi", @"e" ];
    RESERVED_FUNCS = @[ @"vec", @"vector", @"mag", @"norm", @"sin", @"cos", @"tan",
                        @"csc", @"sec", @"cot", @"sinh", @"cosh", @"tanh", @"asin",
                        @"acos", @"atan", @"acsc", @"asec", @"acot", @"asinh",
                        @"acosh", @"atanh", @"acsch", @"asech", @"acoth", @"log",
                        @"sqrt", @"ceil", @"floor", @"round", @"trunc", @"abs",
                        @"ftoc", @"ctof", @"ftok", @"ktof", @"ctok", @"ktoc",
                        @"m2s", @"s2m", @"m2h", @"h2m", @"h2s", @"s2h", @"nCr",
                        @"nPr", @"ncr", @"npr", @"max", @"min", @"gcd", @"gcf",
                        @"lcm", @"ln", @"log2", @"log10", @"dot", @"random",
                        @"randomr"  ];
    alphanum = [[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"] invertedSet];
    return self;
}

- (IBAction)toolbarAction:(id)sender
{
    if (listening) {
        [hotkeyButton setState:NSOffState];
        [self hotKeyChangeTriggered:hotkeyButton];
    }
    
    NSView *previousView = [self viewForTag:prevTag];
    NSView *newView = [self viewForTag:[sender tag]];
    prevTag = [sender tag];
    
    NSRect newFrame = [self newFrameForNewContentView:newView];
    
    [NSAnimationContext beginGrouping];
    
    if ([[NSApp currentEvent] modifierFlags] & NSShiftKeyMask)
        [[NSAnimationContext currentContext] setDuration:1.0];
    else [[NSAnimationContext currentContext] setDuration:0.15];
    
//    [[[[self window] contentView] animator] replaceSubview:previousView with:newView];
    [[[self window] animator] setFrame:newFrame display:YES];
    
    [NSAnimationContext endGrouping];
    
    [[[self window] contentView] replaceSubview:previousView with:newView];
}

- (NSView *)viewForTag:(long)tag
{
    NSView *view;
    switch (tag) {
        case kGeneralTabTag:
            view = general;
            [self updateGeneralTab];
            break;
        case kFeaturesTabTag:
            view = features;
            [self updateFeaturesTab];
            break;
        case kAppearanceTabTag:
            view = appearance;
            [self updateAppearanceTab];
            break;
        case kAdvancedTabTag:
            view = advanced;
            [self updateAdvancedTab];
            break;
        case kUsageTabTag:
            view = usage;
            break;
        case kAboutTabTag:
            view = about;
            [self updateAboutTab];
            break;
        default:
            break;
    }
    return view;
}

- (NSRect)newFrameForNewContentView:(NSView*)view
{
    NSWindow *window = [self window];
    NSRect newFrameRect = [window frameRectForContentRect:[view frame]];
    NSRect oldFrameRect = [window frame];
    NSSize newSize = newFrameRect.size;
    NSSize oldSize = oldFrameRect.size;
    
    NSRect frame = [window frame];
    frame.size = newSize;
    frame.origin.x -= (newSize.width - oldSize.width) / 2;
    frame.origin.y -= (newSize.height - oldSize.height);
    
    return frame;
}

- (IBAction)hotKeyChangeTriggered:(id)sender
{
    [self setListening:[sender state]];
    if (![sender state]) {
        if (![[hotkeyButton title] isEqualToString:kHotKeyPlaceHolder]) {
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setInteger:currentModifierFlags forKey:MODFLAG_KEY];
            [userDefaults setInteger:currentKeyCode forKey:KEYCODE_KEY];
            [userDefaults setObject:[hotkeyButton title] forKey:HOTKEY_KEY];
            [userDefaults synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:REG_HOTKEY_NOTE object:nil];
        } else {
            [hotkeyButton setTitle:[[NSUserDefaults standardUserDefaults] stringForKey:HOTKEY_KEY]];
        }
    } else [hotkeyButton setTitle:kHotKeyPlaceHolder];
}

- (IBAction)escape:(id)sender
{
    if ([[toolbar selectedItemIdentifier] isEqualToString:kGeneralTabIdentifier] && listening) {
        [hotkeyButton setTitle:kHotKeyPlaceHolder];
        [hotkeyButton setState:NSOffState];
        [self hotKeyChangeTriggered:hotkeyButton];
    }
}

- (void)keyDown:(NSEvent *)theEvent
{
    if ([[toolbar selectedItemIdentifier] isEqualToString:kGeneralTabIdentifier] && listening) {
        NSUInteger modFlags = [theEvent modifierFlags];
        if (modFlags == 256 && [theEvent keyCode] == kEnterKeyCode) {
            [hotkeyButton setState:NSOffState];
            [self hotKeyChangeTriggered:hotkeyButton];
            return;
        }
        
        // single character key down
        else if (modFlags == 256) return;
        
        NSString *hotkey = @"";
        
        if (modFlags & NSShiftKeyMask) CONCAT(hotkey, @"⇧ + ");
        if (modFlags & NSControlKeyMask) CONCAT(hotkey, @"⌃ + ");
        if (modFlags & NSAlternateKeyMask) CONCAT(hotkey, @"⌥ + ");
        if (modFlags & NSCommandKeyMask) CONCAT(hotkey, @"⌘ + ");
        if (modFlags & NSFunctionKeyMask) CONCAT(hotkey, @"Fn + ");
        
        NSUInteger keyCode = [theEvent keyCode];
        if (keyCode == kSpaceKeyCode) CONCAT(hotkey, @"Space");
        else if (keyCode == kEnterKeyCode) CONCAT(hotkey, @"Enter");
        else CONCAT(hotkey, [[theEvent charactersIgnoringModifiers] uppercaseString]);
        
        currentModifierFlags = modFlags;
        currentKeyCode = keyCode;
        [hotkeyButton setTitle:hotkey];
        [hotkeyButton setState:NSOffState];
        [self hotKeyChangeTriggered:hotkeyButton];
    }
}

- (IBAction)reset:(id)sender
{
    NSInteger choice = NSRunCriticalAlertPanel(NSLocalizedString(@"Reset",nil), NSLocalizedString(@"Are you sure you want to reset?\n(This will reset all the user settings for Calculatory, including functions and constants)",nil), NSLocalizedString(@"Cancel",nil), NSLocalizedString(@"Reset",nil), nil);
    if (choice == NSAlertAlternateReturn) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        BOOL tmpLogin = [userDefaults boolForKey:LOGIN_KEY];
        NSDictionary *userDefaultsDict = [userDefaults dictionaryRepresentation];
        for (NSString *key in userDefaultsDict.allKeys) {
            [userDefaults removeObjectForKey:key];
        }
        [userDefaults setBool:tmpLogin forKey:LOGIN_KEY];
        [userDefaults synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:REG_HOTKEY_NOTE object:nil];
        [self updateGeneralTab];
    }
}

- (void)awakeFromNib
{
    [self setupToolbar];
    [self.window setAcceptsMouseMovedEvents:NO];
}

- (void)windowWillClose:(NSNotification *)notification
{
    if (listening) {
        [hotkeyButton setState:NSOffState];
        [self hotKeyChangeTriggered:hotkeyButton];
    }
}

- (void)setupToolbar
{
    [toolbar setSelectedItemIdentifier:kGeneralTabIdentifier];
    
    if (general) {
        [[self window] setContentSize:general.frame.size];
        [[[self window] contentView] addSubview:general];
//        [[[self window] contentView] setWantsLayer:YES];
        [self updateGeneralTab];
    }
}

- (void)updateGeneralTab
{
    [hotkeyButton setFrameSize:NSMakeSize(hotkeyButton.frame.size.width, 25)];
    [hotkeyButton setTitle:[[NSUserDefaults standardUserDefaults] stringForKey:HOTKEY_KEY]];
    NSString *lang = [[NSUserDefaults standardUserDefaults] stringForKey:PREFLANG_KEY];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:lang];
    [localization selectItemWithTitle:[locale displayNameForKey:NSLocaleIdentifier value:lang]];
}

- (void)updateFeaturesTab
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [sigfig selectItemWithTag:[userDefaults integerForKey:MAX_FRAC_DIGIT_KEY]];
}

- (void)updateAppearanceTab
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSColor *frameColor = [NSColor colorWithCalibratedRed:[defaults floatForKey:FRAME_RED_KEY] green:[defaults floatForKey:FRAME_GREEN_KEY] blue:[defaults floatForKey:FRAME_BLUE_KEY] alpha:1];
    NSColor *fieldColor = [NSColor colorWithCalibratedRed:[defaults floatForKey:FIELD_RED_KEY] green:[defaults floatForKey:FIELD_GREEN_KEY] blue:[defaults floatForKey:FIELD_BLUE_KEY] alpha:1];
    NSColor *fontColor = [NSColor colorWithCalibratedRed:[defaults floatForKey:FONT_RED_KEY] green:[defaults floatForKey:FONT_GREEN_KEY] blue:[defaults floatForKey:FONT_BLUE_KEY] alpha:1];
    [frameWell setColor:frameColor];
    [fieldWell setColor:fieldColor];
    [fontWell setColor:fontColor];
    [modelFieldView setBackgroundColor:fieldColor];
    [modelFieldView setTextColor:fontColor];
    [modelAnsField setTextColor:fontColor];
    for (int i = 0; i < NUM_THEMES; i++) {
        [themes insertItemWithTitle:NSLocalizedString(THEME_NAMES[i],nil) atIndex:i];
    }
    [themes insertItemWithTitle:NSLocalizedString(@"Custom",nil) atIndex:NUM_THEMES];
    [themes selectItemAtIndex:[defaults integerForKey:THEME_KEY]];
}

- (void)updateAdvancedTab
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [showErrorDetailButton setState:[userDefaults integerForKey:ERR_DETAIL_KEY]];
    [rememberPositionButton setState:![[userDefaults stringForKey:REMEMBER_POS_KEY] isEqualToString:@""]];
}

- (void)updateAboutTab
{
    versionLabel.stringValue = [NSString stringWithFormat:@"v%@",
                                [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
}

- (IBAction)changeLanguage:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lang = [[sender selectedItem] toolTip];
    if ([lang isEqualToString:[defaults stringForKey:PREFLANG_KEY]]) return;
    [defaults setObject:[NSArray arrayWithObject:lang] forKey:@"AppleLanguages"];
    [defaults setObject:lang forKey:PREFLANG_KEY];
    [defaults synchronize];
    NSRunInformationalAlertPanel(NSLocalizedString(@"Localization Change",nil), NSLocalizedString(@"Please restart the app for the localization change to take an effect.",nil), NSLocalizedString(@"OK",nil), nil, nil);
}

- (IBAction)loginChecked:(id)sender
{
    if ([sender state] == NSOnState) {
        // turn on launch at login
        if (!SMLoginItemSetEnabled((__bridge CFStringRef)@"com.hdc.CalculatoryLauncher", YES)) {
            NSAlert *alert = [NSAlert alertWithMessageText:@"Error adding Calculatory to login items"
                                             defaultButton:@"Cancel"
                                           alternateButton:nil
                                               otherButton:nil
                                 informativeTextWithFormat:@"Failed to add Calculatory to login items :(\nPlease contact if this error continues."];
            [alert runModal];
        }
    } else {
        // turn off launch at login
        if (!SMLoginItemSetEnabled((__bridge CFStringRef)@"com.hdc.CalculatoryLauncher", NO)) {
            NSAlert *alert = [NSAlert alertWithMessageText:@"Error removing Calculatory from login items"
                                             defaultButton:@"Cancel"
                                           alternateButton:nil
                                               otherButton:nil
                                 informativeTextWithFormat:@"Failed to remove Calculatory from login items :(\nPlease contact if this error continues."];
            [alert runModal];
        }
    }
}

- (IBAction)addSymbol:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict;
    if ([defaults objectForKey:VAR_KEY]) dict = [[defaults objectForKey:VAR_KEY] mutableCopy];
    else dict = [[NSMutableDictionary alloc] init];
    NSString *key = @"new";
    if ([dict objectForKey:key]) {
        int count = 2;
        do {
            if (count > 100) NSRunCriticalAlertPanel(NSLocalizedString(@"Failed to Add New Constant",nil), NSLocalizedString(@"Please rename or remove some of the new constants.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
            key = [NSString stringWithFormat:@"new%d",count];
            count++;
        } while ([dict objectForKey:key]);
    }
    [dict setObject:@(0) forKey:key];
    [defaults setObject:dict.copy forKey:VAR_KEY];
    [defaults synchronize];
    [symbolTable reloadData];
}

- (IBAction)addFunction:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *custFunc;
    if ([defaults objectForKey:CUST_FUNC_KEY]) custFunc = [[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:CUST_FUNC_KEY]] mutableCopy];
    else custFunc = [[NSMutableDictionary alloc] init];
    NSString *key = @"new";
    if ([custFunc objectForKey:key]) {
        int count = 2;
        do {
            if (count > 100) NSRunCriticalAlertPanel(NSLocalizedString(@"Failed to Add New Function",nil), NSLocalizedString(@"Please rename or remove some of the new functions.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
            key = [NSString stringWithFormat:@"new%d",count];
            count++;
        } while ([custFunc objectForKey:key]);
    }
    [dict setObject:@"@0" forKey:CUST_FUNC_FUNC];
    [dict setObject:@(1) forKey:CUST_FUNC_ARGS];
    [dict setObject:@(key.hash) forKey:CUST_FUNC_ID];
    [dict setObject:key forKey:CUST_FUNC_NAME];
    [custFunc setObject:dict.copy forKey:key];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:custFunc.copy];
    [defaults setObject:data forKey:CUST_FUNC_KEY];
    [defaults synchronize];
    [functionTable reloadData];
}

- (BOOL)changeSymbolWithName:(NSString*)name forName:(NSString*)newName andValue:(NSString*)newValue nameChange:(BOOL)flag
{
    if (newName.length == 0) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Name",nil), NSLocalizedString(@"Please enter a constant name.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if ([newName rangeOfCharacterFromSet:(NSCharacterSet *)alphanum].location != NSNotFound) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Name",nil), NSLocalizedString(@"A constant name must only contain alphabets, numbers, and an underscore",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    char fChar = [newName characterAtIndex:0];
    if (!((fChar >= 'a' && fChar <= 'z') || (fChar >= 'A' && fChar <= 'Z'))) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Name",nil), NSLocalizedString(@"A constant name must start with an alphabet.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if (flag && [RESERVED_VARS containsObject:newName]) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Name",nil), NSLocalizedString(@"The name conflicts with a reserved constant name.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *vars;
    if ([defaults objectForKey:VAR_KEY]) {
        vars = [[defaults objectForKey:VAR_KEY] mutableCopy];
        if (flag && [vars objectForKey:newName]) {
            NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Name",nil), NSLocalizedString(@"The name conflicts with an existing constant name.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
            return NO;
        }
    } else vars = [[NSMutableDictionary alloc] init];
    if (newValue.length == 0) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Value",nil), NSLocalizedString(@"Please enter a value for the constant.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    DCMathParser *parser = [[DCMathParser alloc] initWith:newValue];
    id ans;
    @try {
        ans = [parser parse];
    } @catch (NSException *exception) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Value",nil), NSLocalizedString(@"Invalid value.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if (![ans isKindOfClass:[Numbers class]]) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Constant Value",nil), NSLocalizedString(@"Invalid value.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    double val = [ans doubleValue];
    if (flag) [vars removeObjectForKey:name];
    [vars setObject:@(val) forKey:newName];
    [defaults setObject:vars.copy forKey:VAR_KEY];
    [defaults synchronize];
    if ([[defaults objectForKey:VAR_KEY] objectForKey:newName]) return YES;
    else {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Unknown Error",nil), NSLocalizedString(@"Failed to add the constant.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
}

- (BOOL)changeFunctionWithName:(NSString *)name forName:(NSString*)newName andFormula:(NSString*)newFormula nameChange:(BOOL)flag
{
    if (newName.length == 0) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Name",nil), NSLocalizedString(@"Please enter a function name.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if ([newName rangeOfCharacterFromSet:(NSCharacterSet *)alphanum].location != NSNotFound) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Name",nil), NSLocalizedString(@"A function name must only contain alphabets, numbers, and an underscore",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    char fChar = [newName characterAtIndex:0];
    if (!((fChar >= 'a' && fChar <= 'z') || (fChar >= 'A' && fChar <= 'Z'))) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Name",nil), NSLocalizedString(@"A function name must start with an alphabet.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if (newFormula.length == 0) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Formula",nil), NSLocalizedString(@"Please enter a formula for the function.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if (flag && [RESERVED_FUNCS containsObject:newName]) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Name",nil), NSLocalizedString(@"The name conflicts with a reserved function name.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@\\d+" options:0 error:nil];
    NSArray *matches = [regex matchesInString:newFormula options:0 range:NSMakeRange(0, newFormula.length)];
    NSMutableArray *args = [[NSMutableArray alloc] init];
    for (NSTextCheckingResult *match in matches) {
        [args addObject:@([[[newFormula substringWithRange:[match rangeAtIndex:0]] substringFromIndex:1] intValue])];
    }
    [args sortUsingSelector:@selector(compare:)];
    NSOrderedSet *orderedArgs = [NSOrderedSet orderedSetWithArray:args];
    if ([[orderedArgs lastObject] intValue] != orderedArgs.count-1) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Formula",nil), NSLocalizedString(@"Please use all the arguments (@0 ~ @n).",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    int maxArg = args.count > 0 ? ([[args lastObject] intValue]+1) : 0;
    if ([newFormula rangeOfString:newName].location != NSNotFound) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Formula",nil), NSLocalizedString(@"Recursive function is not allowed.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    if (![self testFunction:newFormula]) {
        NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Formula",nil), NSLocalizedString(@"Invalid formula.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
        return NO;
    }
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:newFormula forKey:CUST_FUNC_FUNC];
    [dict setObject:@(maxArg) forKey:CUST_FUNC_ARGS];
    [dict setObject:@(newName.hash) forKey:CUST_FUNC_ID];
    [dict setObject:newName forKey:CUST_FUNC_NAME];
    NSMutableDictionary *custFunc;
    if ([def objectForKey:CUST_FUNC_KEY]) {
        custFunc = [[NSKeyedUnarchiver unarchiveObjectWithData:[def objectForKey:CUST_FUNC_KEY]] mutableCopy];
        if (flag && [custFunc objectForKey:newName]) {
            NSRunCriticalAlertPanel(NSLocalizedString(@"Invalid Function Name",nil), NSLocalizedString(@"The name conflicts with an existing function name.",nil), NSLocalizedString(@"Cancel",nil), nil, nil);
            return NO;
        }
    } else custFunc = [[NSMutableDictionary alloc] init];
    if (flag) [custFunc removeObjectForKey:name];
    [custFunc setObject:dict.copy forKey:newName];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:custFunc.copy];
    [def setObject:data forKey:CUST_FUNC_KEY];
    [def synchronize];
    return YES;
}

- (IBAction)removeSymbol:(id)sender
{
    NSIndexSet *selected = [symbolTable selectedRowIndexes];
    NSTableColumn *column = [symbolTable tableColumnWithIdentifier:SYMB_NAME_COLUMN_IDENT];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [[defaults objectForKey:VAR_KEY] mutableCopy];
    [selected enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [dict removeObjectForKey:[self tableView:symbolTable objectValueForTableColumn:column row:idx]];
    }];
    [defaults setObject:dict.copy forKey:VAR_KEY];
    [defaults synchronize];
    [symbolTable reloadData];
}

- (IBAction)removeFunction:(id)sender
{
    NSIndexSet *selected = [functionTable selectedRowIndexes];
    NSTableColumn *column = [functionTable tableColumnWithIdentifier:FUNC_NAME_COLUMN_IDENT];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dict = [[NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:CUST_FUNC_KEY]] mutableCopy];
    [selected enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [dict removeObjectForKey:[self tableView:functionTable objectValueForTableColumn:column row:idx]];
    }];
    [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:dict.copy] forKey:CUST_FUNC_KEY];
    [defaults synchronize];
    [functionTable reloadData];
}

- (BOOL)testFunction:(NSString *)formula
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@\\d+" options:0 error:nil];
    NSString *form = [regex stringByReplacingMatchesInString:formula options:0 range:NSMakeRange(0, formula.length) withTemplate:@"1"];
    DCMathParser *parser = [[DCMathParser alloc] initWith:form];
    @try {
        id parsed = [parser parse];
        if ([parsed isKindOfClass:[NSError class]]) return NO;
        else return YES;
    } @catch (NSException *exception) {
        return NO;
    }
}

- (IBAction)errorDetailChecked:(id)sender
{
    NSInteger state = [sender state];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:state forKey:ERR_DETAIL_KEY];
    [defaults synchronize];
}

- (IBAction)rememberButtonChecked:(id)sender
{
    NSInteger state = [sender state];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:state == NSOnState ? @"quickModeWindow" : @"" forKey:REMEMBER_POS_KEY];
    [defaults synchronize];
}

- (IBAction)rememberLastChecked:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:REMEMBER_LAST_STR_KEY];
    [defaults synchronize];
}

- (void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == symbolSearch) [symbolTable reloadData];
    else if ([obj object] == functionSearch) [functionTable reloadData];
}

- (NSInteger)numberOfRowsInTableView:(NSTableView *)tableView
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (tableView == symbolTable) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [symbolSearch stringValue]];
        NSDictionary *dict = [defaults objectForKey:VAR_KEY];
        if (!dict) return 0;
        else if (![[symbolSearch stringValue] isEqualToString:@""])
            return [[[dict allKeys] filteredArrayUsingPredicate:predicate] count];
        else return [dict count];
    } else if (tableView == functionTable) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [functionSearch stringValue]];
        NSData *archive = [defaults objectForKey:CUST_FUNC_KEY];
        if (!archive) return 0;
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:archive];
        if ([[functionSearch stringValue] isEqualToString:@""]) return [dict count];
        NSMutableArray *keys = [[dict allValues] mutableCopy];
        for (int i = 0; i < keys.count; i++) {
            [keys setObject:[[keys objectAtIndex:i] objectForKey:CUST_FUNC_NAME] atIndexedSubscript:i];
        }
        return [[keys filteredArrayUsingPredicate:predicate] count];
    } else return 0;
}

- (id)tableView:(NSTableView *)tableView objectValueForTableColumn:(NSTableColumn *)tableColumn row:(NSInteger)row
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (tableView == symbolTable) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [symbolSearch stringValue]];
        NSDictionary *dict = [defaults objectForKey:VAR_KEY];
        NSArray *sortedKeys = [([[symbolSearch stringValue] isEqualToString:@""] ? [dict allKeys] : [[dict allKeys] filteredArrayUsingPredicate:predicate])
                               sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        if ([tableColumn.identifier isEqualToString:SYMB_NAME_COLUMN_IDENT]) {
            return [sortedKeys objectAtIndex:row];
        } else if ([tableColumn.identifier isEqualToString:SYMB_VAL_COLUMN_IDENT]) {
            return [dict objectForKey:[sortedKeys objectAtIndex:row]];
        } else return nil;
    } else if (tableView == functionTable) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@", [functionSearch stringValue]];
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:CUST_FUNC_KEY]];
        NSArray *sortedKeys = [([[functionSearch stringValue] isEqualToString:@""] ? [dict allKeys] : [[dict allKeys] filteredArrayUsingPredicate:predicate])
                               sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        if ([tableColumn.identifier isEqualToString:FUNC_NAME_COLUMN_IDENT]) {
            return [sortedKeys objectAtIndex:row];
        } else if ([tableColumn.identifier isEqualToString:FUNC_FORM_COLUMN_IDENT]) {
            NSMutableAttributedString *formula = [[NSMutableAttributedString alloc] initWithString:[[dict objectForKey:[sortedKeys objectAtIndex:row]] objectForKey:CUST_FUNC_FUNC]];
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@\\d+" options:0 error:nil];
            NSArray *matches = [regex matchesInString:formula.string options:0 range:NSMakeRange(0, formula.length)];
            for (NSTextCheckingResult *match in matches) {
                NSRange range = [match rangeAtIndex:0];
                [formula addAttribute:NSForegroundColorAttributeName value:[NSColor blueColor] range:range];
            }
            return formula;
        } else return nil;
    } else return nil;
}

- (BOOL)control:(NSControl *)control textView:(NSTextView *)textView doCommandBySelector:(SEL)commandSelector
{
    if (commandSelector == @selector(cancelOperation:)) {
        [control abortEditing];
        return YES;
    } else return NO;
}

- (BOOL)control:(NSControl *)control textShouldEndEditing:(NSText *)fieldEditor
{
    NSString *edited = [fieldEditor string];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (control == symbolTable) {
        NSInteger editedColumn = [symbolTable editedColumn];
        NSInteger editedRow = [symbolTable editedRow];
        NSDictionary *dict = [defaults objectForKey:VAR_KEY];
        NSArray *sortedKeys = [[dict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        NSString *name = [sortedKeys objectAtIndex:editedRow];
        NSString *value = [[dict objectForKey:name] stringValue];
        if (editedColumn == 0) {
            if ([self changeSymbolWithName:name forName:edited andValue:value nameChange:YES]) return YES;
            else return NO;
        } else if (editedColumn == 1) {
            if ([self changeSymbolWithName:name forName:name andValue:edited nameChange:NO]) return YES;
            else return NO;
        } else return NO;
    } else if (control == functionTable) {
        NSInteger editedColumn = [functionTable editedColumn];
        NSInteger editedRow = [functionTable editedRow];
        NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithData:[defaults objectForKey:CUST_FUNC_KEY]];
        NSArray *sortedKeys = [[dict allKeys] sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
        NSString *name = [sortedKeys objectAtIndex:editedRow];
        NSString *formula = [[dict objectForKey:name] objectForKey:CUST_FUNC_FUNC];
        if (editedColumn == 0) {
            if ([self changeFunctionWithName:name forName:edited andFormula:formula nameChange:YES]) return YES;
            else return NO;
        } else if (editedColumn == 1) {
            if ([self changeFunctionWithName:name forName:name andFormula:edited nameChange:NO]) return YES;
            else return NO;
        } else return NO;
    }
    return YES;
}

- (IBAction)colorChanged:(id)sender
{
    [themes selectItemAtIndex:NUM_THEMES];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (sender == frameWell) {
        NSColor *color = [[frameWell color] colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
        [defaults setFloat:color.redComponent forKey:FRAME_RED_KEY];
        [defaults setFloat:color.greenComponent forKey:FRAME_GREEN_KEY];
        [defaults setFloat:color.blueComponent forKey:FRAME_BLUE_KEY];
    } else if (sender == fieldWell) {
        NSColor *color = [[fieldWell color] colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
        [defaults setFloat:color.redComponent forKey:FIELD_RED_KEY];
        [defaults setFloat:color.greenComponent forKey:FIELD_GREEN_KEY];
        [defaults setFloat:color.blueComponent forKey:FIELD_BLUE_KEY];
        [modelFieldView setBackgroundColor:color];
    } else if (sender == fontWell) {
        NSColor *color = [[fontWell color] colorUsingColorSpaceName:NSCalibratedRGBColorSpace];
        [defaults setFloat:color.redComponent forKey:FONT_RED_KEY];
        [defaults setFloat:color.greenComponent forKey:FONT_GREEN_KEY];
        [defaults setFloat:color.blueComponent forKey:FONT_BLUE_KEY];
        [modelFieldView setTextColor:color];
        [modelAnsField setTextColor:color];
    } else return;
    [defaults setInteger:NUM_THEMES forKey:THEME_KEY];
    [defaults synchronize];
    [[self.window contentView] setNeedsDisplay:YES];
}

- (IBAction)themeChanged:(id)sender
{
    int index = (int)[themes indexOfSelectedItem];
    if (index == NUM_THEMES) return;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:index forKey:THEME_KEY];
    [defaults setFloat:THEMES[index][FRAME_R] forKey:FRAME_RED_KEY];
    [defaults setFloat:THEMES[index][FRAME_G] forKey:FRAME_GREEN_KEY];
    [defaults setFloat:THEMES[index][FRAME_B] forKey:FRAME_BLUE_KEY];
    [defaults setFloat:THEMES[index][FIELD_R] forKey:FIELD_RED_KEY];
    [defaults setFloat:THEMES[index][FIELD_G] forKey:FIELD_GREEN_KEY];
    [defaults setFloat:THEMES[index][FIELD_B] forKey:FIELD_BLUE_KEY];
    [defaults setFloat:THEMES[index][FONT_R] forKey:FONT_RED_KEY];
    [defaults setFloat:THEMES[index][FONT_G] forKey:FONT_GREEN_KEY];
    [defaults setFloat:THEMES[index][FONT_B] forKey:FONT_BLUE_KEY];
    [defaults synchronize];
    [self updateAppearanceTab];
    [[self.window contentView] setNeedsDisplay:YES];
}

- (IBAction)contact:(id)sender
{
    NSString *subject = @"Inquiry about Calculatory";
    NSString *body = [NSString stringWithFormat:@"\n\n============================\nTime Stamp: %@\nOS: Mac OS %@\nCalculatory: v%@ b%@\n",
                      [NSDate date],
                      [[NSProcessInfo processInfo] operatingSystemVersionString],
                      [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],
                      [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    NSString *to = @"contact@calculatoryapp.com";
    NSString *encodedSubject = [NSString stringWithFormat:@"SUBJECT=%@", [subject stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *encodedBody = [NSString stringWithFormat:@"BODY=%@", [body stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSString *encodedTo = [to stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *encodedURLString = [NSString stringWithFormat:@"mailto:%@?%@&%@", encodedTo, encodedSubject, encodedBody];
    NSURL *mailtoURL = [NSURL URLWithString:encodedURLString];
    [[NSWorkspace sharedWorkspace] openURL:mailtoURL];
}

- (IBAction)openWebsite:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/"]];
}

- (IBAction)help:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/"]];
}

- (IBAction)faq:(id)sender
{
    [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#faqModal"]];
}

- (IBAction)openHelpPage:(id)sender
{
    NSString *identifier = [sender identifier];
    if ([identifier isEqualToString:@"General"]) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#basicTopics"]];
    } else if ([identifier isEqualToString:@"Features"]) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#basicTopics"]];
    } else if ([identifier isEqualToString:@"Constants"]) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#customConstModal"]];
    } else if ([identifier isEqualToString:@"Functions"]) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#customFnModal"]];
    } else if ([identifier isEqualToString:@"Appearance"]) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#appearanceTopics"]];
    } else if ([identifier isEqualToString:@"Advanced"]) {
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#advancedModal"]];
    }
}

@end
