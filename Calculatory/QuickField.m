//
//  QuickField.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 5. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "QuickField.h"
#import "QuickModeController.h"
#import "Constants.h"

@implementation QuickField

- (void)awakeFromNib
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults boolForKey:MINI_MODE_KEY]) [super setTextContainerInset:NSMakeSize(0, 6.0f)];
    else [super setTextContainerInset:NSMakeSize(0, 3.0f)];
    [self setFont:[NSFont fontWithName:@"Lucida Grande" size:35.0]];
    [self setTextColor:[NSColor colorWithCalibratedRed:[defaults floatForKey:FONT_RED_KEY] green:[defaults floatForKey:FONT_GREEN_KEY] blue:[defaults floatForKey:FONT_BLUE_KEY] alpha:1]];
    NSColor *color = [NSColor colorWithCalibratedRed:[defaults floatForKey:FIELD_RED_KEY] green:[defaults floatForKey:FIELD_GREEN_KEY] blue:[defaults floatForKey:FIELD_BLUE_KEY] alpha:1];
    [self setBackgroundColor:color];
    [self setAutomaticDashSubstitutionEnabled:NO];
//    [self setHorizontallyResizable:NO];
    if ([defaults boolForKey:REMEMBER_LAST_KEY]) {
        NSString *last = [defaults stringForKey:REMEMBER_LAST_STR_KEY];
        if (last.length) {
            [self setString:last];
            [self.delegate textDidChange:[NSNotification notificationWithName:NSTextDidChangeNotification object:self]];
        }
    }
}

- (BOOL)performKeyEquivalent:(NSEvent *)theEvent
{
    if (([theEvent modifierFlags] & NSCommandKeyMask) && [theEvent keyCode] >= 18 && [theEvent keyCode] <= 21) {
        [((QuickModeController *) self.delegate) changeNumSysShortcut:[theEvent keyCode]];
        return YES;
    }
    return NO;
}

- (void)keyDown:(NSEvent *)theEvent
{
    NSUInteger modifiers = [theEvent modifierFlags];
    if ([theEvent keyCode] == 36) { // Enter Key
        if ((modifiers & NSShiftKeyMask)) {
            [self insertNewline:nil];
            return;
        }
    } else if ([theEvent keyCode] == 48) { // Tab Key
        if ((modifiers & NSAlternateKeyMask)) {
            [self insertTab:nil];
            return;
        }
    }
    
    BOOL changed = self.selectedRange.location;
    
    [super keyDown:theEvent];
    
    changed = self.selectedRange.location != changed;
    
    if ((modifiers & NSShiftKeyMask) && [theEvent keyCode] == 25) { // left parenthesis
        NSUInteger selloc = NSNotFound;
        if (([self parenthesisIsMatched] || ![self matchParenthesis:&selloc :YES :NO]) && [[NSUserDefaults standardUserDefaults] boolForKey:AUTO_PAREN_KEY]) {
            [self insertText:@")"];
            [self setSelectedRange:NSMakeRange(self.selectedRange.location-1, 0)];
        } else if (selloc != NSNotFound) {
            [self.textStorage addAttribute:NSBackgroundColorAttributeName value:[NSColor yellowColor] range:NSMakeRange(selloc, 1)];
            [self performSelector:@selector(removeHighlights) withObject:nil afterDelay:0.2];
        }
    } else if ((modifiers & NSShiftKeyMask) && [theEvent keyCode] == 29) { // right parenthesis
        NSUInteger selloc = NSNotFound;
        if (![self matchParenthesis:&selloc :NO :NO] && selloc != NSNotFound) {
            [self.textStorage addAttribute:NSBackgroundColorAttributeName value:[NSColor yellowColor] range:NSMakeRange(selloc, 1)];
            [self performSelector:@selector(removeHighlights) withObject:nil afterDelay:0.2];
        }
    } else if (changed && modifiers == 0xa00100) { // NSNumericPadKeyMask
        NSUInteger selloc = NSNotFound;
        if ([theEvent keyCode] == 123) { // left arrow key
            char paren = [self.string characterAtIndex:self.selectedRange.location];
            if (paren == '(' || (paren == ')' && self.selectedRange.location)) {
                [self matchParenthesis:&selloc :(paren == '(') :YES];
                if (selloc != NSNotFound) {
                    [self.textStorage addAttribute:NSBackgroundColorAttributeName value:[NSColor yellowColor] range:NSMakeRange(selloc, 1)];
                    [self performSelector:@selector(removeHighlights) withObject:nil afterDelay:0.2];
                }
            }
        } else if ([theEvent keyCode] == 124) { // right arrow key
            char paren = [self.string characterAtIndex:self.selectedRange.location-1];
            if ((paren == '(' && self.selectedRange.location - 1 != self.string.length) || paren == ')') {
                [self matchParenthesis:&selloc :(paren == '(') :YES :-1];
                if (selloc != NSNotFound) {
                    [self.textStorage addAttribute:NSBackgroundColorAttributeName value:[NSColor yellowColor] range:NSMakeRange(selloc, 1)];
                    [self performSelector:@selector(removeHighlights) withObject:nil afterDelay:0.2];
                }
            }
        }
    }
}

- (int)matchParenthesis:(NSUInteger*)selloc :(BOOL)left :(BOOL)highlight :(int)option
{
    // look for '(' when flag is true, otherwise look for ')'
    int pcount = 0;
    NSString *afstr = self.string;
    for (long i = self.selectedRange.location - (!highlight && !left) + option; (left ? i < afstr.length : i >= 0); (i += left ? 1 : -1)) {
        if ([afstr characterAtIndex:i] == '(') pcount++;
        else if ([afstr characterAtIndex:i] == ')') pcount--;
        if ((left && pcount < 0) || (!left && pcount > 0) || (highlight && !pcount)) {
            *selloc = i;
            break;
        }
    }
    return pcount;
}

- (int)matchParenthesis:(NSUInteger*)selloc :(BOOL)left :(int)highlight
{
    return [self matchParenthesis:selloc :left :highlight :0];
}

- (BOOL)parenthesisIsMatched
{
    int pcount = -1;
    NSString *afstr = self.string;
    for (long i = 0; i < afstr.length; i++) {
        if ([afstr characterAtIndex:i] == '(') pcount++;
        else if ([afstr characterAtIndex:i] == ')') pcount--;
    }
    return pcount == 0;
}

- (void)removeHighlights
{
    [self.textStorage removeAttribute:NSBackgroundColorAttributeName range:NSMakeRange(0, self.textStorage.length)];
}

@end
