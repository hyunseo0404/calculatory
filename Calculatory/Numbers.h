//
//  Numbers.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"

@interface Numbers : Calculator

@property long double n;

- (id) init;
- (id) initWith: (long double) num;
- (id) initWithString:(NSString *)str;
- (void) setTo:(long double) num;
- (double) doubleValue;
- (long double) longDoubleValue;
- (NSString *) print:(NSString *) fmt;
- (Numbers *) add:(Numbers *) num;
- (Numbers *) subtract:(Numbers *) num;
- (id) multiply:(id) num;
- (id) divide:(id) num;
- (Numbers *) exponentiate:(Numbers *) num;
- (Numbers *) factorial;
- (Numbers *) modulo:(Numbers *) num;
- (Numbers *) chooseR:(Numbers *) num;
- (Numbers *) pickR:(Numbers *) num;
- (Numbers *) gcd:(Numbers *) num;
- (Numbers *) lcm:(Numbers *) num;
- (Numbers *) shiftLeft:(Numbers *)num;
- (Numbers *) shiftRight:(Numbers *)num;
- (Numbers *) shiftRightUnsigned:(Numbers *)num;
- (Numbers *) bitwiseAnd:(Numbers *)num;
- (Numbers *) bitwiseXor:(Numbers *)num;
- (Numbers *) bitwiseOr:(Numbers *)num;
- (Numbers *) negate;
- (Numbers *) not;
- (Numbers *) negative;

@end
