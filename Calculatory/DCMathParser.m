//
//  DCMathParser.m
//  Calculatory
//
//  Created by Dennis Chung on 13. 1. 10..
//  Copyright (c) 2013년 hyunseo0404@gmail.com. All rights reserved.
//
//  GRAMMAR RULES ==
//
//      POSSIBLE TOKENS:
//
//          1. Number: 0 ~ 9 with possible '.', 'e', 'E', '+', and/or '-', only once for each.
//                     '+' and '-' represents a unary operator, and has to appear in the very beginning (if any).
//          2. Operator: '+', '-', '*', '/', '!', '%', 'X', '^', '**', '<<', '>>', '>>>', '.', '&', '|', and '~' possible.
//                       Note: '**' -> '`' for easier handling.
//                       An operator has to have a legit number before (and usually after as well) itself.
//          3. Function: A function to calculate a special formula (e.g. sin() and sqrt()).
//                       A function name must start with an alphabet.
//                       Number of arguments for each function can vary.
//          4. Symbol: A set of pre-defined symbols can be used (e.g. pi and e).
//                     Symbols will be converted into the corresponding number values in the beginning.
//          5. Vector: Must start with a single '<' and end with a single '>',
//                     and contain exactly three arguments, each separated with a comma.
//          6. Parenthesis: '(' and ')' will make sure the expression inside will be evaluated first.
//                          '[', ']', '{', and '}' will all be converted to either '(' or ')'.
//          7. Comparator: '==' will compare the expressions on the left side and the right side,
//                         and return if their evalutated values are the same.
//                         There can be infinite number of '==' as long as there are some expressions on both sides.
//
//      - All numbers, functions, symbols, and vectors have to have an operator before and after each of themselves,
//        before any other form of these can follow.
//      - The result will only end up in either a number format or a vector format.
//      - After evaluation, the number(s) will be represented as a long double value.
//      - This math parser will use Dijkstra's shunting yard algorithm to convert the expression into a Reverse Polish Notation (RPN),
//        and then use the converted expression in the RPN notation to parse and evaluate.
//      - Any incorrect grammar not following any of the above rules will return an error.
//      - The number of parentheses must match in order to continue parsing.
//      - An implicit multiplication in supported (e.g. 3(1+2)4 would be a valid expression).
//

#import "DCMathParser.h"
#import "Constants.h"

#pragma mark Constants

#define kFirstArgPos 0
#define kSecondArgPos 1
#define kOperPos 2

#define kOpenParen @"("
#define kCloseParen @")"
#define kOpenVec @"<"
#define kCloseVec @">"
#define kArgSep @","

// pre-calculated constants (pi and e) as long double's
#define M_PI_L 3.1415926535897932384626433832795L
#define M_E_L 2.7182818284590452353602874713526L

#pragma mark - Macros

#define IS_OPEN_PAREN(O) ([(O) isMemberOfClass:[Operator class]] && [(O) numericValue] == OPEN_PAREN)
#define IS_CLOSE_PAREN(O) ([(O) isMemberOfClass:[Operator class]] && [(O) numericValue] == CLOSE_PAREN)
#define ID_TO_OPER(O) ([(O) isMemberOfClass:[Operator class]] ? ((Operators) [(O) numericValue]) : -1)

// converts the first character of a string to a char
#define STR_TO_CHAR(STR) ([(STR) UTF8String][0])

// creates a number with the given value
#define CREATE_NUM(N) ([[Numbers alloc] initWith:(N)])

// unarchive NSData for the custom function dictionary
#define UNARCHIVE(DATA) ([NSKeyedUnarchiver unarchiveObjectWithData:(DATA)])

#pragma mark - Private Function Class

@interface Function : NSObject

@property NSUInteger func;
@property NSString *custFunc;
@property NSDictionary *custom;

@end

@implementation Function

@synthesize func, custom;

- (id)init
{
    return [self initWith:ERROR];
}

- (id)initWith:(Functions)fn
{
    return [self initWithFunction:fn];
}

- (id)initWithFunction:(NSUInteger)fn
{
    self = [super init];
    if (self) {
        func = fn;
    }
    return self;
}

- (id)initWithCustomFunction:(NSString *)custFunc
{
    self = [super init];
    if (self) {
        custom = [UNARCHIVE([[NSUserDefaults standardUserDefaults] objectForKey:CUST_FUNC_KEY]) objectForKey:custFunc];
        func = custFunc.hash;
    }
    return self;
}

- (BOOL)isEqual:(id)object
{
    return [self isEqualTo:object];
}

- (BOOL)isEqualTo:(id)object
{
    if (![object isMemberOfClass:[Function class]]) return NO;
    return func == [object func];
}

- (NSUInteger)numericValue
{
    return func;
}

#pragma mark === FUNCTION ===
- (int)argsNeeded
{
    if (custom) {
        return [[custom objectForKey:@"numArgs"] intValue];
    }
    
    switch (func) {
        case VECTOR:
            return 3;
        case LOG:
        case MAX: case MIN:
        case COMB: case PERM:
        case GCD: case GCF: case LCM: case DOT: case RANDOMR:
            return 2;
        default:
            return 1;
    }
}

#pragma mark === FUNCTION ===
NSString * const Functions_toString[] = {
    [VECTOR] = @"VECTOR", [MAG] = @"mag", [NORM] = @"norm",
    [SIN] = @"sin", [COS] = @"cos", [TAN] = @"tan",
    [CSC] = @"csc", [SEC] = @"sec", [COT] = @"cot",
    [SINH] = @"sinh", [COSH] = @"cosh", [TANH] = @"tanh",
    [ASIN] = @"asin", [ACOS] = @"acos", [ATAN] = @"atan",
    [ACSC] = @"acsc", [ASEC] = @"asec", [ACOT] = @"acot",
    [ASINH] = @"asinh", [ACOSH] = @"acosh", [ATANH] = @"atanh",
    [ACSCH] = @"acsch", [ASECH] = @"asech", [ACOTH] = @"acoth",
    [LOG] = @"log", [SQRT] = @"sqrt", [CEIL] = @"ceil", [FLOOR] = @"floor",
    [ROUND] = @"round", [TRUNC] = @"trunc", [ABS] = @"abs",
    [FTOC] = @"ftoc", [CTOF] = @"ctof", [FTOK] = @"ftok", [KTOF] = @"ktof",
    [CTOK] = @"ctok", [KTOC] = @"ktoc", [M2S] = @"m2s", [S2M] = @"s2m",
    [M2H] = @"m2h", [H2M] = @"h2m", [H2S] = @"h2s", [S2H] = @"s2h",
    [COMB] = @"nCr", [PERM] = @"nPr", [MAX] = @"max", [MIN] = @"min",
    [GCD] = @"gcd", [GCF] = @"gcf", [LCM] = @"lcm", [LN] = @"ln",
    [LOG2] = @"log2", [LOG10] = @"log10", [DOT] = @"dot",
    [RANDOM] = @"random", [RANDOMR] = @"randomr"
};

- (NSString *)description
{
    return custom ? [custom objectForKey:@"name"] : Functions_toString[func];
}

@end

#pragma mark - Private Operator Class

@interface Operator : NSObject

- (BOOL)isParenthesis;

@property Operators oper;

@end

@implementation Operator

@synthesize oper;

- (id)init
{
    return [self initWith:-1];
}

- (id)initWith:(Operators)op
{
    self = [super init];
    
    if (self) {
        oper = op;
    }
    
    return self;
}

- (BOOL)isParenthesis
{
    return (oper == OPEN_PAREN || oper == CLOSE_PAREN);
}

- (BOOL)isEqual:(id)object
{
    return [self isEqualTo:object];
}

- (BOOL)isEqualTo:(id)object
{
    if (![object isMemberOfClass:[Operator class]]) return NO;
    return oper == [object oper];
}

- (NSUInteger)numericValue
{
    return oper;
}

#pragma mark === OPERATOR ===
- (int)argsNeeded
{
    switch (oper) {
        case PLUS: case MINUS: case MULT: case DIVD: case EXPO:
        case LEFT_SHIFT: case RIGHT_SHIFT: case UNSIGNED_RIGHT_SHIFT:
        case AND: case XOR: case OR: case MODULO:
            return 2;
        case FACT: case NEGATE: case NOT: case NEGATIVE:
            return 1;
        default:
            return -1;
    }
}

#pragma mark === OPERATOR ===
NSString * const Operators_toString[] = {
    [PLUS] = @"+", [MINUS] = @"-", [MULT] = @"*", [DIVD] = @"/",
    [FACT] = @"!", [EXPO] = @"**", [NEGATE] = @"~", [MODULO] = @"%",
    [LEFT_SHIFT] = @"<<", [RIGHT_SHIFT] = @">>", [UNSIGNED_RIGHT_SHIFT] = @">>>",
    [AND] = @"&", [XOR] = @"^", [OR] = @"|",
    [OPEN_PAREN] = @"(", [CLOSE_PAREN] = @")", [NOT] = @"!",
    [NEGATIVE] = @"-"
};

- (NSString *)description
{
    return Operators_toString[oper];
}

@end

#pragma mark - DCMathParser Implementation

@implementation DCMathParser

@synthesize exp, ans, numsys, errPos;
@synthesize raw;

const NSDictionary *functions;
const NSDictionary *operators;
const NSDictionary *variables;
const NSDictionary *customFunctions;

const NSCharacterSet *alphanum;

- (id)init
{
    return [self initWith:@""];
}

- (id)initWith:(NSString *)input
{
    return [self initWith:input andNumeral:DECIMAL];
}

- (id)initWith:(NSString *)input andNumeral:(int)ns
{
    self = [super init];
    if (self) {
        [self setExp:input];
        [self setNumsys:ns];
        [self initialize];
    }
    return self;
}

- (void)initialize
{
#pragma mark === FUNCTION ===
    if (!functions) {
        functions = [NSDictionary dictionaryWithObjectsAndKeys:
                     @(VECTOR),@"vec",@(VECTOR),@"vector",@(MAG),@"mag",@(NORM),@"norm",
                     @(SIN),@"sin",@(COS),@"cos",@(TAN),@"tan",
                     @(CSC),@"csc",@(SEC),@"sec",@(COT),@"cot",
                     @(SINH),@"sinh",@(COSH),@"cosh",@(TANH),@"tanh",
                     @(ASIN),@"asin",@(ACOS),@"acos",@(ATAN),@"atan",
                     @(ACSC),@"acsc",@(ASEC),@"asec",@(ACOT),@"acot",
                     @(ASINH),@"asinh",@(ACOSH),@"acosh",@(ATANH),@"atanh",
                     @(ACSCH),@"acsch",@(ASECH),@"asech",@(ACOTH),@"acoth",
                     @(LOG),@"log",@(SQRT),@"sqrt",@(CEIL),@"ceil",@(FLOOR),@"floor",
                     @(ROUND),@"round",@(TRUNC),@"trunc",@(ABS),@"abs",
                     @(FTOC),@"ftoc",@(CTOF),@"ctof",@(FTOK),@"ftok",@(KTOF),@"ktof",
                     @(CTOK),@"ctok",@(KTOC),@"ktoc",@(M2S),@"m2s",@(S2M),@"s2m",
                     @(M2H),@"m2h",@(H2M),@"h2m",@(H2S),@"h2s",@(S2H),@"s2h",
                     @(COMB),@"nCr",@(PERM),@"nPr",@(COMB),@"ncr",@(PERM),@"npr",
                     @(MAX),@"max",@(MIN),@"min",@(GCD),@"gcd",@(GCF),@"gcf",@(LCM),@"lcm",
                     @(LN),@"ln",@(LOG2),@"log2",@(LOG10),@"log10",@(DOT),@"dot",
                     @(RANDOM),@"random",@(RANDOMR),@"randomr", nil];
    }
    
#pragma mark === OPERATOR ===
    if (!operators) {
        operators = [NSDictionary dictionaryWithObjectsAndKeys:
                     @(PLUS),@"+",@(MINUS),@"-",@(MULT),@"*",@(DIVD),
                     @"/",@(FACT),@"!",@(EXPO),@"**",@(NEGATE),@"~",
                     @(LEFT_SHIFT),@"<<",@(RIGHT_SHIFT),@">>",
                     @(UNSIGNED_RIGHT_SHIFT),@">>>", @(MODULO),@"%",
                     @(AND),@"&",@(XOR),@"^",@(OR),@"|",@(NOT),@"!",
                     @(OPEN_PAREN),@"(",@(CLOSE_PAREN),@")",@(NEGATIVE),@"-", nil];
    }
    
    if (!variables) {
        NSMutableDictionary *tmpVar = [NSMutableDictionary dictionaryWithObjectsAndKeys:@(M_PI),@"pi",@(M_E),@"e", nil];
        [tmpVar addEntriesFromDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:VAR_KEY]];
        variables = [tmpVar copy];
        if (!variables) {
            variables = [NSDictionary dictionary];
        }
    }
    
    if (!customFunctions) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:CUST_FUNC_KEY]) {
            customFunctions = UNARCHIVE([[NSUserDefaults standardUserDefaults] objectForKey:CUST_FUNC_KEY]);
        } else {
            customFunctions = [NSDictionary dictionary];
        }
    }
    
    alphanum = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_"];
}

- (void)addVariableWithSymbol:(NSString *)symb forValue:(double)val
{
    NSMutableDictionary *tmp = [NSMutableDictionary dictionaryWithDictionary:variables.copy];
    [tmp setObject:@(val) forKey:symb];
    variables = tmp.copy;
}

- (void)updateVariables
{
    NSMutableDictionary *tmpVar = [NSMutableDictionary dictionaryWithObjectsAndKeys:@(M_PI),@"pi",@(M_E),@"e", nil];
    [tmpVar addEntriesFromDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:VAR_KEY]];
    variables = [tmpVar copy];
    if (!variables) {
        variables = [NSDictionary dictionary];
    }
}

- (void)updateFunctions
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CUST_FUNC_KEY]) {
        customFunctions = UNARCHIVE([[NSUserDefaults standardUserDefaults] objectForKey:CUST_FUNC_KEY]);
    } else {
        customFunctions = [NSDictionary dictionary];
    }
}

- (BOOL)scanNumber:(Numbers **)number withScanner:(NSScanner **)scanner
{
    if ([*scanner scanString:@"0." intoString:NULL]) {
        [*scanner setScanLocation:[*scanner scanLocation] - 2];
        double num;
        // a decimal number
        if ([*scanner scanDouble:&num]) {
            *number = CREATE_NUM(num);
            return YES;
        }
    }
    // not a decimal number
    else if ([*scanner scanString:@"0" intoString:NULL]) {
        long double num;
        // a binary number
        if ([*scanner scanString:@"b" intoString:NULL]) {
            NSCharacterSet *binSet = [NSCharacterSet characterSetWithCharactersInString:@"01"];
            NSString *str;
            if ([*scanner scanCharactersFromSet:binSet intoString:&str]) {
                num = (long double) strtoll([str UTF8String], NULL, BINARY);
            } else {
                // invalid binary number; throw an error
//                [NSException raise:@"unable to parse" format:@"invalid binary number"];
                return NO;
            }
        }
        // a hexadecimal number
        else if ([*scanner scanString:@"x" intoString:NULL]) {
            NSCharacterSet *hexSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789ABCDEFabcdef"];
            NSString *str;
            if ([*scanner scanCharactersFromSet:hexSet intoString:&str]) {
                num = (long double) strtoll([str UTF8String], NULL, HEX);
            } else {
                // invalid hexadecimal number; throw an error
//                [NSException raise:@"unable to parse" format:@"invalid hexadecimal number"];
                return NO;
            }
        }
        else {
            NSCharacterSet *octSet = [NSCharacterSet characterSetWithCharactersInString:@"01234567"];
            NSString *str;
            // an octal number
            if ([*scanner scanCharactersFromSet:octSet intoString:&str]) {
                num = (long double) strtoll([str UTF8String], NULL, OCTAL);
            } else num = 0.0L; // a decimal number 0
        }
        *number = CREATE_NUM(num);
        return YES;
    } else {
        double num;
        // a decimal number
        if ([*scanner scanDouble:&num]) {
            *number = CREATE_NUM(num);
            return YES;
        }
    }
    return NO;
}

- (BOOL)scanVector:(Vector **)vector withScanner:(NSScanner **)scanner
{
    if ([*scanner scanString:@"<" intoString:NULL]) {
        return YES;
    }
    return NO;
}

// This should always be called after scanFunction
- (BOOL)scanSymbol:(Numbers **)symb withScanner:(NSScanner **)scanner
{
    NSString *var;
    unsigned long prevloc = (*scanner).scanLocation;
    
    if ([*scanner scanCharactersFromSet:(NSCharacterSet *)alphanum intoString:&var]) {
        id variable = [variables objectForKey:var];
        if (variable) {
            *symb = CREATE_NUM([variable doubleValue]);
            return YES;
        } else {
            [*scanner setScanLocation:prevloc];
        }
    }
    
    return NO;
}

- (BOOL)scanOperator:(Operators *)op withScanner:(NSScanner **)scanner
{
    NSCharacterSet *operChar = [NSCharacterSet characterSetWithCharactersInString:@"+-*/^<>&|%"];
    
    NSString *tempOper;
    if ([*scanner scanCharactersFromSet:operChar intoString:&tempOper]) {
        BOOL bitwise = [[NSUserDefaults standardUserDefaults] boolForKey:BITWISE_ON_KEY];
        if (tempOper.length == 1) {
            *op = (Operators) STR_TO_CHAR(tempOper);
            if (*op == LEFT_SHIFT || *op == RIGHT_SHIFT) {
                // unknown operator; throw an error
                [NSException raise:@"Unable to parse" format:@"Unknown operator: %@", tempOper];
                return NO;
            }
            if (*op == XOR && (!bitwise || [[NSUserDefaults standardUserDefaults] boolForKey:CARET_EXPO_KEY])) *op = EXPO;
        } else {
            if ([tempOper hasPrefix:@"**"]) {
                *op = EXPO;
                [*scanner setScanLocation:[*scanner scanLocation] - tempOper.length + 2];
            } else if (bitwise && [tempOper hasPrefix:@">>>"]) {
                *op = UNSIGNED_RIGHT_SHIFT;
                [*scanner setScanLocation:[*scanner scanLocation] - tempOper.length + 3];
            } else if (bitwise && [tempOper hasPrefix:@"<<"]) {
                *op = LEFT_SHIFT;
                [*scanner setScanLocation:[*scanner scanLocation] - tempOper.length + 2];
            } else if (bitwise && [tempOper hasPrefix:@">>"]) {
                *op = RIGHT_SHIFT;
                [*scanner setScanLocation:[*scanner scanLocation] - tempOper.length + 2];
            } else {
                // for the unary operators
                if ([operChar characterIsMember:STR_TO_CHAR(tempOper)]) {
                    *op = (Operators) STR_TO_CHAR(tempOper);
                    [*scanner setScanLocation:[*scanner scanLocation] - tempOper.length + 1];
                } else {
                    // unknown operator; throw an error
                    [NSException raise:@"Unable to parse" format:@"Unknown operator: %@", tempOper];
                    return NO;
                }
            }
        }
        return YES;
    } else return NO;
}

- (BOOL)scanFunction:(Functions *)func optCustom:(NSString **)cust withScanner:(NSScanner **)scanner
{
    NSString *tempFunc;
    unsigned long prevloc = (*scanner).scanLocation;
    
    if ([*scanner scanCharactersFromSet:(NSCharacterSet *)alphanum intoString:&tempFunc]) {
        if (![*scanner scanString:kOpenParen intoString:NULL]) {
            [*scanner setScanLocation:prevloc];
            return NO;
        }
//        NSLog(@"funcName: %@", tempFunc); // TEST
        *func = [self getFunction:tempFunc];
        if (*func != ERROR) return YES;
        else if ([customFunctions objectForKey:tempFunc]) {
            *cust = tempFunc;
            return YES;
        } else {
            [*scanner setScanLocation:prevloc];
        }
    }
    return NO;
}

- (int)numOfArgNeeded:(id)op
{
    return [op argsNeeded];
}

- (Functions)getFunction:(NSString *)func
{
    id fn = [functions objectForKey:func];
    return fn ? [fn intValue] : ERROR;
}

- (id)parse
{
    NSError *error;
    
    NSMutableArray __autoreleasing *output = [[NSMutableArray alloc] init];
    NSMutableArray __autoreleasing *operatorStack = [[NSMutableArray alloc] init];
    self.raw = [[NSMutableArray alloc] init];
    
    NSString *parsing = [[NSString alloc] initWithString:exp];
    parsing = [parsing stringByReplacingOccurrencesOfString:@"[" withString:kOpenParen];
    parsing = [parsing stringByReplacingOccurrencesOfString:@"{" withString:kOpenParen];
    parsing = [parsing stringByReplacingOccurrencesOfString:@"]" withString:kCloseParen];
    parsing = [parsing stringByReplacingOccurrencesOfString:@"}" withString:kCloseParen];
    
    NSScanner *scanner = [NSScanner scannerWithString:parsing];
    
    BOOL needsArg = YES;
    BOOL needsOper = YES;
    Numbers *tempNum;
    Operators tempOper;
    Functions tempFunc;
    NSString *tempCustomFunc;
    BOOL except = NO;
    
    [self updateFunctions];
    [self updateVariables];
    
    // until the scanner has reached the end of the string
    while (![scanner isAtEnd]) {
        
//        NSLog(@"scanning: %ld, char: %c", scanner.scanLocation, [parsing characterAtIndex:scanner.scanLocation]); // TEST
        errPos = -1;
        except = NO;
        
        if (needsArg) {
            if ([scanner scanString:@"!" intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:NOT] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else if ([scanner scanString:@"~" intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:NEGATE] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else if ([self scanNumber:&tempNum withScanner:&scanner]) {
                [self stack:tempNum isOperator:NO toOutput:&output andOperatorStack:&operatorStack];
            } else if ([scanner scanString:kOpenParen intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:OPEN_PAREN] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else if ([scanner scanString:kCloseParen intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:CLOSE_PAREN] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
            } else if ([self scanFunction:&tempFunc optCustom:&tempCustomFunc withScanner:&scanner]) {
                Function *func;
                if (tempFunc == ERROR) func = [[Function alloc] initWithCustomFunction:tempCustomFunc];
                else func = [[Function alloc] initWithFunction:tempFunc];
                [self stack:func isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                [self stack:[[Operator alloc] initWith:OPEN_PAREN] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else if ([self scanSymbol:&tempNum withScanner:&scanner]) {
                [self stack:tempNum isOperator:NO toOutput:&output andOperatorStack:&operatorStack];
            } else if ([scanner scanString:@"-" intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:NEGATIVE] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else {
                // no valid expression found; throw an error
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:[NSString stringWithFormat:@"No valid expression found (location: %ld)", scanner.scanLocation] forKey:NSLocalizedDescriptionKey];
                error = [NSError errorWithDomain:@"Unable to parse" code:101 userInfo:details];
                errPos = scanner.scanLocation;
                return error;
            }
            needsArg = NO;
            needsOper = YES;
        } else if (needsOper) {
            if ([scanner scanString:@"!" intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:FACT] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else if ([self scanOperator:&tempOper withScanner:&scanner]) {
                [self stack:[[Operator alloc] initWith:tempOper] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
            } else if ([scanner scanString:kCloseParen intoString:NULL]) {
                [self stack:[[Operator alloc] initWith:CLOSE_PAREN] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else if ([scanner scanString:kOpenParen intoString:NULL]) { // implicit multiplication
                [self stack:[[Operator alloc] initWith:MULT] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                [self stack:[[Operator alloc] initWith:OPEN_PAREN] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
            } else if ([scanner scanString:kArgSep intoString:NULL]) {
                [self stack:kArgSep isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
            } else if (IS_CLOSE_PAREN([raw lastObject]) && [self scanNumber:&tempNum withScanner:&scanner]) { // implicit multiplication
                [self stack:[[Operator alloc] initWith:MULT] isOperator:YES toOutput:&output andOperatorStack:&operatorStack];
                [self stack:tempNum isOperator:NO toOutput:&output andOperatorStack:&operatorStack];
                continue;
            } else {
                // no valid operator found; throw an error
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:[NSString stringWithFormat:@"No valid operator found (location: %ld)", scanner.scanLocation] forKey:NSLocalizedDescriptionKey];
                error = [NSError errorWithDomain:@"Unable to parse" code:102 userInfo:details];
                errPos = scanner.scanLocation;
                return error;
            }
            needsArg = YES;
            needsOper = NO;
        } else {
            // nothing needed?; throw an error
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:[NSString stringWithFormat:@"Unknown error [nothing needed?] (location: %ld)", scanner.scanLocation] forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"Unable to parse" code:103 userInfo:details];
            errPos = scanner.scanLocation;
            return error;
        }
    }
    
    // pop entire stack to output
    if ( [operatorStack count] != 0 ) {
        while ( [operatorStack count] != 0 ) {
            if (IS_OPEN_PAREN([operatorStack objectAtIndex:0]) || IS_CLOSE_PAREN([operatorStack objectAtIndex:0])) {
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:@"Mismatched parentheses" forKey:NSLocalizedDescriptionKey];
                error = [NSError errorWithDomain:@"Unable to parse" code:111 userInfo:details];
                return error;
            }
            [output addObject:[operatorStack objectAtIndex:0]];
            [operatorStack removeObjectAtIndex:0];
        }
    }
    
    // double-check to see that the operator stack is now empty
    if ( [operatorStack count] != 0 ) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"Operator not cleaned up properly" forKey:NSLocalizedDescriptionKey];
        error = [NSError errorWithDomain:@"Unable to parse" code:112 userInfo:details];
        return error;
    }
    
//    NSLog(@"output: %@", output); // TEST
    
    if (output.count != 1) {
        return [self evaluate:output];
    } else if (![[output objectAtIndex:0] isMemberOfClass:[Numbers class]]
               && ![[output objectAtIndex:0] isMemberOfClass:[Vector class]]) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"Single operator or function found in the stack" forKey:NSLocalizedDescriptionKey];
        error = [NSError errorWithDomain:@"Unable to evaluate" code:121 userInfo:details];
        return error;
    } else {
        return [output objectAtIndex:0];
    }
}

- (long double)valueForSymbol:(Symbols)symb
{
    switch (symb) {
        case PI:
            return M_PI_L;
        case EULER:
            return M_E_L;
        default:
            return symb;
    }
}

- (int)precedenceForOper:(Operators)oper
{
#pragma mark === OPERATOR ===
    switch (oper) {
        case FACT: case NEGATE: case NOT: case NEGATIVE:
            return 8;
        case EXPO:
            return 7;
        case MULT: case DIVD: case MODULO:
            return 6;
        case PLUS: case MINUS:
            return 5;
        case LEFT_SHIFT: case RIGHT_SHIFT: case UNSIGNED_RIGHT_SHIFT:
            return 4;
        case AND:
            return 3;
        case XOR:
            return 2;
        case OR:
            return 1;
        default:
            return INT_MAX; // functions always come first
    }
}

- (BOOL)isLeftAssociative:(Operators)oper
{
    switch (oper) {
        case FACT: case EXPO: case NEGATE: case NOT: case NEGATIVE:
            return NO;
        default:
            return YES;
    }
}

- (void)stack:(id)expr isOperator:(BOOL)flag
     toOutput:(NSMutableArray **)output andOperatorStack:(NSMutableArray **)operatorStack
{
    [raw addObject:expr]; // keep the raw record of what went into the stack
    if (!flag) {
        // simply add the operand to the output stack
        [*output addObject:expr];
    } else {
        // calculate the precedence and insert the operator to the operator stack
        int index = (int) [*operatorStack count] - 1;
        
        // if expr is a function, push it onto the stack
        if ([expr isMemberOfClass:[Function class]]) {
            [*operatorStack insertObject:expr atIndex:0];
        }
        // if expr is a function argument separator (',')
        else if ([expr isEqual:kArgSep]) {
            while ( index >= 0 && !IS_OPEN_PAREN([*operatorStack objectAtIndex:0]) ) {
                [*output addObject:[*operatorStack objectAtIndex:0]];
                [*operatorStack removeObjectAtIndex:0];
                index--;
            }
            if ( index < 0 ) {
                [NSException raise:@"Unable to parse" format:@"Misplaced argument separator or mismatched parentheses found"];
            }
        }
        
        else {
            int preced = [expr isMemberOfClass:[Function class]] ? INT_MAX : [self precedenceForOper:(Operators)[expr numericValue]];
            Operators oper = ID_TO_OPER(expr);
            if ([*operatorStack count] == 0) {
                [*operatorStack addObject:expr];
            } else {
                if ( oper == OPEN_PAREN ) {
                    [*operatorStack insertObject:expr atIndex:0];
                }
                else if ( oper == CLOSE_PAREN ) {
                    while ( index >= 0 && !IS_OPEN_PAREN([*operatorStack objectAtIndex:0]) ) {
                        [*output addObject:[*operatorStack objectAtIndex:0]];
                        [*operatorStack removeObjectAtIndex:0];
                        index--;
                    }
                    [*operatorStack removeObjectAtIndex:0];
                }
                else {
                    while ( index >= 0 && !IS_OPEN_PAREN([*operatorStack objectAtIndex:0])
                           && ( ([self isLeftAssociative:ID_TO_OPER([*operatorStack objectAtIndex:0])]
                               && preced <= [self precedenceForOper:ID_TO_OPER([*operatorStack objectAtIndex:0])])
                               || (![self isLeftAssociative:ID_TO_OPER([*operatorStack objectAtIndex:0])]
                                   && preced < [self precedenceForOper:ID_TO_OPER([*operatorStack objectAtIndex:0])]) ) ) {
                        [*output addObject:[*operatorStack objectAtIndex:0]];
                        [*operatorStack removeObjectAtIndex:0];
                        index--;
                    }
                    [*operatorStack insertObject:expr atIndex:0];
                }
            }
        }
    }
//    NSLog(@"test - output: %@, operators: %@", *output, *operatorStack); // TEST
}

- (id)operateWithOperator:(id)operator andArgs:(NSArray *)args withError:(NSError **)error
{
#pragma mark === OPERATOR ===
    if ([operator isMemberOfClass:[Operator class]]) {
        switch ([operator numericValue]) {
            case PLUS:
                return [(Calculator*)[args objectAtIndex:0] add:[args objectAtIndex:1]];
            case MINUS:
                return [(Calculator*)[args objectAtIndex:0] subtract:[args objectAtIndex:1]];
            case MULT:
                return [(Calculator*)[args objectAtIndex:0] multiply:[args objectAtIndex:1]];
            case DIVD:
                return [(Calculator*)[args objectAtIndex:0] divide:[args objectAtIndex:1]];
            case FACT:
                return [(Calculator*)[args objectAtIndex:0] factorial];
            case EXPO:
                return [(Calculator*)[args objectAtIndex:0] exponentiate:[args objectAtIndex:1]];
            case NEGATE:
                return [(Calculator*)[args objectAtIndex:0] negate];
            case LEFT_SHIFT:
                return [(Calculator*)[args objectAtIndex:0] shiftLeft:[args objectAtIndex:1]];
            case RIGHT_SHIFT:
                return [(Calculator*)[args objectAtIndex:0] shiftRight:[args objectAtIndex:1]];
            case UNSIGNED_RIGHT_SHIFT:
                return [(Calculator*)[args objectAtIndex:0] shiftRightUnsigned:[args objectAtIndex:1]];
            case AND:
                return [(Calculator*)[args objectAtIndex:0] bitwiseAnd:[args objectAtIndex:1]];
            case XOR:
                return [(Calculator*)[args objectAtIndex:0] bitwiseXor:[args objectAtIndex:1]];
            case OR:
                return [(Calculator*)[args objectAtIndex:0] bitwiseOr:[args objectAtIndex:1]];
            case MODULO:
                return [(Calculator*)[args objectAtIndex:0] modulo:[args objectAtIndex:1]];
            case NOT:
                return [(Calculator*)[args objectAtIndex:0] not];
            case NEGATIVE:
                return [(Calculator*)[args objectAtIndex:0] negative];
            default:
                break;
        }
    }
    
#pragma mark === FUNCTION ===
    else if ([operator isMemberOfClass:[Function class]]) {
        id firstArg = [args objectAtIndex:0];
        switch ([operator numericValue]) {
            case VECTOR:
                return [[Vector alloc] initWith:[firstArg longDoubleValue]
                                               :[[args objectAtIndex:1] longDoubleValue]
                                               :[[args objectAtIndex:2] longDoubleValue]];
            case MAG:
                return [firstArg magnitude];
            case NORM:
                return [firstArg normalize];
            case SIN:
                return CREATE_NUM(sinl([firstArg longDoubleValue]));
            case COS:
                return CREATE_NUM(cosl([firstArg longDoubleValue]));
            case TAN:
                return CREATE_NUM(tanl([firstArg longDoubleValue]));
            case CSC:
                return CREATE_NUM(1.0L/sinl([firstArg longDoubleValue]));
            case SEC:
                return CREATE_NUM(1.0L/cosl([firstArg longDoubleValue]));
            case COT:
                return CREATE_NUM(1.0L/tanl([firstArg longDoubleValue]));
            case SINH:
                return CREATE_NUM(sinhl([firstArg longDoubleValue]));
            case COSH:
                return CREATE_NUM(coshl([firstArg longDoubleValue]));
            case TANH:
                return CREATE_NUM(tanhl([firstArg longDoubleValue]));
            case ASIN:
                return CREATE_NUM(asinl([firstArg longDoubleValue]));
            case ACOS:
                return CREATE_NUM(acosl([firstArg longDoubleValue]));
            case ATAN:
                return CREATE_NUM(atanl([firstArg longDoubleValue]));
            case ACSC:
                return CREATE_NUM(1.0L/asinl([firstArg longDoubleValue]));
            case ASEC:
                return CREATE_NUM(1.0L/acosl([firstArg longDoubleValue]));
            case ACOT:
                return CREATE_NUM(1.0L/atanl([firstArg longDoubleValue]));
            case ASINH:
                return CREATE_NUM(1.0L/sinhl([firstArg longDoubleValue]));
            case ACOSH:
                return CREATE_NUM(1.0L/coshl([firstArg longDoubleValue]));
            case ATANH:
                return CREATE_NUM(1.0L/tanhl([firstArg longDoubleValue]));
            case ACSCH:
                return CREATE_NUM(1.0L/asinhl([firstArg longDoubleValue]));
            case ASECH:
                return CREATE_NUM(1.0L/acoshl([firstArg longDoubleValue]));
            case ACOTH:
                return CREATE_NUM(1.0L/atanhl([firstArg longDoubleValue]));
            case LN:
                return CREATE_NUM(logl([firstArg longDoubleValue]));
            case SQRT:
                return CREATE_NUM(sqrtl([firstArg longDoubleValue]));
            case CEIL:
                return CREATE_NUM(ceill([firstArg longDoubleValue]));
            case FLOOR:
                return CREATE_NUM(floorl([firstArg longDoubleValue]));
            case ROUND:
                return CREATE_NUM(roundl([firstArg longDoubleValue]));
            case TRUNC:
                return CREATE_NUM(truncl([firstArg longDoubleValue]));
            case ABS:
                return CREATE_NUM(fabsl([firstArg longDoubleValue]));
            case FTOC:
                return CREATE_NUM(([firstArg longDoubleValue] - 32.0L) * 5.0L/9.0L);
            case CTOF:
                return CREATE_NUM([firstArg longDoubleValue] * 9.0L/5.0L + 32.0L);
            case FTOK:
                return CREATE_NUM(([firstArg longDoubleValue] + 459.67L) * 5.0L/9.0L);
            case KTOF:
                return CREATE_NUM([firstArg longDoubleValue] * 9.0L/5.0L - 459.67L);
            case CTOK:
                return CREATE_NUM([firstArg longDoubleValue] + 273.15L);
            case KTOC:
                return CREATE_NUM([firstArg longDoubleValue] - 273.15L);
            case H2M: case M2S:
                return CREATE_NUM([firstArg longDoubleValue] * 60.0L);
            case M2H: case S2M:
                return CREATE_NUM([firstArg longDoubleValue] / 60.0L);
            case H2S:
                return CREATE_NUM([firstArg longDoubleValue] * 3600.0L);
            case S2H:
                return CREATE_NUM([firstArg longDoubleValue] / 3600.0L);
            case COMB:
                return [firstArg chooseR:[args objectAtIndex:1]];
            case PERM:
                return [firstArg pickR:[args objectAtIndex:1]];
            case MAX:
                return CREATE_NUM(fmaxl([firstArg longDoubleValue], [[args objectAtIndex:1] longDoubleValue]));
            case MIN:
                return CREATE_NUM(fminl([firstArg longDoubleValue], [[args objectAtIndex:1] longDoubleValue]));
            case GCF: case GCD:
                return [firstArg gcd:[args objectAtIndex:1]];
            case LCM:
                return [firstArg lcm:[args objectAtIndex:1]];
            case LOG:
                return CREATE_NUM(logl([firstArg longDoubleValue]) / logl([[args objectAtIndex:1] longDoubleValue]));
            case LOG2:
                return CREATE_NUM(log2l([firstArg longDoubleValue]));
            case LOG10:
                return CREATE_NUM(log10l([firstArg longDoubleValue]));
            case DOT:
                return [firstArg dot:[args objectAtIndex:1]];
            case RANDOM:
                return CREATE_NUM(arc4random_uniform((int)[firstArg doubleValue]));
            case RANDOMR:
                return CREATE_NUM((long double)arc4random_uniform((int)[[args objectAtIndex:1] doubleValue]-(int)[firstArg doubleValue])+[firstArg longDoubleValue]);
            default:
                if ([customFunctions objectForKey:[operator description]]) {
                    NSMutableString *tmp_eq = [[[customFunctions objectForKey:[operator description]] objectForKey:@"func"] mutableCopy];
                    for (int i = 0; i < args.count; i++) {
                        NSString *rplcstr = [@"@" stringByAppendingFormat:@"%d", i];
                        NSString *rplcval = [[args objectAtIndex:i] description];
                        if ([[args objectAtIndex:i] isMemberOfClass:[Vector class]]) {
                            rplcval = [rplcval stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"vec("];
                            rplcval = [rplcval stringByReplacingCharactersInRange:NSMakeRange(rplcval.length-1, 1) withString:@")"];
                        }
                        [tmp_eq replaceOccurrencesOfString:rplcstr withString:rplcval options:NSLiteralSearch range:NSMakeRange(0, tmp_eq.length)];
                    }
                    DCMathParser *tmp_mp = [[DCMathParser alloc] initWith:tmp_eq andNumeral:numsys];
                    return [tmp_mp parse];
                }
                break;
        }
    }
    
    // unknown operator object; throw an error
    NSMutableDictionary* details = [NSMutableDictionary dictionary];
    [details setValue:[@"Unknown operator or function: " stringByAppendingString:[operator description]] forKey:NSLocalizedDescriptionKey];
    *error = [NSError errorWithDomain:@"Unable to evaluate" code:221 userInfo:details];
    
    return nil;
}

- (id)evaluate:(NSMutableArray *)output
{
    NSMutableArray *stack = [NSMutableArray new];
    
    NSError *error = nil;
    
    while ( [output count] != 0 ) {
        while ( [output count] != 0 && ([[output objectAtIndex:0] isMemberOfClass:[Numbers class]] || [[output objectAtIndex:0] isMemberOfClass:[Vector class]]) ) {
            [stack insertObject:[output objectAtIndex:0] atIndex:0];
            [output removeObjectAtIndex:0];
        }
        
//        NSLog(@"test - output: %@, stack: %@", [output description], [stack description]); // TEST
        
        id op = [output objectAtIndex:0];
        int argsNeeded = [op argsNeeded];
        
        if ( [stack count] < argsNeeded ) {
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:[@"Not enough arguments for operator: " stringByAppendingString:[op description]] forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"Unable to evaluate" code:201 userInfo:details];
            return error;
        }
        
        NSMutableArray *args = [[NSMutableArray alloc] initWithCapacity:argsNeeded];
        
        for ( int i = argsNeeded - 1; i >= 0; i-- ) {
            id val = [stack objectAtIndex:i];
            [args addObject:val];
            [stack removeObjectAtIndex:i];
        }
        
        [output removeObjectAtIndex:0];
        
        id result = [self operateWithOperator:op andArgs:args withError:&error];
        
        if (result) {
            [stack insertObject:result atIndex:0];
        } else if (error) {
            return error;
        }
    }
    
    if ( [stack count] == 0 ) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"No evaluatable item in the stack" forKey:NSLocalizedDescriptionKey];
        error = [NSError errorWithDomain:@"Unable to evaluate" code:211 userInfo:details];
        return error;
    } else if ( ![[stack objectAtIndex:0] isMemberOfClass:[Numbers class]]
               && ![[stack objectAtIndex:0] isMemberOfClass:[Vector class]] ) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"Single operator or function found in the stack" forKey:NSLocalizedDescriptionKey];
        error = [NSError errorWithDomain:@"Unable to evaluate" code:212 userInfo:details];
        return error;
    } else if ( [stack count] > 1 ) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"Too many values found in the stack" forKey:NSLocalizedDescriptionKey];
        error = [NSError errorWithDomain:@"Unable to evaluate" code:213 userInfo:details];
        return error;
    } else {
        return [stack objectAtIndex:0];
    }
}

@end
