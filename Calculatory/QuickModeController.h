//
//  QuickModeController.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface QuickModeController : NSWindowController <NSTextFieldDelegate, NSTextViewDelegate> {
    IBOutlet NSTextView *aField;
    IBOutlet NSButton *calculateButton;
    IBOutlet NSTextField *ansField;
    IBOutlet NSButton *stickyButton;
    IBOutlet NSView *innerView;
    IBOutlet NSView *innerMostView;
    IBOutlet NSView *mainView;
    IBOutlet NSButton *numSysButton;
    IBOutlet NSButton *errDetailButton;
    IBOutlet NSTextField *warningLabel;
    
    id temp;
    NSString *ans_str;
    
    // popover objects
    IBOutlet NSPopover *infoPopover;
    IBOutlet NSTextField *infoErrorTitle;
    IBOutlet NSTextField *infoErrorReason;
    IBOutlet NSTextField *infoErrorType;
    IBOutlet NSTextField *infoNoError;
}

- (IBAction)calculate:(id)sender;
- (IBAction)calculateAndCopy:(id)sender;
- (IBAction)ansClicked:(id)sender;
- (IBAction)stick:(id)sender;
- (IBAction)showInfo:(id)sender;
- (IBAction)changeNumSys:(id)sender;
- (void)changeNumSysShortcut:(unsigned short)keyCode;
- (void)changeFontToFitRect:(NSString*)str;
- (void)updateViews;

@property float preY;
@property NSError *currentError;
@property BOOL isMiniMode;

@end
