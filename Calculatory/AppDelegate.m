//
//  AppDelegate.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <ServiceManagement/ServiceManagement.h>

#import "AppDelegate.h"
#import "QuickModeController.h"
#import "ConfigWindowController.h"
#import "Constants.h"

#define CMD_OPT 1573160
#define SPACEBAR 49

@implementation AppDelegate

@synthesize window = _window;
@synthesize quickModeIsOpen, quickMode, configWindow;
@synthesize aField;
@synthesize prevHotKeyRef;

-(IBAction)showQuickMode:(id)sender
{
    if (!quickModeIsOpen) {
        quickModeController = [[QuickModeController alloc] init];
        quickMode = [quickModeController window];
    }
    [NSApp activateIgnoringOtherApps:YES];
    [quickModeController showWindow:self];
}

-(IBAction)showConfigWindow:(id)sender
{
    if (!configWindowController) {
        configWindowController = [[ConfigWindowController alloc] init];
        configWindow = [configWindowController window];
    }
    if (quickModeIsOpen) [quickMode close];
    [NSApp activateIgnoringOtherApps:YES];
    [configWindowController showWindow:self];
}

-(void)awakeFromNib {
    statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [statusItem setMenu:statusMenu];
    [statusItem setImage:[NSImage imageNamed:@"agentIconTemplate"]];
    [statusItem setHighlightMode:YES];
    [statusItem setToolTip:@"Calculatory"];
    [statusItem setMenu:statusMenu];
}

// HANDLES GLOBAL HOTKEYS
static OSStatus MyHotKeyHandler(EventHandlerCallRef nextHandler, EventRef theEvent, void *userData) {
    
    EventHotKeyID hkCom;
    
    GetEventParameter(theEvent, kEventParamDirectObject, typeEventHotKeyID, NULL, sizeof(hkCom), NULL, &hkCom);
//    AppController *controller = (AppController *)userData;
    
    int l = hkCom.id;
    
    switch (l) {
        case 1:
            [(__bridge AppDelegate *)userData showQuickMode:nil];
            break;
//        case 2:
//            NSLog(@"case 2");
//            break;
//        case 3:
//            NSLog(@"case 3");
//            break;
        default:
            break;
    }
    return noErr;
}

// REGISTERS GLOBAL HOTKEYS
-(void)registerHotKeys {
    EventHotKeyRef gMyHotKeyRef;
    EventHotKeyID gMyHotKeyID;
    EventTypeSpec eventType;
    eventType.eventClass=kEventClassKeyboard;
    eventType.eventKind=kEventHotKeyPressed;
    
    InstallApplicationEventHandler(&MyHotKeyHandler, 1, &eventType, (__bridge void *)self, NULL);
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    NSUInteger modFlags = [userDefaults integerForKey:MODFLAG_KEY];
    int modFlag = 0;
    
    if (modFlags & NSShiftKeyMask) modFlag += shiftKey;
    if (modFlags & NSControlKeyMask) modFlag += controlKey;
    if (modFlags & NSAlternateKeyMask) modFlag += optionKey;
    if (modFlags & NSCommandKeyMask) modFlag += cmdKey;
//    if (modFlags & NSFunctionKeyMask) modFlag += kFunctionKeyCharCode; // not working
    
    UnregisterEventHotKey(prevHotKeyRef);
    
    gMyHotKeyID.signature='htk1';
    gMyHotKeyID.id=1;
    RegisterEventHotKey((int)[userDefaults integerForKey:KEYCODE_KEY], modFlag, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
//    RegisterEventHotKey(49, optionKey+cmdKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
    
    prevHotKeyRef = gMyHotKeyRef;
    
//    gMyHotKeyID.signature='htk2';
//    gMyHotKeyID.id=2;
//    RegisterEventHotKey(124, cmdKey+optionKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
    
    // shift+space, DEFAULT GLBOAL HOTKEY for the QuickMode
//    gMyHotKeyID.signature='htk3';
//    gMyHotKeyID.id=3;
//    RegisterEventHotKey(49, shiftKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
}

-(void)applicationDidFinishLaunching:(NSNotification *)notification {
//    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
    NSString *preferredLang = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSDictionary *defaults = [NSDictionary dictionaryWithObjectsAndKeys:
                              @(SPACEBAR),KEYCODE_KEY,@(CMD_OPT),MODFLAG_KEY,@"⌥ + ⌘ + Space",HOTKEY_KEY,@(NO),MANUALLOC_KEY,preferredLang,PREFLANG_KEY,@(YES),MINI_MODE_KEY,
                              @(10),NUMSYS_KEY,@(3),MAX_FRAC_DIGIT_KEY,@(5),MAX_INT_DIGIT_KEY,@(NO),COMMA_SEP_KEY,@(NO),BITWISE_ON_KEY,@(NO),CARET_EXPO_KEY,
                              @(-1),LOGIN_KEY,@(NO),ERR_DETAIL_KEY,@"",REMEMBER_POS_KEY,@(0),THEME_KEY,@(YES),SHOW_STICKY_KEY,@(NO),ALWAYS_STICKY_KEY,
                              @(YES),AUTO_PAREN_KEY,@(NO),REMEMBER_LAST_KEY,@"",REMEMBER_LAST_STR_KEY,
                              @(THEMES[0][FRAME_R]),FRAME_RED_KEY,@(THEMES[0][FRAME_G]),FRAME_GREEN_KEY,@(THEMES[0][FRAME_B]),FRAME_BLUE_KEY,
                              @(THEMES[0][FIELD_R]),FIELD_RED_KEY,@(THEMES[0][FIELD_G]),FIELD_GREEN_KEY,@(THEMES[0][FIELD_B]),FIELD_BLUE_KEY,
                              @(THEMES[0][FONT_R]),FONT_RED_KEY,@(THEMES[0][FONT_G]),FONT_GREEN_KEY,@(THEMES[0][FONT_B]),FONT_BLUE_KEY, nil];
    [defs registerDefaults:defaults];
    [defs synchronize];
    
//    NSLog(@"%f, %f, %f\n%f, %f, %f\n%f, %f, %f", [defs floatForKey:FRAME_RED_KEY], [defs floatForKey:FRAME_GREEN_KEY], [defs floatForKey:FRAME_BLUE_KEY],
//          [defs floatForKey:FIELD_RED_KEY], [defs floatForKey:FIELD_GREEN_KEY], [defs floatForKey:FIELD_BLUE_KEY],
//          [defs floatForKey:FONT_RED_KEY], [defs floatForKey:FONT_GREEN_KEY], [defs floatForKey:FONT_BLUE_KEY]);
    
    if ([defs integerForKey:LOGIN_KEY] == -1) {
        NSUInteger choice = NSRunInformationalAlertPanel(NSLocalizedString(@"Start at Login",nil), NSLocalizedString(@"Would you like the app to always start at login?",nil), NSLocalizedString(@"Yes",nil), NSLocalizedString(@"No",nil), nil);
        if (choice == NSAlertDefaultReturn) {
            if (!SMLoginItemSetEnabled((__bridge CFStringRef)@"com.hdc.CalculatoryLauncher", YES)) {
                NSAlert *alert = [NSAlert alertWithMessageText:@"Error adding Calculatory to login items"
                                                 defaultButton:@"Cancel"
                                               alternateButton:nil
                                                   otherButton:nil
                                     informativeTextWithFormat:@"Failed to add Calculatory to login items :(\nPlease contact if this error continues."];
                [alert runModal];
            }
            [defs setBool:YES forKey:LOGIN_KEY];
        } else {
            [defs setBool:NO forKey:LOGIN_KEY];
        }
        [defs synchronize];
        
        NSAlert *alert = [[NSAlert alloc] init];
        [alert setMessageText:@"Documentation"];
        [alert setInformativeText:@"Looks like it's your first time running Calculatory.\nIf it is, you should go over the documentation to better understand how to use Calculatory!"];
        [alert addButtonWithTitle:@"OK"];
        [alert addButtonWithTitle:@"Skip"];
        [[[alert buttons] objectAtIndex:1] setKeyEquivalent:@"\033"];
        [alert setAlertStyle:NSInformationalAlertStyle];
        NSInteger response = [alert runModal];
        if (response == NSAlertFirstButtonReturn) {
            [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:@"http://calculatoryapp.com/docs/#howtoModal"]];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registerHotKeys) name:REG_HOTKEY_NOTE object:nil];
    [self registerHotKeys];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)sender
{
    [[NSApplication sharedApplication] hide:nil];
    return NO;
}

@end
