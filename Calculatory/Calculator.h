//
//  Calculator.h
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Calculator : NSObject

- (NSString *) print:(NSString *) fmt;
- (double)doubleValue;
- (long double)longDoubleValue;
- (id) add:(id)n;
- (id) subtract:(id)n;
- (id) multiply:(id)n;
- (id) divide:(id)n;
- (id) dot:(id)n;
- (id) exponentiate:(id)n;
- (id) cross:(id)n;
- (id) factorial;
- (id) modulo:(id)n;
- (id) chooseR:(id)n;
- (id) pickR:(id)n;
- (id) shiftLeft:(id)n;
- (id) shiftRight:(id)n;
- (id) shiftRightUnsigned:(id)n;
- (id) bitwiseAnd:(id)n;
- (id) bitwiseXor:(id)n;
- (id) bitwiseOr:(id)n;
- (id) negate;
- (id) not;
- (id) magnitude;
- (id) normalize;
- (id) gcd:(id)n;
- (id) lcm:(id)n;
- (id) negative;

@end
