//
//  RoundRectWindow.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "RoundRectWindow.h"
#import "Constants.h"

@implementation RoundRectWindow

@synthesize trackingArea;

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void)drawRect:(NSRect)dirtyRect
{
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:dirtyRect xRadius:16 yRadius:16];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSColor *color = [NSColor colorWithCalibratedRed:[defaults floatForKey:FRAME_RED_KEY] green:[defaults floatForKey:FRAME_GREEN_KEY] blue:[defaults floatForKey:FRAME_BLUE_KEY] alpha:0.5];
    [color set];
//    [[NSColor colorWithCalibratedRed:0.6882 green:0.8098 blue:1.0000 alpha:0.5] set]; // <-- BLUE 3 THEME COLOR
//    [[NSColor colorWithCalibratedRed:0.7882 green:0.9098 blue:1.0000 alpha:0.5] set]; // <-- BLUE 2 THEME COLOR
//    [[NSColor colorWithCalibratedRed:0 green:0 blue:0 alpha:0.3] set]; // <-- DEFAULT WINDOW BACKGROUND COLOR
    [path fill];
}

-(void)viewWillMoveToWindow:(NSWindow *)newWindow
{
    if (![self.identifier isEqualToString:@"preview"]) {
        trackingArea = [[NSTrackingArea alloc] initWithRect:[self frame] options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways) owner:self userInfo:nil];
        [self addTrackingArea:trackingArea];
    }
}

-(void)updateTrackingAreas
{
    [self removeTrackingArea:trackingArea];
    [self viewWillMoveToWindow:self.window];
    [super updateTrackingAreas];
}

-(void)mouseEntered:(NSEvent *)theEvent
{
    if (![self.window isKeyWindow]) {
        [self.window setAlphaValue:1];
    }
}

-(void)mouseExited:(NSEvent *)theEvent
{
    if (![self.window isKeyWindow]) {
        [self.window setAlphaValue:0.5];
    }
}

@end
