//
//  Message.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 22..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Message.h"

@implementation Message

@synthesize str;

-(id) init
{
    return [self initWith:@""];
}

-(id) initWith:(NSString *)msg
{
    self = [super init];
    if (self) {
        [self setStr:msg];
    }
    return self;
}

-(void) appendMsg:(NSString *)msg
{
    self.str = [self.str stringByAppendingString:msg];
}

-(void) appendLN
{
    self.str = [self.str stringByAppendingString:@"\n"];
}

-(NSString *) print:(NSString *)fmt
{
    return str;
}

-(id) add:(id)placeholder
{
    return placeholder;
}
@end
