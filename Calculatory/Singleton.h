//
//  Singleton.h
//  Calculatory
//
//  Created by Dennis Chung on 12. 11. 20..
//  Copyright (c) 2012년 Dennis Chung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject {
    id sharedInstance;
}

@property (nonatomic, retain) id sharedInstance;

+ (id)sharedManager;

@end
