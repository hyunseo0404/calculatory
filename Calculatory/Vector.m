//
//  Vector.m
//  Calculatory
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Vector.h"

@implementation Vector

@synthesize x, y, z;

-(id) init
{
    return [self initWith:0 :0 :0];
}

-(id) initWith:(long double)a :(long double)b :(long double)c
{
    self = [super init];
    if (self) {
        [self setTo:a :b :c];
    }
    return self;
}

-(id) initWithStr:(NSString *)str
{
    self = [super init];
    if (self) {
        str = [[[str stringByReplacingOccurrencesOfString:@"<" withString:@""]
                stringByReplacingOccurrencesOfString:@">" withString:@""]
               stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSArray *temp = [str componentsSeparatedByString:@","];
        [self setTo:[[temp objectAtIndex:0] doubleValue] :[[temp objectAtIndex:1] doubleValue] :[[temp objectAtIndex:2] doubleValue]];
    }
    return self;
}

-(void) setTo:(long double)a :(long double)b :(long double)c
{
    x = a;
    y = b;
    z = c;
}

-(NSString *) print:(NSString *) fmt
{
    NSString *fmt_tmp = @"<";
//    NSString *fmt_tmp = [NSString stringWithFormat:@"<%@, %@, %@>", fmt, fmt, fmt];
    
    if (self.x != 0 && (fabsl(self.x)/1e5 >= 1.0 || fabsl(self.x)/1e7 < 1e-10)) {
        fmt_tmp = [NSString stringWithFormat:@"%@%@, ", fmt_tmp, @"%.2Le"];
    } else if (ceill(self.x) == self.x && floorl(self.x) == self.x) { // <-- CHECKS IF THE NUMBER IS SIMPLY AN INTEGER
        fmt_tmp = [NSString stringWithFormat:@"%@%@, ", fmt_tmp, @"%.0Lf"];
    } else {
        fmt_tmp = [NSString stringWithFormat:@"%@%@, ", fmt_tmp, fmt];
    }
    if (self.y != 0 && (fabsl(self.y)/1e5 >= 1.0 || fabsl(self.y)/1e7 < 1e-10)) {
        fmt_tmp = [NSString stringWithFormat:@"%@%@, ", fmt_tmp, @"%.2Le"];
    } else if (ceill(self.y) == self.y && floorl(self.y) == self.y) { // <-- CHECKS IF THE NUMBER IS SIMPLY AN INTEGER
        fmt_tmp = [NSString stringWithFormat:@"%@%@, ", fmt_tmp, @"%.0Lf"];
    } else {
        fmt_tmp = [NSString stringWithFormat:@"%@%@, ", fmt_tmp, fmt];
    }
    if (self.z != 0 && (fabsl(self.z)/1e5 >= 1.0 || fabsl(self.z)/1e7 < 1e-10)) {
        fmt_tmp = [NSString stringWithFormat:@"%@%@>", fmt_tmp, @"%.2Le"];
    } else if (ceill(self.z) == self.z && floorl(self.z) == self.z) { // <-- CHECKS IF THE NUMBER IS SIMPLY AN INTEGER
        fmt_tmp = [NSString stringWithFormat:@"%@%@>", fmt_tmp, @"%.0Lf"];
    } else {
        fmt_tmp = [NSString stringWithFormat:@"%@%@>", fmt_tmp, fmt];
    }
    
    NSString *tmp = [NSString stringWithFormat:fmt_tmp, x, y, z];
    tmp = [tmp stringByReplacingOccurrencesOfString:@" " withString:@""];
    return tmp;
}

-(Vector *) add: (Vector *) v
{
    return [[Vector alloc] initWith:(self.x+v.x) :(self.y+v.y) :(self.z+v.z)];
}

-(Vector *) subtract: (Vector *) v
{
    return [[Vector alloc] initWith:(self.x-v.x) :(self.y-v.y) :(self.z-v.z)];
}

-(Vector *) multiply: (id) v
{
    if ( [v isMemberOfClass:[Vector class]] ) {
        return [[Vector alloc] initWith:(self.x*[v x]) :(self.y*[v y]) :(self.z*[v z])];
    }
    else if ( [v isMemberOfClass:[Numbers class]] ) {
        return [[Vector alloc] initWith:(self.x*[v n]) :(self.y*[v n]) :(self.z*[v n])];
    }
    else {
        [NSException raise:@"Invalid vector multiplication" format:@"Attempt to multiply vector with unknown object"];
        return v;
    }
}

-(Vector *) divide:(id) v
{
    if ( [v isMemberOfClass:[Vector class]] ) {
        return [[Vector alloc] initWith:(self.x/[v x]) :(self.y/[v y]) :(self.z/[v z])];
    }
    else if ( [v isMemberOfClass:[Numbers class]] ) {
        return [[Vector alloc] initWith:(self.x/[v n]) :(self.y/[v n]) :(self.z/[v n])];
    }
    else {
        [NSException raise:@"Invalid vector division" format:@"Attempt to multiply vector with unknown object"];
        return v;
    }
}

-(Vector *) exponentiate: (Numbers *) n
{
    return [[Vector alloc] initWith:powl(self.x,n.n) :powl(self.y,n.n) :powl(self.z,n.n)];
}

-(Numbers *) dot: (id) v
{
    if ( [v isMemberOfClass:[Vector class]] ) {
        return [[Numbers alloc] initWith:(self.x*[v x] + self.y*[v y] + self.z*[v z])];
    }
    else if ( [v isMemberOfClass:[Numbers class]] ) {
        return [[Numbers alloc] initWith:(self.x*[v n] + self.y*[v n] + self.z*[v n])];
    }
    return nil;
}

-(Numbers *) magnitude
{
    return [[Numbers alloc] initWith:(sqrtl(powl(self.x,2)+powl(self.y,2)+powl(self.z,2)))];
}

-(Vector *) normalize
{
    return [self divide:[self magnitude]];
}

-(Vector *) cross: (Vector *) v
{
    return [[Vector alloc] initWith:(self.y*v.z-self.z*v.y) :(self.z*v.x-self.x*v.z) :(self.x*v.y-self.y*v.x)];
}

-(Vector *) negative
{
    return [[Vector alloc] initWith:-self.x :-self.y :-self.z];
}

- (NSString *) description
{
    return [NSString stringWithFormat:@"<%Lf, %Lf, %Lf>", x, y, z];
}

- (BOOL)isEqual:(id)object
{
    return [self isEqualTo:object];
}

- (BOOL)isEqualTo:(id)object
{
    if (![object isMemberOfClass:[Vector class]]) return NO;
    else return (self.x == [object x] && self.y == [object y] && self.z == [object z]);
}

@end
