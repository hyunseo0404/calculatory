//
//  AppDelegate.h
//  CalculatoryLauncher
//
//  Created by Hyun Seo Chung on 5/31/14.
//  Copyright (c) 2014 Hyun Seo Chung. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@end
