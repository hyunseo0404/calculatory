//
//  main.m
//  CalculatoryLauncher
//
//  Created by Hyun Seo Chung on 5/31/14.
//  Copyright (c) 2014 Hyun Seo Chung. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
