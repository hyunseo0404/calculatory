//
//  CalculatoryTests.m
//  CalculatoryTests
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "CalculatoryTests.h"
#import "DCMathParser.h"
#import "Numbers.h"
#import "Vector.h"
#import "Constants.h"

// pre-calculated constants (pi and e) as long double's
#define M_PI_L 3.1415926535897932384626433832795L
#define M_E_L 2.7182818284590452353602874713526L

@implementation CalculatoryTests

- (void)setUp
{
    [super setUp];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:BITWISE_ON_KEY]; // Temporarily enable all the bitwise operators
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testBasic1
{
    NSString *expr = @"5+((1+2)*4)-3";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    XCTAssertEqual([result longDoubleValue], 14.0L, @"Failed to parse '5+((1+2)*4)-3'");
}

- (void)testBasic2
{
    NSString *expr = @"3+4*2/(1-5)**2**3*sin(12)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
//    NSString *ans = [@"(\n    3,4,2,\"*\",1,5,\"-\",2,3,\"^\",\"^\",\"/\",12,sin,\"*\",\"+\"\n)" stringByReplacingOccurrencesOfString:@"," withString:@",\n    "];
//    STAssertEqualObjects([[parser parse] description], ans, @"Failed to parse '3+4*2/(1-5)^2^3*sin(12)'");
    id result = [parser parse];
    long double expected = 3+4*2/powl((1-5),powl(2,3))*sinl(12);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '3+4*2/(1-5)^2^3*sin(12)'");
}

- (void)testComplex1
{
    NSString *expr = @"mag((2*cos(sin(mag(norm(cot(mag(vec(1,2,3))*3)*vec(4,-2,3)))*pi))+1)*vec(1,2,3))";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = [[[[[Numbers alloc] initWith:(2.0L*cosl(sinl([[[[[[[Numbers alloc] initWith:(1.0L/tanl([[[[[Vector alloc] initWith:1 :2 :3] magnitude] multiply:[[Numbers alloc] initWith:3]] longDoubleValue]))] multiply:[[Vector alloc] initWith:4 :-2 :3]] normalize] magnitude] multiply:[[Numbers alloc] initWith:M_PI_L]] longDoubleValue]))+1.0L)] multiply:[[Vector alloc] initWith:1 :2 :3]] magnitude] longDoubleValue];
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse 'mag((2*cos(sin(mag(norm(cot(mag(vec(1,2,3))*3)*vec(4,-2,3)))*pi))+1)*vec(1,2,3))'");
}

- (void)testFunc1
{
    NSString *expr = @"sin(45)+cos(45)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
//    NSString *ans = [@"(\n    45,sin,45,cos,\"+\"\n)" stringByReplacingOccurrencesOfString:@"," withString:@",\n    "];
//    STAssertEqualObjects([[parser parse] description], ans, @"Failed to parse 'sin(45)+cos(45)'");
    id result = [parser parse];
    long double expected = sinl(45) + cosl(45);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse 'sin(45)+cos(45)'");
}

- (void)testFunc2
{
    NSString *expr = @"cos(45+sin(45))";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
//    NSString *ans = [@"(\n    45,45,sin,\"+\",cos\n)" stringByReplacingOccurrencesOfString:@"," withString:@",\n    "];
//    STAssertEqualObjects([[parser parse] description], ans, @"Failed to parse 'cos(45+sin(45))'");
    id result = [parser parse];
    long double expected = cosl(45+sinl(45));
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse 'cos(45+sin(45))'");
}

- (void)testImplMult
{
    NSString *expr = @"3(4)3(4)(3)4";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 3*4*3*4*3*4;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '3(4)3(4)(3)4");
}

- (void)testVector1
{
    NSString *expr = @"vec(1,2,3)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    Vector *expected = [[Vector alloc] initWith:1 :2 :3];
    XCTAssertEqualObjects(result, expected, @"Failed to parse 'vec(1,2,3)'");
}

- (void)testUnaryOper1
{
    NSString *expr = @"3!!*+3!";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 720*6;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '3!'");
}

- (void)testUnaryOper2
{
    NSString *expr = @"~~~~~+5-~~-1";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = ~~~~~+5-~~-1;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '~~~~~5-~~1'");
}

- (void)testUnaryOper3
{
    NSString *expr = @"~~5!*~3!!-~+2!";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = ~~120*~720-~+2;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '~~5!*~3!!-~+2!'");
}

- (void)testBitwiseOper1
{
    NSString *expr = @"-35 << 5";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = -35 << 5;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 << 5'");
}

- (void)testBitwiseOper2
{
    NSString *expr = @"-35 >> 5";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = -35 >> 5;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 >> 5'");
}

- (void)testBitwiseOper3
{
    NSString *expr = @"-35 >>> 5";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = (unsigned int) -35 >> 5;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 >>> 5'");
}

- (void)testBitwiseOper4
{
    NSString *expr = @"-35 & 5";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = -35 & 5;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 & 5'");
}

- (void)testBitwiseOper5
{
    NSString *expr = @"-35 ^ 5";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = -35 ^ 5;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 ^ 5'");
}

- (void)testBitwiseOper6
{
    NSString *expr = @"-35 | 5";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = -35 | 5;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 | 5'");
}

- (void)testBitwiseOper7
{
    NSString *expr = @"-35 << 5 | 13 >> 7 & 3";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = -35 << 5 | (13 >> 7 & 3);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '-35 << 5 | 13 >> 7 & 3'");
}

- (void)testVectorFunc1
{
    NSString *expr = @"mag(vec(1,2,3))";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = [[[[Vector alloc] initWith:1 :2 :3] magnitude] longDoubleValue];
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse 'mag(vec(1,2,3))'");
}

- (void)testSymbol1
{
    NSString *expr = @"2*pi+e/-1";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 2 * M_PI + M_E / -1;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '2*pi+e/-1'");
}

- (void)testRandomFunc
{
    NSString *expr = @"1.2+max(2.3,3.5)!";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 1.2 + tgammal(fmaxl(2.3, 3.5)+1);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '1.2+max(2.3,2.5)!'");
}

- (void)testLogFunc
{
    NSString *expr = @"log(5,7)+ln(5)-log2(3)*log10(8)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = logl(5)/logl(7)+logl(5)-log2l(3)*log10l(8);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse 'log(5,7)+ln(5)-log2(3)*log10(8)'");
}

- (void)testDefaultVariable
{
    /** Tested to be fine but the parser does not have initialized dictionary for some reason during the test case **/
    /*
    NSString *expr = @"3*$c/2";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 3.0 * 299792458.0L / 2;
    STAssertEquals([result longDoubleValue], expected, @"Failed to parse '3*$c/2'");
     */
}

- (void)testBinaryNum1
{
    NSString *expr = @"0b1010";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 10.0L;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '0b1010'");
}

- (void)testBinaryNum2
{
    NSString *expr = @"0b1010 << 0b10";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 10 << 2;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '0b1010 << 0b10'");
}

- (void)testHexNum1
{
    NSString *expr = @"0xDEADBEEF";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 0xDEADBEEF;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '0xDEADBEEF'");
}

- (void)testHexNum2
{
    NSString *expr = @"0xBEEF >> 0xF";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 0xBEEF >> 0xF;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '0xBEEF >> 0xF'");
}

- (void)testOctNum1
{
    NSString *expr = @"037";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 037;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '037'");
}

- (void)testOctNum2
{
    NSString *expr = @"037 >> 012";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 037 >> 012;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '037 >> 012'");
}

- (void)testCustomFunction1
{
    // when test() -> ceil(@0)*max(@1, @0)
//    NSString *expr = @"test(ceil(12.3)*3, h2s(abs(-3.1))/10.2)";
    NSString *expr = @"ceil(ceil(12.3)*3)*max(h2s(abs(-3.1))/10.2, ceil(12.3)*3)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = ceill(ceill(12.3L)*3.0L)*fmaxl((fabsl(-3.1L)*3600.0L)/10.2L, ceill(12.3L)*3.0L);
    NSLog(@"result:   %.30Lf", [result longDoubleValue]);
    NSLog(@"expected: %.30Lf", expected);
    NSString *testA = [NSString stringWithFormat:@"%.4Lf", [result longDoubleValue]];
    NSString *testB = [NSString stringWithFormat:@"%.4Lf", expected];
    XCTAssertEqualObjects(testA, testB, @"Failed to parse 'test(ceil(12.3)*3, h2s(abs(-3.1))/10.2)'");
}

- (void)testOperWithUnary1
{
    NSString *expr = @"12**-3";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = powl(12, -3);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '12**-3");
}

- (void)testOperWithUnary2
{
    NSString *expr = @"12***3";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    XCTAssertTrue([result isMemberOfClass:[NSError class]], @"No error thrown for '12***3'");
    XCTAssertEqual((int)[result code], 101, @"Invalid error thrown for '12***3'");
}

- (void)testNegativeOper1
{
    NSString *expr = @"12*-max(-5,3)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 12*-3;
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '12*-max(-5,3)'");
}

- (void)testNegativeOper2
{
    NSString *expr = @"12**-max(-5,3)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = powl(12, -3);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '12**-max(-5,3)'");
}

- (void)testNegativeOper3
{
    NSString *expr = @"12-(3+5)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = 12-(3+5);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '12-(3+5)'");
}

- (void)testNegativeOper4
{
    NSString *expr = @"12**-(-3+1)";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = powl(12, -(-3+1));
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '12**-(-3+1)'");
}

- (void)testNegativeOper5
{
    NSString *expr = @"12**----3";
    DCMathParser *parser = [[DCMathParser alloc] initWith:expr];
    id result = [parser parse];
    long double expected = powl(12, 3);
    XCTAssertEqual([result longDoubleValue], expected, @"Failed to parse '12**----3'");
}

/** TEST **/
- (void)testLF
{
    long double x = -3.1L;
    Numbers *expr = [[Numbers alloc] initWith:-3.1L];
    x = fabsl(x);
    expr = [[Numbers alloc] initWith:fabsl([expr longDoubleValue])];
    XCTAssertEqual([expr longDoubleValue], x, @"-3.1L != expr.longDoubleValue");
}

- (void)testLongDouble
{
    long double a = -3.1L;
    long double b = -3.1L;
    XCTAssertEqual(a, b, @"a != b");
    XCTAssertTrue(fabsl(a) == fabsl(b), @"fabsl(a) != fabsl(b); a == b");
//    STAssertEquals(fabsl(a), fabsl(b), @"fabsl(a) != fabsl(b); a == b"); // doesn't work for some reason
}

- (void)testR
{
    NSLog(@"%d, %Lf", -35>>5, (long double)(-35>>5));
    NSLog(@"%Lf", [[[[DCMathParser alloc] initWith:@"-35>>>5"] parse] longDoubleValue]);
    XCTAssertEqual([[[[DCMathParser alloc] initWith:@"-35>>5"] parse] longDoubleValue], (long double) (-35 >> 5));
}
/** TEST **/

@end
