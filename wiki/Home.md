[TOC]
# Calculatory

This is a wiki for ***Calculatory***.

## Home

### Brief Introduction

The ```Calculatory``` is an Mac OS X application that supports basic to quite complicated formulas on-the-go.
The algorithm to process the given string into a calculation is the [Shunting-yard algorithm](http://en.wikipedia.org/wiki/Shunting-yard_algorithm) by Dijkstra after converting to the [RPN notation](http://en.wikipedia.org/wiki/Reverse_Polish_notation). The application does not utilize any external library, and the code here includes everything needed.  
  
## Main Features

The main features of the Calculatory is to be able to be accessible anywhere in Mac OS X through a hot-key. Some other special features would be the ability to add any custom functions and symbols (variables). The calculator also supports vectors and related operations, which is also a very unique function.  
<br/>
*Have fun!*
