//
//  AdditionalFrame2.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "AdditionalFrame2.h"

@implementation AdditionalFrame2

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [[NSColor colorWithCalibratedRed:0.9294 green:0.9294 blue:0.9294 alpha:1] set];
    NSRectFill([self bounds]);

}

@end
