//
//  Message.h
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 22..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property (nonatomic,strong) NSString *str;

-(id) init;
-(id) initWith:(NSString *)msg;
-(void) appendMsg:(NSString *)msg;
-(void) appendLN;
-(NSString *) print:(NSString *) fmt;
-(id) add:(id)placeholder;
@end
