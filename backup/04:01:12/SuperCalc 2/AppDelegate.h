//
//  AppDelegate.h
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class MainWindowController, QuickModeController, ConfigWindowController;

@interface AppDelegate : NSObject <NSApplicationDelegate> {
    MainWindowController *mainWindowController;
    QuickModeController *quickModeController;
    ConfigWindowController *configWindowController;
    
    IBOutlet NSWindow *mainWindow;
    IBOutlet NSWindow *quickMode;
    IBOutlet NSWindow *configWindow;
    
    IBOutlet NSMenu *statusMenu;
    NSStatusItem *statusItem;
    
}

-(IBAction)showMainWindow:(id)sender;
-(IBAction)showQuickMode:(id)sender;
-(IBAction)showConfigWindow:(id)sender;

-(void)registerHotKeys;

@property (assign) IBOutlet NSWindow *window;

@end
