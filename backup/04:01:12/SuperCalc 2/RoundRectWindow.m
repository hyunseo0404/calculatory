//
//  RoundRectWindow.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "RoundRectWindow.h"

@implementation RoundRectWindow

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    NSBezierPath * path;
    path = [NSBezierPath bezierPathWithRoundedRect:dirtyRect xRadius:8 yRadius:8];
    [[NSColor colorWithCalibratedRed:0 green:0 blue:0 alpha:0.5] set];
    [path fill];
}

@end
