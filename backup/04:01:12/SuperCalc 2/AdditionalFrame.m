//
//  AdditionalFrame.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "AdditionalFrame.h"

@implementation AdditionalFrame

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
//    [[NSColor colorWithCalibratedRed:0 green:0 blue:0 alpha:0.5] set];
//    NSRectFill([self bounds]);
    NSBezierPath * path;
    path = [NSBezierPath bezierPathWithRoundedRect:dirtyRect xRadius:8 yRadius:8];
    [[NSColor colorWithCalibratedRed:0.9294 green:0.9294 blue:0.9294 alpha:1] set];
    [path fill];
}

@end
