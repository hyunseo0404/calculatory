//
//  MainWindowController.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "MainWindowController.h"

@implementation MainWindowController

-(id) init {
    self = [super initWithWindowNibName:@"MainWindow"];
    return self;
}

-(IBAction)add:(id)sender
{
    id ans, temp1, temp2;
    int option1, option2;
    NSString *sf = [sigfig stringValue];
    if ([[[aField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option1 = 0;
    } else {
        @try {
            if ([[[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option1 = 1;
            }
        }
        @catch (NSException *exception) {
            option1 = 10;
        }
    }
    if ([[[bField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option2 = 0;
    } else {
        @try {
            if ([[[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option2 = 1;
            }
        }
        @catch (NSException *exception) {
            option2 = 10;
        }
    }
    if (option1 == 1) {
        if (option2 == 1) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [Vector new];
            NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp2 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 add: temp2];
        }
        else if (option2 == 0) {
            ans = [Message new];
            [ans setStr:@"ILLEGAL INPUT!: CAN'T ADD A VECTOR WITH A SCALAR"];
        }
    }
    else if (option1 == 0) {
        if (option2 == 1) {
            ans = [Message new];
            [ans setStr:@"ILLEGAL INPUT!: CAN'T ADD A SCALAR WITH A VECTOR"];
        }
        else if (option2 == 0) {
            ans = [Numbers new];
            [ans setTo: [aField doubleValue] + [bField doubleValue]];
        }
    }
    if ([[ansField string] isEqualToString:@""]) {
        [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
    }
    else {
        [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
    }
    // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
    [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
}

-(IBAction)subtract:(id)sender
{
    id ans, temp1, temp2;
    int option1, option2;
    NSString *sf = [sigfig stringValue];
    if ([[[aField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option1 = 0;
    } else {
        @try {
            if ([[[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option1 = 1;
            }
        }
        @catch (NSException *exception) {
            option1 = 10;
        }
    }
    if ([[[bField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option2 = 0;
    } else {
        @try {
            if ([[[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option2 = 1;
            }
        }
        @catch (NSException *exception) {
            option2 = 10;
        }
    }
    if (option1 == 1) {
        if (option2 == 1) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [Vector new];
            NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp2 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 subtract: temp2];
        }
        else if (option2 == 0) {
            ans = [Message new];
            [ans setStr:@"ILLEGAL INPUT!: CAN'T SUBTRACT A VECTOR WITH A SCALAR"];
        }
    }
    else if (option1 == 0) {
        if (option2 == 1) {
            ans = [Message new];
            [ans setStr:@"ILLEGAL INPUT!: CAN'T SUBTRACT A SCALAR WITH A VECTOR"];
        }
        else if (option2 == 0) {
            ans = [Numbers new];
            [ans setTo: [aField doubleValue] - [bField doubleValue]];
        }
    }
    if ([[ansField string] isEqualToString:@""]) {
        [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
    }
    else {
        [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
    }
    // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
    [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
}

-(IBAction)multiply:(id)sender
{
    id ans, temp1, temp2;
    int option1, option2;
    NSString *sf = [sigfig stringValue];
    if ([[[aField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option1 = 0;
    } else {
        @try {
            if ([[[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option1 = 1;
            }
        }
        @catch (NSException *exception) {
            option1 = 10;
        }
    }
    if ([[[bField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option2 = 0;
    } else {
        @try {
            if ([[[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option2 = 1;
            }
        }
        @catch (NSException *exception) {
            option2 = 10;
        }
    }
    if (option1 == 1) {
        if (option2 == 1) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [Vector new];
            NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp2 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 multiply:temp2];
        }
        else if (option2 == 0) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [[Numbers alloc] initWith:[bField doubleValue]];
            NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 multiply:temp2];
        }
    }
    else if (option1 == 0) {
        if (option2 == 1) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [[Numbers alloc] initWith:[aField doubleValue]];
            NSArray *input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 multiply:temp2];
        }
        else if (option2 == 0) {
            ans = [Numbers new];
            [ans setTo: [aField doubleValue] * [bField doubleValue]];
        }
    }
    if ([[ansField string] isEqualToString:@""]) {
        [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
    }
    else {
        [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
    }
    // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
    [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
}

-(IBAction)divide:(id)sender
{
    id ans, temp1, temp2;
    int option1, option2;
    NSString *sf = [sigfig stringValue];
    if ([[[aField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option1 = 0;
    } else {
        @try {
            if ([[[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option1 = 1;
            }
        }
        @catch (NSException *exception) {
            option1 = 10;
        }
    }
    if ([[[bField stringValue] componentsSeparatedByString:@","] count] == 1) {
        option2 = 0;
    } else {
        @try {
            if ([[[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","] count] == 3) {
                option2 = 1;
            }
        }
        @catch (NSException *exception) {
            option2 = 10;
        }
    }
    if (option1 == 1) {
        if (option2 == 1) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [Vector new];
            NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp2 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 divide:temp2];
        }
        else if (option2 == 0) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [[Numbers alloc] initWith:[bField doubleValue]];
            NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 divide:temp2];
        }
    }
    else if (option1 == 0) {
        if (option2 == 1) {
            ans = [Vector new];
            temp1 = [Vector new];
            temp2 = [[Numbers alloc] initWith:[aField doubleValue]];
            NSArray *input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
            [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
            ans = [(Vector*)temp1 divide:temp2];
        }
        else if (option2 == 0) {
            ans = [Numbers new];
            [ans setTo: [aField doubleValue] / [bField doubleValue]];
        }
    }
    if ([[ansField string] isEqualToString:@""]) {
        [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
    }
    else {
        [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
    }
    // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
    [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
}

-(IBAction)square:(id)sender
{
    
}

-(IBAction)modular:(id)sender
{
    
}

-(IBAction)magnitude:(id)sender
{
    id ans, temp;
    temp = [Vector new];
    NSString *sf = [sigfig stringValue];
    @try {
        NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
        [temp setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
        input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
        ans = [(Vector*)temp magnitude];
    } @catch (NSException *exception) {
        ans = [[Message alloc] initWith:@"Invalid Input!"];
    }
    @finally {
        if ([[ansField string] isEqualToString:@""]) {
            [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
        }
        else {
            [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
        }
        // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
        [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
    }
}

-(IBAction)normalize:(id)sender
{
    id ans, temp;
    temp = [Vector new];
    NSString *sf = [sigfig stringValue];
    @try {
        NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
        [temp setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
        input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
        ans = [(Vector*)temp normalize];
    } @catch (NSException *exception) {
        ans = [[Message alloc] initWith:@"Invalid Input!"];
    }
    @finally {
        if ([[ansField string] isEqualToString:@""]) {
            [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
        }
        else {
            [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
        }
        // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
        [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
    }
}

-(IBAction)dot:(id)sender
{
    id ans, temp1, temp2;
    NSString *sf = [sigfig stringValue];
    ans = [Vector new];
    temp1 = [Vector new];
    temp2 = [Vector new];
    @try {
        NSArray *input = [[[[[aField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
        [temp1 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
        input = [[[[[bField stringValue] stringByReplacingOccurrencesOfString:@" " withString:@""] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] componentsSeparatedByString:@","];
        [temp2 setTo:[[input objectAtIndex:0] doubleValue] :[[input objectAtIndex:1] doubleValue] :[[input objectAtIndex:2] doubleValue]];
        ans = [(Vector*)temp1 dot:temp2];
    } @catch (NSException *exception) {
        ans = [[Message alloc] initWith:@"Invalid Input!"];
    }
    @finally {
        if ([[ansField string] isEqualToString:@""]) {
            [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
        }
        else {
            [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
        }
        // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
        [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
    }
}

// AUTO CALCULATION ---
/*
 * AUTO CALCULATION USES "MODIFIED" SHUNTING-YARD ALGORITHM TO PARSE THE EQAUTION FROM THE INPUT STRING
 * ALONG WITH THE REVERSE POLISH NOTATION (RPN) TO REPRESENT THE FINAL OUTPUT.
 * 
 * REFER TO THE FOLLOWING LINKS FOR DETAILED EXPLANATION FOR THE ABOVE ALGORITHM:
 * http://en.wikipedia.org/wiki/Shunting_yard_algorithm
 * http://en.wikipedia.org/wiki/Reverse_Polish_notation
 */
-(IBAction)calculate:(id)sender
{
    id ans;
    NSString *sf = [sigfig stringValue];
    NSString *temp = [NSString new];
    temp = [aField stringValue];
    
    EquationParser *eq = [[EquationParser alloc] initWith:temp];
    temp = [eq prepare];
    
    @try {
        // try interpretation + calculation
        // if the input was blank
        if ( [temp isEqualToString:@""] ) {
            ans = [[Message alloc] initWith:@"BLANK INPUT!"];
        }
        // else start interpretation + calculation
        else {
            // use the EquationParser to interpret and calculate
            ans = [eq parse];
            ans = [(Calculator*) ans print:sf];
            ans = [ans stringByReplacingOccurrencesOfString:@"-inf" withString:[NSString stringWithFormat:@"-%C", 0x221E]];
            ans = [ans stringByReplacingOccurrencesOfString:@"inf" withString:[NSString stringWithFormat:@"+%C", 0x221E]];
            ans = [[Message alloc] initWith:ans];
        }
    }
    
    @catch (NSException *exception) {
        [ans appendLN];
        [ans appendLN];
        ans = [[Message alloc] initWith:[exception name]];
        [ans appendMsg:@": "];
        [ans appendMsg:[exception reason]];
        [ans appendLN];
    }
    
    @finally {
        // print the final value
        if ([[ansField string] isEqualToString:@""]) {
            [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
        }
        else {
            [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
        }
        // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
        [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
    }
    
}

// For the textfield (aField),
// overrides the default setting so that when the user types the 'Return' key, 
// the program automatically calculates the input (value).
-(BOOL)control:(NSControl *)control textView:(NSTextView *)fieldEditor
doCommandBySelector:(SEL)commandSelector {
    BOOL retval = NO;
    if (commandSelector == @selector(insertNewline:)) {
        retval = YES;
        [self calculate:[fieldEditor string]];
    }
    return retval;
}

-(IBAction)sendKeys:(id)sender
{
    NSAppleScript *key = [[NSAppleScript alloc] initWithSource:@"tell application ""System Events"" to keystroke ""W"" using {command down}"];
    [key executeAndReturnError:nil];
}

/*
-(void)keyDown:(NSEvent *)theEvent {
    if ([[theEvent keyCode])
}
*/

-(void)awakeFromNib {
//    [self initialize];
//    [NSApp arrangeInFront:self];
//    [[self window] makeKeyAndOrderFront:self];
//    [NSApp activateIgnoringOtherApps:YES];
}

@end
