//
//  QuickModeController.h
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Calculator.h"
#import "EquationParser.h"

@interface QuickModeController : NSWindowController {
    IBOutlet id aField;
    IBOutlet id calculateButton;
    IBOutlet id configButton;
    IBOutlet id ansField;
    
    NSString *temp;
    NSString *ans_str;
}

-(IBAction)calculate:(id)sender;
-(IBAction)ansClicked:(id)sender;
-(void)changeFontToFitRect:(NSString*)str;

@end
