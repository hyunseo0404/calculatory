//
//  Fraction.h
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject

@property int numerator, denominator;

-(NSString *) print:(NSString *) fmt;
-(void) setTo: (int) n over: (int) d;
-(double) convertToNum;
-(id) add: (id) f;
-(Fraction *) subtract: (Fraction *) f;
-(Fraction *) multiply: (Fraction *) f;
-(Fraction *) divide: (Fraction *) f;

-(void) reduce;

//
-(int) gcf;
-(void) simplify;
-(double) decimalize;
//
@end
