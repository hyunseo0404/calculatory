//
//  Numbers.m
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Numbers.h"
#import "Vector.h"

@implementation Numbers

@synthesize n;

-(id) init
{
    return [self initWith:0];
}

-(id) initWith:(double)num
{
    self = [super init];
    if (self) {
        [self setTo:num];
    }
    return self;
}

-(void) setTo:(double)num
{
    n = num;
}

-(NSString *) print:(NSString *)fmt
{
//    NSString *fmt = @"%.5f";
    NSString *tmp = [NSString new];
    tmp = [NSString stringWithFormat:fmt, n];
    return tmp;
}

-(Numbers *) add:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:self.n+num.n];
    return temp;
}

-(Numbers *) subtract:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:self.n-num.n];
    return temp;
}

-(id) multiply:(id)num
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [num isMemberOfClass:[Numbers class]] ) {
            Numbers *temp = [Numbers new];
            [temp setTo:[self n]*[num n]];
            return temp;

        }
        else if ( [num isMemberOfClass:[Vector class]] ) {
            NSLog(@"test1");
            return [(Vector*)num multiply:self];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [num isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self multiply:num];
        }
        else if ( [num isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self multiply:num];
        }
    }
    
    else {
        [NSException raise:@"Invalid multiplication" format:@"unknown multiplication error"];
    }
    return nil;
}

-(id) divide:(id)num
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [num isMemberOfClass:[Numbers class]] ) {
            Numbers *temp = [Numbers new];
            [temp setTo:self.n/[num n]];
            return temp;
        }
        else if ( [num isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector division" format:@"cannot divide a scalar with a vector"];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [num isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self divide:num];
        }
        else if ( [num isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self divide:num];
        }
    }
    
    else {
        [NSException raise:@"Invalid division" format:@"unknown division error"];
    }
    return nil;
}

//-(Numbers *) multiply:(Numbers *)num
//{
//    Numbers *temp = [Numbers new];
//    [temp setTo:self.n*num.n];
//    return temp;
//}

//-(Numbers *) divide:(Numbers *)num
//{
//    Numbers *temp = [Numbers new];
//    [temp setTo:self.n/num.n];
//    return temp;
//}

-(Numbers *) exponentiate:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:(pow(self.n, num.n))];
    return temp;
}
@end
