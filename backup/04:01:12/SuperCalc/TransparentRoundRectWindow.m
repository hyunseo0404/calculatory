//
//  TransparentRoundRectWindow.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "TransparentRoundRectWindow.h"

@implementation TransparentRoundRectWindow

-(id)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag
{
    self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    if (self) {
        [self setAlphaValue:0.75];
        [self setOpaque:YES];
        [self setHasShadow:YES];
        [self setBackgroundColor:[NSColor clearColor]];
        [self setMovableByWindowBackground:YES];
    }
    return self;
}

-(BOOL)canBecomeKeyWindow
{
    return YES;
}

- (void)mouseDown:(NSEvent *)theEvent
{
    initialLocation = [theEvent locationInWindow];
}

-(void)drawRect:(NSRect)rect {
    NSBezierPath * path;
    path = [NSBezierPath bezierPathWithRoundedRect:rect xRadius:8 yRadius:8];
    [[NSColor colorWithCalibratedRed:0 green:0 blue:0 alpha:0.75] set];
    [path fill];
}

@end
