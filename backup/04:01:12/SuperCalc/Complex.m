//
//  Complex.m
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Complex.h"

@implementation Complex

@synthesize real, imaginary;

-(void) setReal:(double)a andImaginary:(double)b
{
    real = a;
    imaginary = b;
}

-(NSString *) print:(NSString *)fmt
{
//    NSString *fmt = @"%.5f + %.5fi";
    NSString *tmp = [NSString new];
    tmp = [NSString stringWithFormat:fmt, real, imaginary];
    return tmp;
}

-(id) add: (id) complexNum;
{
    id result = [Complex new];
    [result setReal: real + [complexNum real]];
    [result setImaginary: imaginary + [complexNum imaginary]];
    return result;
}
@end
