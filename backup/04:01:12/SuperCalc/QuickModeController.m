//
//  QuickModeController.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "QuickModeController.h"

@implementation QuickModeController

-(NSString*) ans_str
{
    return ans_str;
}

-(void) setAns_str:(NSString*) str;
{
    ans_str = str;
}

-(id) init {
    self = [super initWithWindowNibName:@"QuickMode"];
    return self;
}

-(IBAction)calculate:(id)sender
{
    id ans;
//    NSString *sf = [sigfig stringValue];
    NSString *sf = @"%.3f"; // TEST
    temp = [NSString new];
    temp = [aField stringValue];
    
    EquationParser *eq = [[EquationParser alloc] initWith:temp];
    
    @try {
        temp = [eq prepare];
        if ( [temp isEqualToString:@""] ) {
            ans = [[Message alloc] initWith:@"..."];
        }
        else {
            ans = [eq parse];
            ans = [(Calculator*) ans print:sf];
            ans = [ans stringByReplacingOccurrencesOfString:@"-inf" withString:[NSString stringWithFormat:@"-%C", 0x221E]];
            ans = [ans stringByReplacingOccurrencesOfString:@"inf" withString:[NSString stringWithFormat:@"+%C", 0x221E]];
            ans = [[Message alloc] initWith:ans];
        }
    }
    
    @catch (NSException *exception) {
        ans = [[Message alloc] initWith:@"..."];
    }
    
    @finally {
        ans_str = [(Calculator*)ans print:sf];
        NSLog(@"%@", ans_str);
        ans = [NSString stringWithFormat:@"= %@", ans_str];
        [ansField setStringValue:ans];
    }
}

-(IBAction)ansClicked:(id)sender
{
    if ([[ansField stringValue] isNotEqualTo:@""] && [[ansField stringValue] isNotEqualTo:@"= ..."]) {
        NSPasteboard *pasteBoard = [NSPasteboard generalPasteboard];
        [pasteBoard declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:nil];
        [pasteBoard setString:ans_str forType:NSStringPboardType];
    }
}

-(void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == aField) {
        if ([[aField stringValue] isNotEqualTo:@""]) {
            [self calculate:[aField stringValue]];
        } else {
            [ansField setStringValue:@""];
        }
    }
}

// For the textfield (aField),
// overrides the default setting so that when the user types the 'Return' key, 
// the program automatically calculates the input (value).
-(BOOL)control:(NSControl *)control textView:(NSTextView *)fieldEditor
doCommandBySelector:(SEL)commandSelector {
    BOOL retval = NO;
    if (commandSelector == @selector(insertNewline:)) {
        retval = YES;
        [self ansClicked:nil];
    }
    return retval;
}

// refocus on the previous working app when quick mode is closed
-(void)windowWillClose:(NSNotification *)notification {
    [NSApp hide:nil];
}

-(void)cancelOperation:(id)sender {
    [[NSApp keyWindow] orderOut:sender];
    [NSApp hide:nil];
}

/*
-(void)keyDown:(NSEvent *)theEvent
{
    NSLog(@"testest");
    if ([theEvent keyCode] == 53) {
        NSLog(@"test");
//        [[self window] close];
    }
}
*/


@end
