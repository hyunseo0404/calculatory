//
//  EquationParser.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 22..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "EquationParser.h"

@implementation EquationParser

@synthesize equation, ans;

// CONSTANTS
const double g = 9.8;
const double G = 6.67e-11;
const double c = 3e8;
const double pi = M_PI;
const NSArray* possibleStr = nil;
const NSArray* possibleNum = nil;

id memory, long_memory;

-(id) init
{
    return [self initWith:@""];
}

-(id) initWith:(NSString *)input
{
    self = [super init];
    if (self) {
        [self setEquation:input];
        [self initialize];
    }
    return self;
}

-(void)initialize {
    if(!possibleStr)
        possibleStr = [[NSArray alloc] initWithObjects:
                       @"(",@")",@"<",@">",@",",@"**",@"^",@"*",@"/",@"+",@"-",@".",@"e",@"E", nil];
    if(!possibleNum)
        possibleNum = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
}

NSMutableArray *parts;
NSMutableArray *paren_count;

-(id) prepare
{
    NSString *temp = [[NSString alloc] initWithString:equation];
    // replace any white space
    temp = [temp stringByReplacingOccurrencesOfString:@" " withString:@""];
    // replace any valid variables (currently: pi, g, G, c)
    temp = [temp stringByReplacingOccurrencesOfString:@"pi" withString:[NSString stringWithFormat:@"%f", pi]];
    temp = [temp stringByReplacingOccurrencesOfString:@"**" withString:@"^"];
    
    int p1=0, p2=0;
    // see if number of parentheses match
    for ( int i = 0; i < [temp length]; i++ ) {
        if ([temp characterAtIndex:i] == '(') {
            p1++;
        }
        else if ([temp characterAtIndex:i] == ')') {
            p2++;
        }
    }
    
    // if the number of parentheses do match
    if (p1==p2) {
        NSLog(@"match"); // TEST
    } else {
        [NSException raise:@"Evaluation error" format:@"unmatched parentheses"];
    }
    
    // replace any invalid character
    for ( int i = 0; i < [temp length]; i++ ) {
        if ( [possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:i]]] || [possibleStr containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:i]]] ) {}
        else if ( [temp length] >= 4 ) {
            int length = 4;
            if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"sin("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer==0) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '.') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Numbers class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                    j = i;
                    j--;
                }
                else {
                    num_tmp = sin([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"cos("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer==0) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '.') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Numbers class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                    j = i;
                    j--;
                }
                else {
                    num_tmp = cos([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"tan("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer==0) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '.') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Numbers class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                    j = i;
                    j--;
                }
                else {
                    num_tmp = tan([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"sec("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer==0) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '.') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Numbers class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                    j = i;
                    j--;
                }
                else {
                    num_tmp = acos([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"csc("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer==0) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '.') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Numbers class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                    j = i;
                    j--;
                }
                else {
                    num_tmp = asin([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"cot("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer==0) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '.') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Numbers class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                    j = i;
                    j--;
                }
                else {
                    num_tmp = atan([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [[temp substringWithRange:NSMakeRange(i, 4)] isEqualTo:@"mag("] ) {
                int count = 0;
                int j = i+length;
                BOOL needed=NO;
                int countPer=0;
                int countPar=1;
                while ( countPar!=0 ) {
                    if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ((([temp characterAtIndex:j-1] == ' ' && [temp characterAtIndex:j-2] == ',')
                              || [temp characterAtIndex:j-1] == ',') && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                    else if ([temp characterAtIndex:j] == ' ') {}
                    else if ([temp characterAtIndex:j] == '(') {
                        countPar++;
                    } else if ([temp characterAtIndex:j] == ')') {
                        countPar--;
                    } else if ([temp characterAtIndex:j] == '.') {
                        if (countPer<3) {
                            countPer++;
                        } else {
                            needed = YES;
                        }
                    } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                               && [temp characterAtIndex:j] != '<' && [temp characterAtIndex:j] != '>'  && [temp characterAtIndex:j] != ',') {
                        needed = YES;
                    }
                    count++;
                    j++;
                }
                j++;
                count--;
                j--;
                Vector *vec_tmp;
                double num_tmp;
                if (needed) {
                    NSString *newTemp;
                    while (![newTemp isMemberOfClass:[Vector class]]) {
                        EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                        newTemp = [newParser prepare];
                        newTemp = [newParser parse];
                    }
                    NSString* vec_tmp_str = [(Calculator*)newTemp print:@"%f"];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:vec_tmp_str];
                    NSLog(@"testing: %@", vec_tmp_str);
                    NSLog(@"testing2: %@", temp);
                    j = i;
                    j--;
                }
                else {
                    vec_tmp = [[Vector alloc] initWithStr:[temp substringWithRange:NSMakeRange(i+length, count)]]; // CHANGE THIS PART!!
                    num_tmp = [[[vec_tmp magnitude] print:@"%f"] doubleValue];
                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                }
                if ( num_tmp != num_tmp ) {
                    [NSException raise:@"Invalid input" format:@"math domain error"];
                }
                i = j;
            }
            else if ( [temp length] >= 5 ) {
                int length = 5;
                if ( [[temp substringWithRange:NSMakeRange(i, 5)] isEqualTo:@"norm("] ) {
                    int count = 0;
                    int j = i+length;
                    BOOL needed=NO;
                    int countPer=0;
                    int countPar=1;
                    while ( countPar!=0 ) {
                        if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                        else if ((([temp characterAtIndex:j-1] == ' ' && [temp characterAtIndex:j-2] == ',')
                                  || [temp characterAtIndex:j-1] == ',') && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                        else if ([temp characterAtIndex:j] == ' ') {}
                        else if ([temp characterAtIndex:j] == '(') {
                            countPar++;
                        } else if ([temp characterAtIndex:j] == ')') {
                            countPar--;
                        } else if ([temp characterAtIndex:j] == '.') {
                            if (countPer<3) {
                                countPer++;
                            } else {
                                needed = YES;
                            }
                        } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                                   && [temp characterAtIndex:j] != '<' && [temp characterAtIndex:j] != '>'  && [temp characterAtIndex:j] != ',') {
                            needed = YES;
                        }
                        count++;
                        j++;
                    }
                    j++;
                    count--;
                    j--;
                    Vector *vec_tmp;
                    if (needed) {
                        NSString *newTemp;
                        while (![newTemp isMemberOfClass:[Vector class]]) {
                            EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                            newTemp = [newParser prepare];
                            newTemp = [newParser parse];
                        }
                        NSString* vec_tmp_str = [(Calculator*)newTemp print:@"%f"];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:vec_tmp_str];
                        j = i;
                        j--;
                    }
                    else {
                        vec_tmp = [[Vector alloc] initWithStr:[temp substringWithRange:NSMakeRange(i+length, count)]]; // CHANGE THIS PART!!
                        vec_tmp = [vec_tmp normalize];
                        NSString* vec_tmp_str = [vec_tmp print:@"%f"];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:vec_tmp_str];
                    }
                    i = j;
                }
                else if ( [[temp substringWithRange:NSMakeRange(i, 5)] isEqualTo:@"sqrt("] ) {
                    int count = 0;
                    int j = i+length;
                    BOOL needed=NO;
                    int countPer=0;
                    int countPar=1;
                    while ( countPar!=0 ) {
                        if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                        else if ([temp characterAtIndex:j] == '(') {
                            countPar++;
                        } else if ([temp characterAtIndex:j] == ')') {
                            countPar--;
                        } else if ([temp characterAtIndex:j] == '.') {
                            if (countPer==0) {
                                countPer++;
                            } else {
                                needed = YES;
                            }
                        } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]) {
                            needed = YES;
                        }
                        count++;
                        j++;
                    }
                    j++;
                    count--;
                    j--;
                    double num_tmp;
                    if (needed) {
                        NSString *newTemp;
                        while (![newTemp isMemberOfClass:[Numbers class]]) {
                            EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                            newTemp = [newParser prepare];
                            newTemp = [newParser parse];
                        }
                        num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                        NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                        j = i;
                        j--;
                    }
                    else {
                        num_tmp = sqrt([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]); // CHANGE THIS PART!!
                        NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                    }
                    if ( num_tmp != num_tmp ) {
                        [NSException raise:@"Invalid input" format:@"math domain error"];
                    }
                    i = j;
                }
            }
        }
        else if ( [temp characterAtIndex:i] == 'g' ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", g]];
        }
        else if ( [temp characterAtIndex:i] == 'G' ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", G]];
        }
        else if ( [temp characterAtIndex:i] == 'c' ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", c]];
        }
        else {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@""];
            i--;
        }
    }
    
    self.equation = temp;
    return temp;
    
}

-(id) parse
{
    
    NSArray* possibleNum = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"e",@"E", nil];
    NSArray* possibleOper = [[NSArray alloc] initWithObjects:@"**",@"^",@"*",@"/",@"+",@"-", @".", nil];
    
    NSArray* third = [[NSArray alloc] initWithObjects:@"^", nil];
    NSArray* second = [[NSArray alloc] initWithObjects:@"*", @"/", @".", nil];
    NSArray* first = [[NSArray alloc] initWithObjects:@"+", @"-", nil];
    
    NSMutableArray *output = [NSMutableArray new];
    NSMutableArray *operator = [NSMutableArray new];
    int unary = 1;
    char c;
    NSString *cur;
    
    for ( int i = 0; i < [equation length]; i++ ) {
        
        c = [equation characterAtIndex:i];
        cur = [NSString stringWithFormat:@"%c", c];
        
        // check for unary operator (in the beginning)
        if ( i == 0 && (c == '+' || c == '-') ) {
            if (c == '+') unary = 1;
            else if (c == '-') unary = -1;
        }
        
        // check for unary operator
        else if ( (c == '+' || c == '-') && ([possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]]) ) {
            if ( [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i+1]]] ) {
                [NSException raise:@"evaluation error" format:@"Two or more operators found in a row"];
            }
            else {
                if (c == '+') unary = 1;
                else if (c == '-') unary = -1;
            }
        }
        
        // check for numbers
        else if ( [possibleNum containsObject:cur] || (i==0 && c=='.') || ([possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]] && c == '.') ) {
            NSString *num = @"";
            while ( i < [equation length] && ( ([possibleNum containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i]]] 
                   || [equation characterAtIndex:i] == '.') || (([equation characterAtIndex:i] == '-' || [equation characterAtIndex:i] == '+')
                   && ([equation characterAtIndex:i-1] == 'e' || [equation characterAtIndex:i-1] == 'E') 
                   && [possibleNum containsObject:[NSString stringWithFormat:@"%c", [equation characterAtIndex:i+1]]]) ) ) {
                num = [NSString stringWithFormat:@"%@%c",num,[equation characterAtIndex:i]];
                i++;
            }
            // add the number to the output
            [output addObject:[[Numbers alloc] initWith:unary*[num doubleValue]]];
            // reset num
            num = nil;
            // reset unary
            unary = 1;
            i--;
        }
        
        // check for vectors
        else if ( c == '<' ) {
            NSArray *temp = [NSArray new];
            Vector *vec = [Vector new];
            int count = 0;
            i++;
            int origin = i;
            NSString *num = @"";
            while ( i < [equation length] && [equation characterAtIndex:i] != '>' ) {
                count++;
                i++;
            }
            num = [num stringByAppendingString:[equation substringWithRange:NSMakeRange(origin, count)]];
            num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
            temp = [num componentsSeparatedByString:@","];
            [vec setTo:[[temp objectAtIndex:0] doubleValue] :[[temp objectAtIndex:1] doubleValue] :[[temp objectAtIndex:2] doubleValue]];
            // add the vector to the output
            [output addObject:vec];
        }
        
        // check for operators
        else if ( [possibleOper containsObject:cur] || c == '(' || c == ')' ) {
            
            if ( [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]]
                     && [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i+1]]] ) {
                [NSException raise:@"evaluation error" format:@"Two or more operators found in a row"];
            }
            else {
                
                // SHUNTING YARD ALGORITHM
                
                // precedence 1: '+', '-'
                if ( [first containsObject:cur] ) {
                    
                    int a = (int) [operator count]-1;
                    
                    if ([operator count] == 0) {
                        [operator addObject:cur];
                    }
                    else {
                        if ( [[operator objectAtIndex:0] isEqualTo:@"("] ) {
                            [operator insertObject:cur atIndex:0];
                        }
                        else {
                            while ( a >= 0 && ![[operator objectAtIndex:0] isEqualTo:@"("] ) {
                                [output addObject:[operator objectAtIndex:0]];
                                [operator removeObjectAtIndex:0];
                                a--;
                            }
                            [operator insertObject:cur atIndex:0];
                        }
                    }
                }
                
                // precedence 2: '*', '/', '.'
                else if ( [second containsObject:cur] ) {
                    
                    int a = (int) [operator count]-1;
                    
                    if ([operator count] == 0) {
                        [operator addObject:cur];
                    }
                    else {
                        if ( [[operator objectAtIndex:0] isEqualTo:@"("] ) {
                            [operator insertObject:cur atIndex:0];
                        }
                        else {
                            while ( a >= 0 && ![[operator objectAtIndex:0] isEqualTo:@"("] && ![first containsObject:[operator objectAtIndex:0]] ) {
                                [output addObject:[operator objectAtIndex:0]];
                                [operator removeObjectAtIndex:0];
                                a--;
                            }
                            [operator insertObject:cur atIndex:0];
                        }
                    }
                }
                
                // precedence 3: '^' ('**')
                else if ( [third containsObject:cur] ) {
                    [operator insertObject:cur atIndex:0];                   
                }
                
                // open parenthesis
                else if ( c == '(' ) {
                    [operator insertObject:cur atIndex:0];
                }
                
                // close parenthesis
                else if ( c == ')' ) {
                    
                    int a = (int) [operator count]-1;
                    
                    while ( a >= 0 && ![[operator objectAtIndex:0] isEqualTo:@"("] ) {
                        [output addObject:[operator objectAtIndex:0]];
                        [operator removeObjectAtIndex:0];
                        a--;
                    }
                    [operator removeObjectAtIndex:0];
                }
                
            }
        }
        
        // raise exception
        else {
            [NSException raise:@"Evaluation error" format:[NSString stringWithFormat:@"unknown case: %c",c]];
        }
        
        
        // TEST
        NSLog(@"%@", [NSString stringWithFormat:@"output: %@,\noperator: %@",[output description],[operator description]]);
        
    }
    
    // pop entire stack to output
    if ( [operator count] != 0 ) {
        while ( [operator count] != 0 ) {
            [output addObject:[operator objectAtIndex:0]];
            [operator removeObjectAtIndex:0];
        }
    }
    
    // double-check to see that the operator stack is now empty
    if ( [operator count] != 0 ) {
        [NSException raise:@"evaluation error" format:@"operator not clean up properly"];
    }
    
    // TEST (prints the final postfix evaluation result)
    NSString *final = @"";
    for (int i = 0; i < [output count]; i++) {
        if ([[output objectAtIndex:i] isKindOfClass:[NSString class]]) {
            final = [final stringByAppendingString:[output objectAtIndex:i]];
        }
        else {
            final = [final stringByAppendingString:[(Calculator*)[output objectAtIndex:i] print:@"%.0f"]];
        }
        final = [final stringByAppendingString:@" "];
    }
    NSLog(@"%@", final);
    // TEST
    
    NSLog(@"%@",[NSString stringWithFormat:@"equation: %@,\noutput: %@,\noperator: %@,\n\nFINAL: %@",equation,[output description],[operator description], final]);
    
    NSString *t = [NSString new];
    if ( [output count] != 1 ) {
        t = [self evalulate:output];
    } else {
        t = [output objectAtIndex:0];
    }
    
    return t;
}

-(id) evalulate:(NSMutableArray *)output
{
    NSMutableArray *stack = [NSMutableArray new];
    
    while ( [output count] != 0 ) {
        while ( [output count] != 0 && ([[output objectAtIndex:0] isMemberOfClass:[Numbers class]] || [[output objectAtIndex:0] isMemberOfClass:[Vector class]]) ) {
            [stack addObject:[output objectAtIndex:0]];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        }
        if ( [[output objectAtIndex:0] isEqualTo:@"^"] ) {
            [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] exponentiate:[stack objectAtIndex:[stack count]-1]]];
            [stack removeObjectAtIndex:[stack count]-2];
            [stack removeObjectAtIndex:[stack count]-2];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        } else if ( [[output objectAtIndex:0] isEqualTo:@"*"] ) {
            [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] multiply:[stack objectAtIndex:[stack count]-1]]];
            [stack removeObjectAtIndex:[stack count]-2];
            [stack removeObjectAtIndex:[stack count]-2];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        } else if ( [[output objectAtIndex:0] isEqualTo:@"/"] ) {
            [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] divide:[stack objectAtIndex:[stack count]-1]]];
            [stack removeObjectAtIndex:[stack count]-2];
            [stack removeObjectAtIndex:[stack count]-2];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        } else if ( [[output objectAtIndex:0] isEqualTo:@"+"] ) {
            [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] add:[stack objectAtIndex:[stack count]-1]]];
            [stack removeObjectAtIndex:[stack count]-2];
            [stack removeObjectAtIndex:[stack count]-2];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        } else if ( [[output objectAtIndex:0] isEqualTo:@"-"] ) {
            [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] subtract:[stack objectAtIndex:[stack count]-1]]];
            [stack removeObjectAtIndex:[stack count]-2];
            [stack removeObjectAtIndex:[stack count]-2];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        } else if ( [[output objectAtIndex:0] isEqualTo:@"."] ) {
            [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] dot:[stack objectAtIndex:[stack count]-1]]];
            [stack removeObjectAtIndex:[stack count]-2];
            [stack removeObjectAtIndex:[stack count]-2];
            [output removeObjectAtIndex:0];
            NSLog(@"%@,%@",[stack description], [output description]);
        }
    }
    
    return [stack objectAtIndex:0];
    
}
@end
