//
//  Calculator.m
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

-(NSString *) print:(NSString *)fmt
{
    return [self print:fmt];
}

-(id) add:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self add:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector addition" format:@"cannot add a scalar with a vector"];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self add:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            [NSException raise:@"Invalid vector addition" format:@"cannot add a vector with a scalar"];
        }
    }
    
    else {
        [NSException raise:@"Invalid addition" format:@"unknown addition error"];
    }
    return nil;
}

-(id) subtract:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self subtract:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector subtraction" format:@"cannot subtract a scalar with a vector"];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self subtract:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            [NSException raise:@"Invalid vector subtraction" format:@"cannot subtract a vector with a scalar"];
        }
    }
    
    else {
        [NSException raise:@"Invalid subtraction" format:@"unknown subtraction error"];
    }
    return nil;
}

-(id) multiply:(id)n
{
    NSLog(@"test0");
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self multiply:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            NSLog(@"test1");
            return [(Vector*)n multiply:self];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self multiply:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self multiply:n];
        }
    }

    else {
        [NSException raise:@"Invalid multiplication" format:@"unknown multiplication error"];
    }
    return nil;
}

-(id) divide:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self divide:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid vector division" format:@"cannot divide a scalar with a vector"];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self divide:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self divide:n];
        }
    }
    
    else {
        [NSException raise:@"Invalid division" format:@"unknown division error"];
    }
    return nil;
}

-(id) dot:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            [NSException raise:@"Invalid dot operation" format:@"cannot use a dot operation with scalars"];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)n dot:self];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            return [(Vector*)self dot:n];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self dot:n];
        }
    }
    
    else {
        [NSException raise:@"Invalid dot operation" format:@"unknown dot operation error"];
    }
    return nil;
}

-(id) exponentiate:(id)n
{
    if ( [self isMemberOfClass:[Numbers class]] ) {
        if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Numbers*)self exponentiate:n];
        }
        else if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid exponentiation" format:@"cannot exponentiate 'to' a vector"];
        }
    }
    
    else if ( [self isMemberOfClass:[Vector class]] ) {
        if ( [n isMemberOfClass:[Vector class]] ) {
            [NSException raise:@"Invalid exponentiation" format:@"cannot exponentiate 'to' a vector"];
        }
        else if ( [n isMemberOfClass:[Numbers class]] ) {
            return [(Vector*)self exponentiate:n];
        }
    }
    
    else {
        [NSException raise:@"Invalid exponentiation" format:@"unknown exponentiation error"];
    }
    return nil;
}
@end
