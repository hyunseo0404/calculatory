//
//  Vector.m
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Vector.h"

@implementation Vector

@synthesize x, y, z;

-(id) init
{
    return [self initWith:0 :0 :0];
}

-(id) initWith:(double)a :(double)b :(double)c
{
    self = [super init];
    if (self) {
        [self setTo:a :b :c];
    }
    return self;
}

-(id) initWithStr:(NSString *)str
{
    self = [super init];
    if (self) {
        NSArray *temp = [NSArray new];
        str = [[[str stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
        temp = [str componentsSeparatedByString:@","];
        [self setTo:[[temp objectAtIndex:0] doubleValue] :[[temp objectAtIndex:1] doubleValue] :[[temp objectAtIndex:2] doubleValue]];
    }
    return self;
}

-(void) setTo:(double)a :(double)b :(double)c
{
    x = a;
    y = b;
    z = c;
}

-(NSString *) print:(NSString *) fmt
{
    NSString *fmt_tmp = [NSString stringWithFormat:@"<%@, %@, %@>", fmt, fmt, fmt];
    NSString *tmp = [NSString new];
    tmp = [NSString stringWithFormat:fmt_tmp, x, y, z];
    return tmp;
}

-(Vector *) add: (Vector *) v
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:(self.x+v.x) :(self.y+v.y) :(self.z+v.z)];
    return temp;
}

-(Vector *) subtract: (Vector *) v
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:(self.x-v.x) :(self.y-v.y) :(self.z-v.z)];
    return temp;
}

-(Vector *) multiply: (id) v
{
    if ( [v isMemberOfClass:[Vector class]] ) {
        v = (Vector*) v;
        Vector *temp = [[Vector alloc] init];
        [temp setTo:(self.x*[v x]) :(self.y*[v y]) :(self.z*[v z])];
        return temp;
    }
    else if ( [v isMemberOfClass:[Numbers class]] ) {
        v = (Numbers*) v;
        Vector *temp = [[Vector alloc] init];
        [temp setTo:(self.x*[v n]) :(self.y*[v n]) :(self.z*[v n])];
        return temp;
    }
    else {
        [NSException raise:@"Invalid vector multiplication" format:@"attempt to multiply vector with unknown object"];
    }
    return v;
}

/*
-(Vector *) multiply: (Vector *) v
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:(self.x*v.x) :(self.y*v.y) :(self.z*v.z)];
    return temp;
}
 
-(Vector *) multiplyNum: (Numbers *) n
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:(self.x*n.n) :(self.y*n.n) :(self.z*n.n)];
    return temp;
}
*/

-(Vector *) divide:(id) v
{
    if ( [v isMemberOfClass:[Vector class]] ) {
        v = (Vector*) v;
        Vector *temp = [[Vector alloc] init];
        [temp setTo:(self.x/[v x]) :(self.y/[v y]) :(self.z/[v z])];
        return temp;
    }
    else if ( [v isMemberOfClass:[Numbers class]] ) {
        v = (Numbers*) v;
        Vector *temp = [[Vector alloc] init];
        [temp setTo:(self.x/[v n]) :(self.y/[v n]) :(self.z/[v n])];
        return temp;
    }
    else {
        [NSException raise:@"Invalid vector multiplication" format:@"attempt to multiply vector with unknown object"];
    }
    return v;
}

/*
-(Vector *) divide: (Vector *) v
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:(self.x/v.x) :(self.y/v.y) :(self.z/v.z)];
    return temp;
}

-(Vector *) divideNum: (Numbers *) n
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:(self.x/n.n) :(self.y/n.n) :(self.z/n.n)];
    return temp;
}
*/

-(Vector *) exponentiate: (Numbers *) n
{
    Vector *temp = [[Vector alloc] init];
    [temp setTo:pow(self.x,(int)n) :pow(self.y,(int)n) :pow(self.y,(int)n)];
    return temp;
}

-(Numbers *) dot: (id) v
{
    if ( [v isMemberOfClass:[Vector class]] ) {
        Numbers *temp = [[Numbers alloc] init];
        [temp setTo:(self.x*[v x] + self.y*[v y] + self.z*[v z])];
        return temp;
    }
    else if ( [v isMemberOfClass:[Numbers class]] ) {
        Numbers *temp = [[Numbers alloc] init];
        [temp setTo:(self.x*[v n] + self.y*[v n] + self.z*[v n])];
        return temp;
    }
    return nil;
}

-(Numbers *) magnitude
{
    Numbers *temp = [[Numbers alloc] init];
    [temp setTo:(sqrt(pow(self.x,2)+pow(self.y,2)+pow(self.z,2)))];
    return temp;
}

-(Vector *) normalize
{
    return [self divide:[self magnitude]];
}
@end
