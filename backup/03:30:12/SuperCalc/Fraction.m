//
//  Fraction.m
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Fraction.h"

@implementation Fraction

@synthesize numerator, denominator;

-(NSString *) print:(NSString *)fmt
{
    BOOL sign;
    int tmp_num = numerator;
    int tmp_denom = denominator;
    
    NSString *tmp = [NSString new];
    
    if ( ( numerator > 0 && denominator > 0 ) || ( numerator < 0 && denominator < 0 ) )
        sign = true;
    else if ( numerator < 0 || denominator < 0 )
        sign = false;
    
    tmp_num = abs(numerator);
    tmp_denom = abs(denominator);
    
    if ( tmp_num > tmp_denom )
        if ( sign )
            tmp = [NSString stringWithFormat:@"%i %i/%i", tmp_num/tmp_denom, tmp_num-tmp_num/tmp_denom*tmp_denom, tmp_denom];
        else
            tmp = [NSString stringWithFormat:@"-%i %i/%i", tmp_num/tmp_denom, tmp_num-tmp_num/tmp_denom*tmp_denom, tmp_denom];
        else if ( tmp_num == tmp_denom )
            if ( sign )
                tmp = [NSString stringWithFormat:@" 1"];
            else
                tmp = [NSString stringWithFormat:@"-1"];
            else
                if ( sign )
                    tmp = [NSString stringWithFormat:@"%i/%i", tmp_num, tmp_denom];
                else
                    tmp = [NSString stringWithFormat:@"-%i/%i", tmp_num, tmp_denom];
    
    return tmp;
    
}

-(void) setTo:(int)n over:(int)d
{
    numerator = n;
    denominator = d;
}

-(double) convertToNum
{
    if (denominator != 0)
        return (double) numerator / denominator;
    else
        return NAN;
}

-(id) add:(id)f
{
    id result = [Fraction new];
    [result setNumerator: numerator * [f denominator] + denominator * [f numerator]];
    [result setDenominator: denominator * [f denominator]];
    
    [result reduce];
    //    [result simplify];
    
    return result;
}

-(Fraction *) subtract:(Fraction *)f
{
    Fraction *result = [Fraction new];
    result.numerator = numerator * f.denominator - denominator * f.numerator;
    result.denominator = denominator * f.denominator;
    
    [result reduce];
    
    return result;
}

-(Fraction *) multiply:(Fraction *)f
{
    Fraction *result = [Fraction new];
    result.numerator = numerator * f.numerator;
    result.denominator = denominator * f.denominator;
    
    [result reduce];
    
    return result;
}

-(Fraction *) divide:(Fraction *)f
{
    Fraction *result = [Fraction new];
    result.numerator = numerator * f.denominator;
    result.denominator = denominator * f.numerator;
    
    [result reduce];
    
    return result;
}

-(void) reduce
{
    int u = numerator;
    int v = denominator;
    int temp;
    
    while ( v != 0 ) {
        temp = u % v;
        u = v;
        v = temp;
    }
    
    numerator /= u;
    denominator /= u;
}


// just for my convinience
-(void) simplify
{
    int factor = [self gcf];
    numerator /= factor;
    denominator /= factor;
}

-(int) gcf
{
    int factor;
    if ( denominator != 0 ) {
        if ( numerator < denominator ) {
            for ( int i = 1; i <= numerator; i++ ) {
                if ( numerator % i == 0 && denominator % i == 0 ) {
                    factor = i;
                }
            }
        }
        else if ( numerator > denominator ) {
            for ( int j = 1; j <= denominator; j++ ) {
                if ( numerator % j == 0 && denominator % j == 0 ) {
                    factor = j;
                }
            }
        }
    }
    else {
        factor = 1;
    }
    return factor;
}

-(double) decimalize
{
    return (double) numerator / denominator;
}
@end
