//
//  EquationParser.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 22..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "EquationParser.h"

@implementation EquationParser

@synthesize equation, ans;

// CONSTANTS
//const double g = 9.8;
//const double G = 6.67e-11;
//const double c = 3e8;
//const double pi = M_PI;
//const NSArray* possibleStr = nil;
//const NSArray* possibleNum = nil;

id memory, long_memory;

-(id) init
{
//    if(!possibleStr)
//        possibleStr = [[NSArray alloc] initWithObjects:
//                       @"(",@")",@"<",@">",@",",@"**",@"^",@"*",@"/",@"+",@"-",@".", nil];
//    if(!possibleNum)
//        possibleNum = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    return [self initWith:@""];
}

-(id) initWith:(NSString *)input
{
    self = [super init];
    if (self) {
        [self setEquation:input];
    }
    return self;
}

NSMutableArray *parts;
NSMutableArray *paren_count;

-(id) evalulate
{
    
    // CONSTANTS
    //const double g = 9.8;
    //const double G = 6.67e-11;
    //const double c = 3e8;
    //const double pi = M_PI;
    //const NSArray* possibleStr = nil;
    //const NSArray* possibleNum = nil;
    
    NSArray* possibleStr = [[NSArray alloc] initWithObjects:@"(",@")",@"<",@">",@",",@"**",@"^",@"*",@"/",@"+",@"-",@".", nil];
    NSArray* possibleNum = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
    NSArray* possibleOper = [[NSArray alloc] initWithObjects:@"**",@"^",@"*",@"/",@"+",@"-", @".", nil];
    
    NSArray* third = [[NSArray alloc] initWithObjects:@"^", nil];
    NSArray* second = [[NSArray alloc] initWithObjects:@"*", @"/", @".", nil];
    NSArray* first = [[NSArray alloc] initWithObjects:@"+", @"-", nil];
    
    int p1=0, p2=0;
    // see if number of parentheses match
    for ( int i = 0; i < [equation length]; i++ ) {
        if ([equation characterAtIndex:i] == '(') {
            p1++;
        }
        else if ([equation characterAtIndex:i] == ')') {
            p2++;
        }
    }
    // if the number of parentheses do match
    if (p1==p2) {
        NSLog(@"match"); // TEST
    } else {
        [NSException raise:@"evaluation error" format:@"Unmatched parentheses"];
    }
    
    NSMutableArray *output = [NSMutableArray new];
    NSMutableArray *operator = [NSMutableArray new];
    int unary = 1;
    char c;
    NSString *cur;
    
    for ( int i = 0; i < [equation length]; i++ ) {
        
        c = [equation characterAtIndex:i];
        cur = [NSString stringWithFormat:@"%c", c];
        
        // check for unary operator (in the beginning)
        if ( i == 0 && (c == '+' || c == '-') ) {
            if (c == '+') unary = 1;
            else if (c == '-') unary = -1;
        }
        
        // check for unary operator
        else if ( (c == '+' || c == '-') && ([possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]]) ) {
            if ( [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i+1]]] ) {
                [NSException raise:@"evaluation error" format:@"Two or more operators found in a row"];
            }
            else {
                if (c == '+') unary = 1;
                else if (c == '-') unary = -1;
            }
        }
        
        // check for numbers
        else if ( [possibleNum containsObject:cur] ) {
            NSString *num = @"";
            while ( i < [equation length] && ([possibleNum containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i]]] 
                   || [equation characterAtIndex:i] == '.') ) {
                num = [NSString stringWithFormat:@"%@%c",num,[equation characterAtIndex:i]];
                i++;
            }
            // add the number to the output
            [output addObject:[[Numbers alloc] initWith:unary*[num doubleValue]]];
            // reset num
            num = nil;
            // reset unary
            unary = 1;
            i--;
        }
        
        // check for vectors
        else if ( c == '<' ) {
            Vector *vec = [Vector new];
            int comma = 1;
            i++;
            while ( i < [equation length] && [equation characterAtIndex:i] != '>' ) {
                NSString *num = @"";
                while ( [equation characterAtIndex:i] != ',' && [equation characterAtIndex:i] != '>' && i < [equation length] ) {
                    num = [num stringByAppendingString:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i]]];
                    i++;
                }
                i++;
                switch (comma) {
                    case 1:
                        [vec setX:[num doubleValue]];
                        break;
                    case 2:
                        [vec setY:[num doubleValue]];
                        break;
                    case 3:
                        [vec setZ:[num doubleValue]];
                        break;
                    default:
                        [NSException raise:@"evaluation error" format:@"Too many vector components"];
                        break;
                }
                num = nil;
                i++;
                comma++;
            }
            // add the vector to the output
            [output addObject:vec];
        }
        
        // check for operators
        else if ( [possibleOper containsObject:cur] || c == '(' || c == ')' ) {
            if ( c == '*' && [equation characterAtIndex:i+1] == '*' ) {
                [operator addObject:@"^"];
                i++;
            }
            else if ( [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]]
                     && [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i+1]]] ) {
                [NSException raise:@"evaluation error" format:@"Two or more operators found in a row"];
            }
            else {
                
                // SHUNTING YARD ALGORITHM
                
                // precedence 1: '+', '-'
                if ( [first containsObject:cur] ) {
                    
                    if ([operator count] == 0) {
                        [operator addObject:cur];
                    }
                    else {
                        int a = (int) [operator count]-1;
                        BOOL found = false;
                        
                        if ( [[operator objectAtIndex:a] isEqualTo:@"("] ) {
                            [operator addObject:cur];
                        }
                        else {
                            while ( !found && a >= 0 && ![[operator objectAtIndex:a] isEqualTo:@"("] ) {
                                if ([first containsObject:[operator objectAtIndex:a]]) {
                                    [output addObject:[operator objectAtIndex:a]];
                                    [operator removeObjectAtIndex:a];
                                    [operator insertObject:cur atIndex:a];
                                    found = true;
                                }
                                a--;
                            }
                            if (!found) {
                                int a = (int) [operator count]-1;
                                while ( !found && a >= 0 && ![[operator objectAtIndex:a] isEqualTo:@"("] ) {
                                    a--;
                                }
                                [operator insertObject:cur atIndex:a];
                                found = true;
                            }
                        }
                    }
                }
                
                // precedence 2: '*', '/', '.'
                else if ( [second containsObject:cur] ) {
                    
                    if ([operator count] == 0) {
                        [operator addObject:cur];
                    }
                    else {
                        int a = (int) [operator count]-1;
                        BOOL found = false;
                        
                        if ( [[operator objectAtIndex:a] isEqualTo:@"("] ) {
                            [operator addObject:cur];
                        }
                        else {
                            while ( !found && a >= 0 && ![[operator objectAtIndex:a] isEqualTo:@"("] ) {
                                if ([second containsObject:[operator objectAtIndex:a]]) {
                                    [output addObject:[operator objectAtIndex:a]];
                                    [operator removeObjectAtIndex:a];
                                    [operator insertObject:cur atIndex:a];
                                    found = true;
                                }
                                a--;
                            }
                            if (!found) {
                                int a = (int) [operator count]-1;
                                while ( !found && a >= 0 && ![[operator objectAtIndex:a] isEqualTo:@"("] && ![first containsObject:[operator objectAtIndex:a]] ) {
                                    a--;
                                }
                                [operator insertObject:cur atIndex:a];
                                found = true;
                            }
                        }
                    }
                }
                
                // precedence 3: '^' ('**')
                else if ( [third containsObject:cur] ) {
                    [operator insertObject:cur atIndex:0];                   
                }
                
                // open parenthesis
                else if ( c == '(' ) {
//                    parenFound = true;
                    [operator addObject:cur];
                }
                
                // close parenthesis
                else if ( c == ')' ) {
//                    parenFound = false;
                    int a = (int) [operator count]-1;
                    while (a > 0 && ![[operator objectAtIndex:a] isEqualTo:@"("]) {
                        [output addObject:[operator objectAtIndex:a]];
                        [operator removeObjectAtIndex:a];
                        a--;
                    }
                    [operator removeObjectAtIndex:a];
                }
                
//                [operator addObject:cur];
                
            }
        }
        
        // raise exception
        else {
            [NSException raise:@"evaluation error" format:[NSString stringWithFormat:@"unknown case: %c",c]];
        }
        
        
        // TEST
        NSLog(@"%@", [NSString stringWithFormat:@"output: %@,\noperator: %@",[output description],[operator description]]);
        
    }
    
    // pop entire stack to output
    if ( [operator count] != 0 ) {
        while ( [operator count] != 0 ) {
            [output addObject:[operator objectAtIndex:0]];
            [operator removeObjectAtIndex:0];
        }
    }
    
//    if ( [operator count] != 0 ) {
//        for ( unsigned long i = [operator count]; i > 0; i-- ) {
//            [output addObject:[operator objectAtIndex:i-1]];
//            [operator removeObjectAtIndex:i-1];
//        }
//    }
    
    // double-check to see that the operator stack is now empty
    if ( [operator count] != 0 ) {
        [NSException raise:@"evaluation error" format:@"operator not clean up properly"];
    }
    
    
    /*
    int count = 0;
    BOOL found = NO;
    
    // while not all the parentheses have been identified
    while ([parts count] != p1) {
        [parts addObject:[self getParts:0]];
    }
    
    // sort parentheses (precedence)
    for ( int i = 0; i < [equation length]; i++ ) {
        while ( i < [equation length] && [equation characterAtIndex:i] != ')' ) {
            if ( [equation characterAtIndex:i] == '(' ) {
                count++;
            }
            i++;
        }
        i--;
    }
    */
    
    // TEST (prints the final postfix evaluation result)
    NSString *final = @"";
    for (int i = 0; i < [output count]; i++) {
        if ([[output objectAtIndex:i] isKindOfClass:[NSString class]]) {
            final = [final stringByAppendingString:[output objectAtIndex:i]];
        }
        else {
            final = [final stringByAppendingString:[(Calculator*)[output objectAtIndex:i] print:@"%.0f"]];
        }
        final = [final stringByAppendingString:@" "];
    }
    NSLog(@"%@", final);
    // TEST
    
    return [NSString stringWithFormat:@"equation: %@,\noutput: %@,\noperator: %@,\n\nFINAL: %@",equation,[output description],[operator description], final];
//    return equation;
}
@end
