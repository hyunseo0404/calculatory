//
//  EquationParser.h
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 22..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Calculator.h"

@interface EquationParser : Calculator

@property (nonatomic,strong) NSString* equation;
@property (nonatomic,strong) Calculator* ans;

-(id) init;
-(id) initWith:(NSString *)input;
-(id) evalulate;
@end
