//
//  Vector.h
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Numbers.h"

@interface Vector : Numbers

@property double x, y, z;

-(id) init;
-(id) initWith:(double)a:(double)b:(double)c;
-(void) setTo:(double)a:(double)b:(double)c;
-(NSString *) print:(NSString *) fmt;
-(Vector *) add: (Vector *) v;
-(Vector *) subtract: (Vector *) v;
-(Vector *) multiply: (id) v;
//-(Vector *) multiply: (Vector *) v;
//-(Vector *) multiplyNum: (Numbers *) n;
-(Vector *) divide: (id) v;
//-(Vector *) divide: (Vector *) v;
//-(Vector *) divideNum: (Numbers *) n;
-(Numbers *) dot: (Vector *) v;
-(Numbers *) magnitude;
-(Vector *) normalize; // unit vector
@end
