//
//  Calculator.h
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "Complex.h"
//#import "Fraction.h"
#import "Numbers.h"
#import "Vector.h"
#import "Message.h"
//#import "EquationParser.h"

@interface Calculator : NSObject

-(NSString *) print:(NSString *) fmt;
-(id) add:(id)placeholder;
@end
