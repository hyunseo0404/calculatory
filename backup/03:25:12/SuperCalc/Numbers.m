//
//  Numbers.m
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "Numbers.h"

@implementation Numbers

@synthesize n;

-(id) init
{
    return [self initWith:0];
}

-(id) initWith:(double)num
{
    self = [super init];
    if (self) {
        [self setTo:num];
    }
    return self;
}

-(void) setTo:(double)num
{
    n = num;
}

-(NSString *) print:(NSString *)fmt
{
//    NSString *fmt = @"%.5f";
    NSString *tmp = [NSString new];
    tmp = [NSString stringWithFormat:fmt, n];
    return tmp;
}

-(Numbers *) add:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:self.n+num.n];
    return temp;
}

-(Numbers *) subtract:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:self.n-num.n];
    return temp;
}

-(Numbers *) multiply:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:self.n*num.n];
    return temp;
}

-(Numbers *) divide:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:self.n/num.n];
    return temp;
}

-(Numbers *) exponentiate:(Numbers *)num
{
    Numbers *temp = [Numbers new];
    [temp setTo:(pow(self.n, num.n))];
    return temp;
}
@end
