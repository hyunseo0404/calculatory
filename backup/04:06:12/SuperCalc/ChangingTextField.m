//
//  ChangingTextField.m
//  QuickCalc
//
//  Created by Hyun Seo Chung on 12. 4. 4..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "ChangingTextField.h"

@implementation ChangingTextField

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        [self.window setAcceptsMouseMovedEvents:YES];
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    
     NSBezierPath * path;
     path = [NSBezierPath bezierPathWithRoundedRect:dirtyRect xRadius:8 yRadius:8];
     [[NSColor colorWithCalibratedRed:0.6882 green:0.8098 blue:1.0000 alpha:1] set]; // <-- BLUE 3
     //    [[NSColor colorWithCalibratedRed:0.6882 green:0.8098 blue:1.0000 alpha:1] set]; // <-- BLUE 2
     //    [[NSColor colorWithCalibratedRed:0.7843 green:0.8980 blue:0.9922 alpha:1] set]; // <-- BLUE 1
     //    [[NSColor colorWithCalibratedRed:0.9294 green:0.9294 blue:0.9294 alpha:1] set]; // <-- DEFAULT WINDOW BACKGROUND COLOR
     [path fill];
     
}

@end
