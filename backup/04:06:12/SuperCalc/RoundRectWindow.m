//
//  RoundRectWindow.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 4. 1..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "RoundRectWindow.h"

@implementation RoundRectWindow

-(id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

-(void)drawRect:(NSRect)dirtyRect
{
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:dirtyRect xRadius:8 yRadius:8];
    [[NSColor colorWithCalibratedRed:0.6882 green:0.8098 blue:1.0000 alpha:0.5] set]; // <-- BLUE 3 THEME COLOR
//    [[NSColor colorWithCalibratedRed:0.7882 green:0.9098 blue:1.0000 alpha:0.5] set]; // <-- BLUE 2 THEME COLOR
//    [[NSColor colorWithCalibratedRed:0 green:0 blue:0 alpha:0.3] set]; // <-- DEFAULT WINDOW BACKGROUND COLOR
    [path fill];
}

-(void)viewWillMoveToWindow:(NSWindow *)newWindow
{
    NSTrackingArea *trackingArea = [[NSTrackingArea alloc] initWithRect:[self frame] options:(NSTrackingMouseEnteredAndExited | NSTrackingActiveAlways) owner:self userInfo:nil];
    [self addTrackingArea:trackingArea];
}

-(void)mouseEntered:(NSEvent *)theEvent
{
    if (![self.window isKeyWindow]) {
        [self.window setAlphaValue:1];
    }
}

-(void)mouseExited:(NSEvent *)theEvent
{
    if (![self.window isKeyWindow]) {
        [self.window setAlphaValue:0.5];
    }
}

@end
