//
//  MainWindowController.h
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Calculator.h"
#import "EquationParser.h"

@interface MainWindowController : NSWindowController {
    IBOutlet id aField, bField;
    IBOutlet id plusButton, minusButton, modularButton, multiplyButton, divideButton, squareButton;
    IBOutlet id ansField;
    IBOutlet id angm;
    IBOutlet id calculateButton;
    IBOutlet id sigfig;
    IBOutlet id helpButton;
    IBOutlet id magButton;
    IBOutlet id normButton;
    IBOutlet id dotButton;
    
@public
    id memory;
}

-(IBAction)add:(id)sender;
-(IBAction)subtract:(id)sender;
-(IBAction)multiply:(id)sender;
-(IBAction)divide:(id)sender;
-(IBAction)square:(id)sender;
-(IBAction)modular:(id)sender;
-(IBAction)calculate:(id)sender;
-(IBAction)magnitude:(id)sender;
-(IBAction)normalize:(id)sender;
-(IBAction)dot:(id)sender;
//-(IBAction)sendKeys:(id)sender;

@end
