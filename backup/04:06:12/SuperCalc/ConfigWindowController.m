//
//  ConfigWindowController.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "ConfigWindowController.h"

@implementation ConfigWindowController

-(id) init {
    self = [super initWithWindowNibName:@"ConfigWindow"];
    return self;
}

-(NSToolbarItem *)toolbar:(NSToolbar *)toolbar itemForItemIdentifier:(NSString *)itemIdentifier willBeInsertedIntoToolbar:(BOOL)flag
{
    
}

-(NSArray *)toolbarAllowedItemIdentifiers:(NSToolbar *)toolbar
{
    
}

-(NSArray *)toolbarDefaultItemIdentifiers:(NSToolbar *)toolbar
{
    
}

-(NSArray *)toolbarSelectableItemIdentifiers:(NSToolbar *)toolbar
{
    
}

-(void)awakeFromNib {
//    NSLog(@"hi");
//    [self.window orderFront:nil];
}

@end
