//
//  ConfigWindowController.h
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ConfigWindowController : NSWindowController <NSToolbarDelegate>
{
    IBOutlet NSToolbar *toolbar;
    
    IBOutlet NSToolbarItem *general;
    IBOutlet NSToolbarItem *features;
    IBOutlet NSToolbarItem *appearance;
    IBOutlet NSToolbarItem *colors;
    IBOutlet NSToolbarItem *advanced;
    IBOutlet NSToolbarItem *usage;
    IBOutlet NSToolbarItem *about;
    
    // font style toolbar item
    IBOutlet NSView *stylePopUpView;         // the font style changing view (ends up in an NSToolbarItem)
    IBOutlet NSMenu *fontStyleMenu;     // this menu and the fontSizeMenu are the menuFormRepresentations of the toolbar items
    NSInteger fontStylePicked;          // a state variable that keeps track of what style has been picked (plain, bold, italic)
    
    // font size toolbar item
    IBOutlet NSView *fontSizeView;      // the font size changing view (ends up in an NSToolbarItem)
    IBOutlet NSMenu *fontSizeMenu;
    IBOutlet id fontSizeStepper;
    IBOutlet id fontSizeField;
    IBOutlet NSTextView *contentView;   // the NSTextView that holds all our text
}

@end
