//
//  EquationParser.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 22..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "EquationParser.h"

@implementation EquationParser

@synthesize equation, ans, result;

// CONSTANTS
const double g = 9.8;
const double G = 6.67e-11;
const double c = 3e8;
const double pi = M_PI;
const double e = M_E;
const NSArray* possibleStr = nil;
const NSArray* possibleNum = nil;
const NSArray* possibleOper = nil;
const NSArray* first = nil;
const NSArray* second = nil;
const NSArray* third = nil;
const NSDictionary* possibleNumFunc = nil;
const NSDictionary* possibleVecFunc = nil;

id memory, long_memory;

-(id) init
{
    return [self initWith:@""];
}

-(id) initWith:(NSString *)input
{
    self = [super init];
    if (self) {
        [self setEquation:input];
        [self initialize];
    }
    return self;
}

-(void)initialize {
    if(!possibleStr)
        possibleStr = [[NSArray alloc] initWithObjects:
                       @"(",@")",@"<",@">",@",",@"**",@"^",@"*",@"/",@"+",@"-",@".",@"e",@"E", nil];
    
    if(!possibleNum)
        possibleNum = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"e",@"E", nil];
    
    if(!possibleOper)
        possibleOper = [[NSArray alloc] initWithObjects:@"**",@"^",@"*",@"/",@"+",@"-", @".", nil];
    
    if(!possibleNumFunc)
        possibleNumFunc = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [NSNumber numberWithInt:0],@"sin(",[NSNumber numberWithInt:1],@"cos(",[NSNumber numberWithInt:2],@"tan(",
                           [NSNumber numberWithInt:3],@"csc(",[NSNumber numberWithInt:4],@"sec(",[NSNumber numberWithInt:5],@"cot(",
                           [NSNumber numberWithInt:6],@"asin(",[NSNumber numberWithInt:7],@"acos(",[NSNumber numberWithInt:8],@"atan(",
                           [NSNumber numberWithInt:9],@"acsc(",[NSNumber numberWithInt:10],@"asec(",[NSNumber numberWithInt:11],@"acot(",
                           [NSNumber numberWithInt:12],@"sinh(",[NSNumber numberWithInt:13],@"cosh(",[NSNumber numberWithInt:14],@"tanh(",
                           [NSNumber numberWithInt:15],@"csch(",[NSNumber numberWithInt:16],@"sech(",[NSNumber numberWithInt:17],@"coth(",
                           [NSNumber numberWithInt:18],@"asinh(",[NSNumber numberWithInt:19],@"acosh(",[NSNumber numberWithInt:20],@"atanh(",
                           [NSNumber numberWithInt:21],@"acsch(",[NSNumber numberWithInt:22],@"asech(",[NSNumber numberWithInt:23],@"acoth(",
                           [NSNumber numberWithInt:24],@"log(",[NSNumber numberWithInt:25],@"sqrt(", nil];
    
    if(!possibleVecFunc)
        possibleVecFunc = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [NSNumber numberWithInt:0],@"mag(",[NSNumber numberWithInt:1],@"norm(", nil];
    
    if (!third)
        third = [[NSArray alloc] initWithObjects:@"^", nil];
    
    if (!second)
        second = [[NSArray alloc] initWithObjects:@"*", @"/", @".", nil];
    
    if (!first)
        first = [[NSArray alloc] initWithObjects:@"+", @"-", nil];
}

NSMutableArray *parts;
NSMutableArray *paren_count;

-(id) doIt
{
    NSError *error = nil;
    NSString *sf = @"%.3f"; // TEST
    BOOL temp;
    
    temp = [self prepare:&error];
    if (!temp) {
        ans = @"...";
        NSLog(@"%@", [error localizedDescription]); // TEST
    }
    else {
        temp = [self parse:&error];
        if ([ans isMemberOfClass:[Numbers class]]) {
            if ([ans n] != 0 && (ABS([ans n]/1e5) >= 1.0 || ABS([ans n]/1e-5) <= 1.0)) {
                ans = [NSString stringWithFormat:@"%.2e",[ans n]];
            } else if (ceil([ans n]) == [ans n] && floor([ans n]) == [ans n]) { // <-- CHECKS IF THE NUMBER IS SIMPLY AN INTEGER
                ans = [NSString stringWithFormat:@"%d",(int)[ans n]];
            } else if (!temp) {
                ans = [[Message alloc] initWith:@"..."];
            } else {
                ans = [(Calculator*) ans print:sf];
            }
        } else if ([ans isMemberOfClass:[Vector class]]) {
            ans = [(Calculator*) ans print:sf];
        } else {
            ans = @"...";
//            ans = [(Calculator*) ans print:sf];
        }
        ans = [ans stringByReplacingOccurrencesOfString:@"-inf" withString:[NSString stringWithFormat:@"-%C", 0x221E]];
        ans = [ans stringByReplacingOccurrencesOfString:@"inf" withString:[NSString stringWithFormat:@"+%C", 0x221E]];
        ans = [ans stringByReplacingOccurrencesOfString:@"nan" withString:@"NaN"];
        ans = [[Message alloc] initWith:ans];
    }
    
    return ans;
}

-(BOOL) prepare:(NSError**)error
{
    NSString *temp = [[NSString alloc] initWithString:equation];
    // replace any white space
    temp = [temp stringByReplacingOccurrencesOfString:@" " withString:@""];
    // replace any valid variables (currently: pi)
    temp = [temp stringByReplacingOccurrencesOfString:@"pi" withString:[NSString stringWithFormat:@"%f", pi]];
    // replace '**' with '^' which means the same thing (exponentiate)
    temp = [temp stringByReplacingOccurrencesOfString:@"**" withString:@"^"];
    
    int p1=0, p2=0;
    // see if number of parentheses match
    // and convert Euler's contants ('e') to the corresponding numbers
    for ( int i = 0; i < [temp length]; i++ ) {
        if ([temp characterAtIndex:i] == '(') {
            p1++;
        }
        else if ([temp characterAtIndex:i] == ')') {
            p2++;
        }
        else if ( [temp characterAtIndex:i] == 'e' && ( (i==0 || i==[temp length]-1) 
                 || ([possibleOper containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:i-1]]]
                 && [possibleOper containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:i+1]]])
                 || ([temp characterAtIndex:i-1] == '(' || [temp characterAtIndex:i+1] == ')') ) ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", e]];
        }
    }
    
    // if the number of parentheses do match
    if (p1!=p2) {
//        [NSException raise:@"Evaluation error" format:@"unmatched parentheses"];
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"unmatched parenthesis" forKey:NSLocalizedDescriptionKey];
        if (error != NULL)
            *error = [NSError errorWithDomain:@"invalid input" code:110 userInfo:details];
        return NO;
    }
    
    // replace any invalid character
    for ( int i = 0; i < [temp length]; i++ ) {
        
        if ( [possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:i]]] || [possibleStr containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:i]]] ) {}
        
        else {
            
            if ( [temp length] >= 4 ) {
                
                int length, length_was;
                BOOL numFuncOrNot = NO;
                BOOL vecFuncOrNot = NO;
                
                if ( [temp length] >= 6 )
                {
                    length = 6;
                    length_was = 6;
                    if ([possibleNumFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 6)]]!=nil)
                        numFuncOrNot = YES;
                    else if ([possibleVecFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 6)]]!=nil)
                        vecFuncOrNot = YES;
                }
                if ( !numFuncOrNot && !vecFuncOrNot && [temp length] >= 5 )
                {
                    length = 5;
                    length_was = 5;
                    if ([possibleNumFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 5)]]!=nil)
                        numFuncOrNot = YES;
                    else if ([possibleVecFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 5)]]!=nil)
                        vecFuncOrNot = YES;
                }
                if ( !numFuncOrNot && !vecFuncOrNot && [temp length] >= 4 )
                {
                    length = 4;
                    length_was = 4;
                    if ([possibleNumFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 4)]]!=nil)
                        numFuncOrNot = YES;
                    else if ([possibleVecFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 4)]]!=nil)
                        vecFuncOrNot = YES;
                }
                
                if (numFuncOrNot) {
                    int count = 0;
                    int j = i+length;
                    BOOL needed=NO;
                    int countPer=0;
                    int countPar=1;
                    while ( countPar!=0 ) {
                        if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                        else if ([temp characterAtIndex:j] == '(') {
                            countPar++;
                        } else if ([temp characterAtIndex:j] == ')') {
                            countPar--;
                        } else if ([temp characterAtIndex:j] == '.') {
                            if (countPer==0) {
                                countPer++;
                            } else {
                                needed = YES;
                            }
                        } else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                                   && [temp characterAtIndex:j] != '.') {
                            needed = YES;
                        }
                        count++;
                        j++;
                    }
                    j++;
                    count--;
                    j--;
                    double num_tmp;
                    
                    if (needed) {
                        NSString *newTemp;
                        while (![newTemp isMemberOfClass:[Numbers class]] && ![newTemp isMemberOfClass:[Vector class]]) {
                            EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                            NSError* new_error = nil; //
                            newTemp = [newParser prepare:&new_error]; //
                            newTemp = [newParser parse:&new_error]; //
                        }
                        if (![newTemp isMemberOfClass:[Numbers class]]) {
//                            [NSException raise:@"Invalid input" format:@"invalid operation for a vector"];
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            [details setValue:@"invalid operation for a vector" forKey:NSLocalizedDescriptionKey];
                            if (error != NULL)
                                *error = [NSError errorWithDomain:@"invalid input" code:120 userInfo:details];
                            return NO;
                        }
                        num_tmp = [[(Calculator*)newTemp print:@"%f"] doubleValue];
                        NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:num_tmp_str];
                        j = i;
                        j--;
                    }
                    
                    else {
                        if ( length_was == 6 )
                        {
                            switch ([[possibleNumFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 6)]] intValue]) {
                                case 18:
                                    num_tmp = asinh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 19:
                                    num_tmp = acosh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 20:
                                    num_tmp = atanh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 21:
                                    num_tmp = 1/asinh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 22:
                                    num_tmp = 1/acosh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 23:
                                    num_tmp = 1/atanh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else if ( length_was == 5 )
                        {
                            switch ([[possibleNumFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 5)]] intValue]) {
                                case 6:
                                    num_tmp = asin([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 7:
                                    num_tmp = acos([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 8:
                                    num_tmp = atan([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 9:
                                    num_tmp = 1/asin([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 10:
                                    num_tmp = 1/acos([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 11:
                                    num_tmp = 1/atan([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 12:
                                    num_tmp = sinh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 13:
                                    num_tmp = cosh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 14:
                                    num_tmp = tanh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 15:
                                    num_tmp = 1/sinh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 16:
                                    num_tmp = 1/cosh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 17:
                                    num_tmp = 1/tanh([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 25:
                                    num_tmp = sqrt([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else if ( length_was == 4 )
                        {
                            switch ([[possibleNumFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 4)]] intValue]) {
                                case 0:
                                    num_tmp = sin([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 1:
                                    num_tmp = cos([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 2:
                                    num_tmp = tan([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 3:
                                    num_tmp = 1/sin([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 4:
                                    num_tmp = 1/cos([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 5:
                                    num_tmp = 1/tan([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                case 24:
                                    num_tmp = log([[temp substringWithRange:NSMakeRange(i+length, count)] doubleValue]);
                                    break;
                                default:
                                    break;
                            }
                        }
                        NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                    }
                    if ( num_tmp != num_tmp ) {
//                        [NSException raise:@"Invalid input" format:@"math domain error"];
                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                        [details setValue:@"math domain error" forKey:NSLocalizedDescriptionKey];
                        if (error != NULL)
                            *error = [NSError errorWithDomain:@"invalid input" code:130 userInfo:details];
                        return NO;
                    }
                    i--;
                }
                
                else if (vecFuncOrNot) {
                    
                    int count = 0;
                    int j = i+length;
                    BOOL needed=NO;
                    int countPer=0;
                    int countPar=1;
                    while ( countPar!=0 ) {
                        if (j == i + length && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                        else if ((([temp characterAtIndex:j-1] == ' ' && [temp characterAtIndex:j-2] == ',')
                                  || [temp characterAtIndex:j-1] == ',' || [temp characterAtIndex:j-1] == '<')
                                 && ([temp characterAtIndex:j] == '-' || [temp characterAtIndex:j] == '+')) {}
                        else if ([temp characterAtIndex:j] == ' ') {}
                        else if ([temp characterAtIndex:j] == '(') {
                            countPar++;
                        } else if ([temp characterAtIndex:j] == ')') {
                            countPar--;
                        } else if ([temp characterAtIndex:j] == '.') {
                            if (countPer<3) {
                                countPer++;
                            } else {
                                needed = YES;
                            }
                        } else if (([temp characterAtIndex:j] == '+' || [temp characterAtIndex:j] == '-') 
                                   && ([temp characterAtIndex:j-1] == 'e' || [temp characterAtIndex:j-1] == 'E')) {}
                        else if (![possibleNum containsObject:[NSString stringWithFormat:@"%c",[temp characterAtIndex:j]]]
                                 && [temp characterAtIndex:j] != '<' && [temp characterAtIndex:j] != '>'  && [temp characterAtIndex:j] != ',') {
                            needed = YES;
                        }
                        count++;
                        j++;
                    }
                    j++;
                    count--;
                    j--;
                    Vector *vec_tmp;
                    double num_tmp;
                    
                    if (needed) {
                        NSString *newTemp;
                        while (![newTemp isMemberOfClass:[Vector class]] && ![newTemp isMemberOfClass:[Numbers class]]) {
                            EquationParser *newParser = [[EquationParser alloc] initWith:[temp substringWithRange:NSMakeRange(i+length, count)]];
                            
                            NSError* new_error = nil; //
                            newTemp = [newParser prepare:&new_error]; //
                            newTemp = [newParser parse:&new_error]; //
                        }
                        NSString* vec_tmp_str = [(Calculator*)newTemp print:@"%f"];
                        temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i+length, count) withString:vec_tmp_str];
                        j = i;
                        j--;
                    }
                    
                    else {
                        if ( length_was == 6 )
                        {
//                            [NSException raise:@"Invalid input" format:@"unknown error"];
                            NSMutableDictionary* details = [NSMutableDictionary dictionary];
                            [details setValue:@"unknown error (length_was == 6)" forKey:NSLocalizedDescriptionKey];
                            if (error != NULL)
                                *error = [NSError errorWithDomain:@"invalid input" code:199 userInfo:details];
                            return NO;
                        }
                        else if ( length_was == 5 )
                        {
                            switch ([[possibleVecFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 5)]] intValue]) {
                                case 1:
                                {
                                    vec_tmp = [[Vector alloc] initWithStr:[temp substringWithRange:NSMakeRange(i+length, count)]];
                                    vec_tmp = [vec_tmp normalize];
                                    NSString* vec_tmp_str = [vec_tmp print:@"%f"];
                                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:vec_tmp_str];
                                    break;
                                }
                                default:
                                    break;
                            }
                        }
                        else if ( length_was == 4 )
                        {
                            switch ([[possibleVecFunc objectForKey:[temp substringWithRange:NSMakeRange(i, 4)]] intValue]) {
                                case 0:
                                {
                                    vec_tmp = [[Vector alloc] initWithStr:[temp substringWithRange:NSMakeRange(i+length, count)]];
                                    num_tmp = [[[vec_tmp magnitude] print:@"%f"] doubleValue];
                                    NSString* num_tmp_str = [NSString stringWithFormat:@"%f", num_tmp];
                                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, count+length+1) withString:num_tmp_str];
                                    if ( num_tmp != num_tmp ) {
                                        NSMutableDictionary* details = [NSMutableDictionary dictionary];
                                        [details setValue:@"math domain error" forKey:NSLocalizedDescriptionKey];
                                        if (error != NULL)
                                            *error = [NSError errorWithDomain:@"invalid input" code:130 userInfo:details];
                                        return NO;
                                    }
                                    break;
                                }
                                default:
                                    break;
                            }
                        }
                    }
                    i--;
                }
                
                else {
                    temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:@""];
                    i--;
                }
                
            }
            // if length >= 4 condition done
        }
        // else condition done
//        NSLog(@"%@", temp); // TEST
    }
    // after for loop
    
        /*
        else if ( [temp characterAtIndex:i] == 'g' ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", g]];
        }
        else if ( [temp characterAtIndex:i] == 'G' ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", G]];
        }
        else if ( [temp characterAtIndex:i] == 'c' ) {
            temp = [temp stringByReplacingCharactersInRange:NSMakeRange(i, 1) withString:[NSString stringWithFormat:@"%f", c]];
        }
        */
    
    self.equation = temp;
//    return temp;
    return YES;
    
}

-(BOOL) parse:(NSError**)error
{
    NSMutableArray *output = [NSMutableArray new];
    NSMutableArray *operator = [NSMutableArray new];
    int unary = 1;
    char c;
    NSString *cur;
    
    for ( int i = 0; i < [equation length]; i++ ) {
        
        c = [equation characterAtIndex:i];
        cur = [NSString stringWithFormat:@"%c", c];
        
        // check for unary operator (in the beginning)
        if ( i == 0 && (c == '+' || c == '-') ) {
            if (c == '+') unary = 1;
            else if (c == '-') unary = -1;
        }
        
        // check for unary operator
        else if ( (c == '+' || c == '-') && ( ([possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]])
                 || [equation characterAtIndex:i-1] == '(' ) ) {
            if ( [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i+1]]] && [equation characterAtIndex:i+1] != '.' ) {
//                [NSException raise:@"evaluation error" format:@"Two or more operators found in a row"];
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:@"two or more operators found in a row" forKey:NSLocalizedDescriptionKey];
                if (error != NULL)
                    *error = [NSError errorWithDomain:@"unable to parse" code:210 userInfo:details];
                return NO;
            }
            else {
                if (c == '+') unary = 1;
                else if (c == '-') unary = -1;
            }
        }
        
        // check for numbers
        else if ( [possibleNum containsObject:cur] || (i==0 && c=='.') || ([possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]] && c == '.') || ([equation characterAtIndex:i-1] == '(' && c == '.') ) {
            NSString *num = @"";
            while ( i < [equation length] && ( ([possibleNum containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i]]] 
                   || [equation characterAtIndex:i] == '.') || (([equation characterAtIndex:i] == '-' || [equation characterAtIndex:i] == '+')
                   && ([equation characterAtIndex:i-1] == 'e' || [equation characterAtIndex:i-1] == 'E') 
                   && [possibleNum containsObject:[NSString stringWithFormat:@"%c", [equation characterAtIndex:i+1]]]) ) ) {
                num = [NSString stringWithFormat:@"%@%c",num,[equation characterAtIndex:i]];
                i++;
            }
            // add the number to the output
            [output addObject:[[Numbers alloc] initWith:unary*[num doubleValue]]];
            // reset num
            num = nil;
            // reset unary
            unary = 1;
            i--;
        }
        
        // check for vectors
        else if ( c == '<' ) {
            NSArray *temp = [NSArray new];
            Vector *vec = [Vector new];
            int count = 0;
            i++;
            int origin = i;
            NSString *num = @"";
            while ( i < [equation length] && [equation characterAtIndex:i] != '>' ) {
                count++;
                i++;
            }
            num = [num stringByAppendingString:[equation substringWithRange:NSMakeRange(origin, count)]];
            num = [num stringByReplacingOccurrencesOfString:@" " withString:@""];
            temp = [num componentsSeparatedByString:@","];
            [vec setTo:[[temp objectAtIndex:0] doubleValue] :[[temp objectAtIndex:1] doubleValue] :[[temp objectAtIndex:2] doubleValue]];
            // add the vector to the output
            [output addObject:vec];
        }
        
        // check for operators
        else if ( [possibleOper containsObject:cur] || c == '(' || c == ')' ) {
            
            if ( !(c == '.' || [equation characterAtIndex:i-1] == '.') && [possibleOper containsObject:[NSString stringWithFormat:@"%c",c]]
                     && [possibleOper containsObject:[NSString stringWithFormat:@"%c",[equation characterAtIndex:i-1]]] ) {
//                [NSException raise:@"evaluation error" format:@"Two or more operators found in a row"];
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:@"two or more operators found in a row" forKey:NSLocalizedDescriptionKey];
                if (error != NULL)
                    *error = [NSError errorWithDomain:@"unable to parse" code:210 userInfo:details];
                return NO;
            }
            else {
                
                // SHUNTING YARD ALGORITHM
                
                // precedence 1: '+', '-'
                if ( [first containsObject:cur] ) {
                    
                    int a = (int) [operator count]-1;
                    
                    if ([operator count] == 0) {
                        [operator addObject:cur];
                    }
                    else {
                        if ( [[operator objectAtIndex:0] isEqualTo:@"("] ) {
                            [operator insertObject:cur atIndex:0];
                        }
                        else {
                            while ( a >= 0 && ![[operator objectAtIndex:0] isEqualTo:@"("] ) {
                                [output addObject:[operator objectAtIndex:0]];
                                [operator removeObjectAtIndex:0];
                                a--;
                            }
                            [operator insertObject:cur atIndex:0];
                        }
                    }
                }
                
                // precedence 2: '*', '/', '.'
                else if ( [second containsObject:cur] ) {
                    
                    int a = (int) [operator count]-1;
                    
                    if ([operator count] == 0) {
                        [operator addObject:cur];
                    }
                    else {
                        if ( [[operator objectAtIndex:0] isEqualTo:@"("] ) {
                            [operator insertObject:cur atIndex:0];
                        }
                        else {
                            while ( a >= 0 && ![[operator objectAtIndex:0] isEqualTo:@"("] && ![first containsObject:[operator objectAtIndex:0]] ) {
                                [output addObject:[operator objectAtIndex:0]];
                                [operator removeObjectAtIndex:0];
                                a--;
                            }
                            [operator insertObject:cur atIndex:0];
                        }
                    }
                }
                
                // precedence 3: '^' ('**')
                else if ( [third containsObject:cur] ) {
                    [operator insertObject:cur atIndex:0];                   
                }
                
                // open parenthesis
                else if ( c == '(' ) {
                    [operator insertObject:cur atIndex:0];
                }
                
                // close parenthesis
                else if ( c == ')' ) {
                    
                    int a = (int) [operator count]-1;
                    
                    while ( a >= 0 && ![[operator objectAtIndex:0] isEqualTo:@"("] ) {
                        [output addObject:[operator objectAtIndex:0]];
                        [operator removeObjectAtIndex:0];
                        a--;
                    }
                    [operator removeObjectAtIndex:0];
                }
                
            }
        }
        
        // raise exception
        else {
//            [NSException raise:@"Evaluation error" format:[NSString stringWithFormat:@"unknown case: %c",c]];
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:[NSString stringWithFormat:@"unknown error (unknown case: %c)",c] forKey:NSLocalizedDescriptionKey];
            if (error != NULL)
                *error = [NSError errorWithDomain:@"unable to parse" code:299 userInfo:details];
            return NO;
        }
        
        
        // TEST
//        NSLog(@"%@", [NSString stringWithFormat:@"output: %@,\noperator: %@",[output description],[operator description]]);
        
    }
    
    // pop entire stack to output
    if ( [operator count] != 0 ) {
        while ( [operator count] != 0 ) {
            [output addObject:[operator objectAtIndex:0]];
            [operator removeObjectAtIndex:0];
        }
    }
    
    // double-check to see that the operator stack is now empty
    if ( [operator count] != 0 ) {
//        [NSException raise:@"Evaluation error" format:@"operator not clean up properly"];
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"operator not cleaned up properly" forKey:NSLocalizedDescriptionKey];
        if (error != NULL)
            *error = [NSError errorWithDomain:@"unable to parse" code:220 userInfo:details];
        return NO;
    }
    
    // TEST (prints the final postfix evaluation result)
    NSString *final = @"";
    for (int i = 0; i < [output count]; i++) {
        if ([[output objectAtIndex:i] isKindOfClass:[NSString class]]) {
            final = [final stringByAppendingString:[output objectAtIndex:i]];
        }
        else {
            final = [final stringByAppendingString:[(Calculator*)[output objectAtIndex:i] print:@"%.0f"]];
        }
        final = [final stringByAppendingString:@" "];
    }
//    NSLog(@"%@", final);
    // TEST
    
//    NSLog(@"%@",[NSString stringWithFormat:@"equation: %@,\noutput: %@,\noperator: %@,\n\nFINAL: %@",equation,[output description],[operator description], final]);
    
    NSString *t = [NSString new];
    if ( [output count] != 1 ) {
        NSError* new_error = nil;
        t = [self evalulate:output:&new_error];
    } else {
        t = [output objectAtIndex:0];
    }
    
    ans = t;
    
//    return t;
    return YES;
}

-(BOOL) evalulate:(NSMutableArray *)output:(NSError**)error
{
    NSMutableArray *stack = [NSMutableArray new];
    
    while ( [output count] != 0 ) {
        while ( [output count] != 0 && ([[output objectAtIndex:0] isMemberOfClass:[Numbers class]] || [[output objectAtIndex:0] isMemberOfClass:[Vector class]]) ) {
            [stack addObject:[output objectAtIndex:0]];
            [output removeObjectAtIndex:0];
//            NSLog(@"%@,%@",[stack description], [output description]);
        }
        
        if ( [possibleOper containsObject:[output objectAtIndex:0]] && [stack count] >= 3 ) {
            
            if ( [[output objectAtIndex:0] isEqualTo:@"^"] ) {
                [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] exponentiate:[stack objectAtIndex:[stack count]-1]]];
                [stack removeObjectAtIndex:[stack count]-2];
                [stack removeObjectAtIndex:[stack count]-2];
                [output removeObjectAtIndex:0];
    //            NSLog(@"%@,%@",[stack description], [output description]);
            } else if ( [[output objectAtIndex:0] isEqualTo:@"*"] ) {
                [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] multiply:[stack objectAtIndex:[stack count]-1]]];
                [stack removeObjectAtIndex:[stack count]-2];
                [stack removeObjectAtIndex:[stack count]-2];
                [output removeObjectAtIndex:0];
    //            NSLog(@"%@,%@",[stack description], [output description]);
            } else if ( [[output objectAtIndex:0] isEqualTo:@"/"] ) {
                [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] divide:[stack objectAtIndex:[stack count]-1]]];
                [stack removeObjectAtIndex:[stack count]-2];
                [stack removeObjectAtIndex:[stack count]-2];
                [output removeObjectAtIndex:0];
    //            NSLog(@"%@,%@",[stack description], [output description]);
            } else if ( [[output objectAtIndex:0] isEqualTo:@"+"] ) {
                [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] add:[stack objectAtIndex:[stack count]-1]]];
                [stack removeObjectAtIndex:[stack count]-2];
                [stack removeObjectAtIndex:[stack count]-2];
                [output removeObjectAtIndex:0];
    //            NSLog(@"%@,%@",[stack description], [output description]);
            } else if ( [[output objectAtIndex:0] isEqualTo:@"-"] ) {
                [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] subtract:[stack objectAtIndex:[stack count]-1]]];
                [stack removeObjectAtIndex:[stack count]-2];
                [stack removeObjectAtIndex:[stack count]-2];
                [output removeObjectAtIndex:0];
    //            NSLog(@"%@,%@",[stack description], [output description]);
            } else if ( [[output objectAtIndex:0] isEqualTo:@"."] ) {
                [stack addObject:[(Calculator*)[stack objectAtIndex:[stack count]-2] dot:[stack objectAtIndex:[stack count]-1]]];
                [stack removeObjectAtIndex:[stack count]-2];
                [stack removeObjectAtIndex:[stack count]-2];
                [output removeObjectAtIndex:0];
    //            NSLog(@"%@,%@",[stack description], [output description]);
            }
        
        } else if ( [possibleOper containsObject:[output objectAtIndex:0]] ) {
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:@"operator found without enough items to operate with" forKey:NSLocalizedDescriptionKey];
            if (error != NULL)
                *error = [NSError errorWithDomain:@"unable to evaluate" code:330 userInfo:details];
            return NO;
        }
        
    }
    
    if ([stack count] == 0) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"no evaluatable item in the stack" forKey:NSLocalizedDescriptionKey];
        if (error != NULL)
            *error = [NSError errorWithDomain:@"unable to evaluate" code:310 userInfo:details];
        return NO;
    }
    else if ([possibleOper containsObject:[stack objectAtIndex:0]]) {
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"single operator found in the stack" forKey:NSLocalizedDescriptionKey];
        if (error != NULL)
            *error = [NSError errorWithDomain:@"unable to evaluate" code:320 userInfo:details];
        return NO;
    }
    else {
//        return [stack objectAtIndex:0];
        ans = [stack objectAtIndex:0];
        return YES;
    }
    
}
@end
