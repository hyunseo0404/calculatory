//
//  AppDelegate.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "AppDelegate.h"
#import "MainWindowController.h"
#import "QuickModeController.h"
#import "ConfigWindowController.h"

#import <Carbon/Carbon.h>

@implementation AppDelegate

-(IBAction)showMainWindow:(id)sender
{
//    if (!mainWindowController) {
        mainWindowController = [[MainWindowController alloc] init];
//    }
    [mainWindowController showWindow:nil];
//    [NSApp arrangeInFront:sender];
//    [[mainWindowController window] makeKeyAndOrderFront:sender];
//    [NSApp activateIgnoringOtherApps:YES];
}

-(IBAction)showQuickMode:(id)sender
{
//    if (!quickModeController) {
        quickModeController = [[QuickModeController alloc] init];
//    }
    [quickModeController showWindow:nil];
}

-(IBAction)showConfigWindow:(id)sender
{
//    if (!configWindowController) {
        configWindowController = [[ConfigWindowController alloc] init];
//    }
    [configWindowController showWindow:nil];
    [[configWindowController window] makeKeyWindow];
    [[configWindowController window] makeMainWindow];
    [[configWindowController window] makeKeyAndOrderFront:nil];
}

/*
-(IBAction)unhideWindow:(id)sender
{
    [NSApp arrangeInFront:sender];
//    [[self window] makeKeyAndOrderFront:sender];
    [NSApp activateIgnoringOtherApps:YES];
}
*/

-(void)awakeFromNib {
    statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
    [statusItem setMenu:statusMenu];
    [statusItem setTitle:[NSString stringWithFormat:@"%C", 0x27B9]];
//    -(void)setImage:(NSImage *)image
//    -(void)setAlternateImage:(NSImage *)image
    [statusItem setHighlightMode:YES];
    [statusItem setToolTip:@"SuperCalc"];
    [statusItem setMenu: statusMenu];
    [self registerHotKeys];
}

// HANDLES GLOBAL HOTKEYS
OSStatus MyHotKeyHandler(EventHandlerCallRef nextHandler, EventRef theEvent, void *userData) {
    
    EventHotKeyID hkCom;
    
    GetEventParameter(theEvent, kEventParamDirectObject, typeEventHotKeyID, NULL, sizeof(hkCom), NULL, &hkCom);
//    AppController *controller = (AppController *)userData;
    
    int l = hkCom.id;
    
    switch (l) {
        case 1:
            [[[MainWindowController alloc] init] showWindow:nil];
            NSLog(@"case 1");
            break;
        case 2:
            NSLog(@"case 2");
            break;
        case 3:
            NSLog(@"case 3");
            break;
        default:
            break;
    }
    return noErr;
}

// REGISTERS GLOBAL HOTKEYS
-(void)registerHotKeys {
    EventHotKeyRef gMyHotKeyRef;
    EventHotKeyID gMyHotKeyID;
    EventTypeSpec eventType;
    eventType.eventClass=kEventClassKeyboard;
    eventType.eventKind=kEventHotKeyPressed;
    
    InstallApplicationEventHandler(&MyHotKeyHandler, 1, &eventType, NULL, NULL);
    
    gMyHotKeyID.signature='htk1';
    gMyHotKeyID.id=1;
    RegisterEventHotKey(49, controlKey+optionKey+cmdKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
    
    gMyHotKeyID.signature='htk2';
    gMyHotKeyID.id=2;
    RegisterEventHotKey(124, cmdKey+optionKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
    
    // shift+space, DEFAULT GLBOAL HOTKEY for the QuickMode
    gMyHotKeyID.signature='htk3';
    gMyHotKeyID.id=3;
    RegisterEventHotKey(49, shiftKey, gMyHotKeyID, GetApplicationEventTarget(), 0, &gMyHotKeyRef);
    
}

-(void)applicationDidFinishLaunching:(NSNotification *)notification {
    [NSApp activateIgnoringOtherApps:YES];
    // put code...
}

@end
