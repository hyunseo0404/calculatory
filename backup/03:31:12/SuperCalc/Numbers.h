//
//  Numbers.h
//  SuperCalcText
//
//  Created by Hyun Seo Chung on 12. 3. 21..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Numbers : NSObject

@property double n;

-(id) init;
-(id) initWith: (double) num;
-(void) setTo:(double) num;
-(NSString *) print:(NSString *) fmt;
-(Numbers *) add:(Numbers *) num;
-(Numbers *) subtract:(Numbers *) num;
-(id) multiply:(id) num;
-(id) divide:(id) num;
-(Numbers *) exponentiate:(Numbers *) num;
@end
