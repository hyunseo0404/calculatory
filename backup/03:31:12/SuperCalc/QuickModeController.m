//
//  QuickModeController.m
//  SuperCalc
//
//  Created by Hyun Seo Chung on 12. 3. 28..
//  Copyright (c) 2012년 hyunseo0404@naver.com. All rights reserved.
//

#import "QuickModeController.h"

@implementation QuickModeController

-(id) init {
    self = [super initWithWindowNibName:@"QuickMode"];
    return self;
}

-(IBAction)calculate:(id)sender
{
    id ans;
//    NSString *sf = [sigfig stringValue];
    NSString *sf = @"%.5f"; // TEST
    temp = [NSString new];
    temp = [aField stringValue];
    
    EquationParser *eq = [[EquationParser alloc] initWith:temp];
    temp = [eq prepare];
    
    @try {
        // try interpretation + calculation
        // if the input was blank
        if ( [temp isEqualToString:@""] ) {
            ans = [[Message alloc] initWith:@"BLANK INPUT!"];
        }
        // else start interpretation + calculation
        else {
            // use the EquationParser to interpret and calculate
            ans = [eq parse];
            ans = [(Calculator*) ans print:sf];
            ans = [[Message alloc] initWith:ans];
        }
    }
    
    @catch (NSException *exception) {
        [ans appendLN];
        [ans appendLN];
        ans = [[Message alloc] initWith:[exception name]];
        [ans appendMsg:@": "];
        [ans appendMsg:[exception reason]];
        [ans appendLN];
    }
    
    @finally {
        // print the final value
        if ([[ansField string] isEqualToString:@""]) {
            [ansField setString:[NSString stringWithFormat:@"%@", [(Calculator*)ans print:sf]]];
        }
        else {
            [ansField setString:[NSString stringWithFormat:@"%@\n%@", [ansField string], [(Calculator*)ans print:sf]]];
        }
        // AUTO-SCROLL TO THE BOTTM AFTER EACH CALCULATION
        [ansField scrollRangeToVisible:NSMakeRange([[ansField string] length], 0)];
    }
}

-(void)controlTextDidChange:(NSNotification *)obj
{
    if ([obj object] == aField) {
        if ([[aField stringValue] isNotEqualTo:@""]) {
            NSLog(@"test2");
            [self calculate:[aField stringValue]];
        }
    }
}

// For the textfield (aField),
// overrides the default setting so that when the user types the 'Return' key, 
// the program automatically calculates the input (value).
-(BOOL)control:(NSControl *)control textView:(NSTextView *)fieldEditor
doCommandBySelector:(SEL)commandSelector {
    BOOL retval = NO;
    if (commandSelector == @selector(insertNewline:)) {
        retval = YES;
        // CHANGE TO COPY THE LAST ANSWER
        [self calculate:[fieldEditor string]];
    }
    return retval;
}

/*
-(void)keyDown:(NSEvent *)theEvent
{
    NSLog(@"testest");
    if ([theEvent keyCode] == 53) {
        NSLog(@"test");
        [[self window] close];
    }
}
*/

@end
